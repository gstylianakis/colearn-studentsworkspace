<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="general.ActivityResources"%>
<jsp:useBean id="parser" scope="page" class="general.LDElementParser"/>
<%
    String activity = request.getParameter("activity");
    ActivityResources resources = parser.getItemsFromActivity(parser.stringToXml(activity));
    out.println(resources.getLearning_objectives().getContentURL());
%>