<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.BufferedWriter"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.util.UUID"%>
<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<%@page import="java.io.IOException"%>
<%@page import="java.io.File"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.util.Properties"%>
<%
    //UUID id = UUID.randomUUID();

    Properties configFile = new Properties();
    String dirName="C";
    try{
        configFile.load( new FileInputStream("config.properties"));//periexei tin pliroforia gia to directory pu tha swzontai ta unit of learnings zip arxeia
        dirName = configFile.getProperty("uploadFileDir");//uploadFileDir
    } catch(FileNotFoundException e){
        dirName="C";
    }
    
    
    String filename = request.getParameter("tmp_filename");
    String filenameToShow = request.getParameter("filename");
    String content = request.getParameter("content");
    
    String filepath=dirName+"/"+filename;
    //System.out.println(Base64.decode(content));
    FileWriter fstream = new FileWriter(filepath);
    BufferedWriter tmpFile = new BufferedWriter(fstream);
    tmpFile.write(content);
  //Close the output stream
    tmpFile.close();
   
//psaxnei to unzip arxeio..........
//an svinontai telika ta unzip arxeia apo ton server tha prepei na psaxeni to zip arxeio....

    BufferedInputStream buf=null;
    ServletOutputStream myOut=null;
    File myfile = null;
    try{
        myOut = response.getOutputStream( );
        myfile = new File(filepath);
    //System.out.println("to path einai "+filepath);
        //set response headers
        response.setContentType("text/plain");
        response.addHeader("Content-Disposition","attachment; filename="+filenameToShow );

        response.setContentLength( (int) myfile.length( ) );
        String content2 = content.substring(content.indexOf(",")+1);
        //System.out.println(content2);
        Base64 decoder = new Base64();
        byte[] imgBytes = decoder.decode(content2);
        //System.out.println(imgBytes);
        myOut.write(imgBytes);
        myOut.flush();
        //FileInputStream input = new FileInputStream(myfile);
        //buf = new BufferedInputStream(input);
        //int readBytes = 0;
        //read from the file; write to the ServletOutputStream
        //while((readBytes = buf.read( )) != -1)
            //myOut.write(readBytes);

    } catch (IOException ioe){
        throw new ServletException(ioe.getMessage( ));
     } finally {
     //close the input/output streams
         if (myOut != null)
             myOut.close( );
          if (buf != null)
          buf.close( );
          //myfile.delete();

     }
%>