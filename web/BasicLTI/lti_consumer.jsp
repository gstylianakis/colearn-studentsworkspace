<%@page import="java.util.HashMap"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.Import"%>
<%@page import="org.json.JSONObject"%>
<html>
<head>
  <title>IMS Basic Learning Tools Interoperability</title>
</head>
<body style="font-family:sans-serif">
<img src="http://www.sun.com/images/l2/l2_duke_java.gif" align="right">

<p>Authorisation..Please wait...</p><img src="../images/Loading.gif"/>
<%@ page import="org.imsglobal.basiclti.BasicLTIUtil" %>
<%@ page import="java.util.Properties" %>
<%@ page import="javax.servlet.http.HttpServletRequest" %>

<%
    Gson gson = new Gson();
    HashMap basic_ltiParam = gson.fromJson(request.getParameter("params"),HashMap.class);
    
    String secret = (String)basic_ltiParam.get("secret");
    String key = (String)basic_ltiParam.get("key");

    if ( key == null || key.compareTo("undefined")==0)
        key = "12345";

    
    String org_id = (String)basic_ltiParam.get("org_id");

    if ( org_id == null || org_id.compareTo("undefined")==0)
      org_id = "no_organization_id_defined";
  
    String org_desc = (String)basic_ltiParam.get("org_desc");
  
  if ( org_desc == null || org_desc.compareTo("undefined")==0)
      org_desc = "no_organization_description_defined";
  
    String org_secret = (String)basic_ltiParam.get("org_secret");
  
      if ( (secret == null || secret.compareTo("undefined")==0) && org_secret == null)
          secret = "secret";
      if ( org_secret == null || org_secret.compareTo("undefined")==0)
          org_secret = "";

    String endpoint = (String)basic_ltiParam.get("launch_url");
//String endpoint  = "http://147.27.41.25:8084/jforum/redirect2Jforum.jsp";
  if ( endpoint == null )
      endpoint = "no_url";
  String context_id = (String)basic_ltiParam.get("context_id");
  if(context_id==null || context_id.compareTo("undefined")==0)
    context_id="456434513";
  String context_title = (String)basic_ltiParam.get("context_title");
  if(context_title==null || context_title.compareTo("undefined")==0)
      context_title = "no title specified";
  String context_label = (String)basic_ltiParam.get("context_label");
  if(context_label==null || context_label.compareTo("undefined")==0)
      context_label = "no lable specified";
  
  String email = request.getParameter("email");
  
  if(email==null || email.compareTo("undefined")==0)
      email="no email specified";
  String user_id = request.getParameter("user_id");
  
  if(user_id==null || user_id.compareTo("undefined")==0)
      user_id="-1";
  String username = request.getParameter("username");
  //System.out.println("to username einai "+username);

  if(username==null || username.compareTo("undefined")==0 )
      username="none";
  String resource_id = (String)basic_ltiParam.get("resource_link_id")+"_"+request.getParameter("resourceId");
  if(resource_id==null || resource_id.compareTo("undefined")==0)
      resource_id = "120988f929-274612";

   String resource_title = (String)basic_ltiParam.get("resource_link_title");
  if(resource_title==null)
      resource_title = "testing forum capability";

  String resource_label = request.getParameter("resource_label");
  if(resource_label==null)
      resource_label = "no_label_given";

  String resource_description = request.getParameter("resource_description");
  if(resource_description==null)
      resource_description = "no_description_given";

  Properties postProp = new Properties();
  postProp.setProperty("resource_link_id",resource_id);  
  postProp.setProperty("user_id",user_id);
  postProp.setProperty("roles","administrator");

  postProp.setProperty("lis_person_name_given",username);
  postProp.setProperty("lis_person_contact_email_primary",email);
  postProp.setProperty("lis_person_sourcedid","tuc:user");
  postProp.setProperty("context_id",context_id);
  postProp.setProperty("context_title",context_title);
  postProp.setProperty("context_label",context_label);
  //postProp.setProperty("context_id",context_id);
  //postProp.setProperty("context_title",context_title);
  postProp.setProperty("lti_message_type","basic-lti-launch-request");
  postProp.setProperty("resource_link_title",resource_title);
  postProp.setProperty("resource_link_label",resource_label);
  postProp.setProperty("resource_link_description",resource_description);
  postProp.setProperty("BLTI","yes");
  //getLMSDummyData(postProp);
  String urlformatstr = request.getParameter("format");
  boolean urlformat = urlformatstr == null || ! urlformatstr.equals("XML");
  String lmspwstr = request.getParameter("lmspw");
  boolean lmspw = lmspwstr == null || ! lmspwstr.equals("Resource");

  
  // Ignore the organizational info if this is not an LMS password
  if ( ! lmspw ) org_id = null;

  Properties info = new Properties();
  
   /*else {
    if ( BasicLTIUtil.parseDescriptor(info, postProp, xmldesc) ) {
      getLMSDummyData(postProp);
      endpoint = info.getProperty("launch_url");
      if ( endpoint == null ) {
        out.println("<p>Error, did not find a launch_url or secure_launch_url in the XML descriptor</p>\n");
        return;
      }
      endpoint = endpoint.replace("CUR_URL",cur_url.replace("lms.jsp", "provider"));

    }
  }*/

  // Off to the races with BasicLTI...
 
  if ( org_secret.equals("") ) org_secret = null;
  postProp = BasicLTIUtil.signProperties(postProp, endpoint, "POST", key, secret, org_secret, org_id, org_desc);
 
  String postData = BasicLTIUtil.postLaunchHTML(postProp, endpoint, false);
  System.out.println(postData);
  out.println(postData);
%>