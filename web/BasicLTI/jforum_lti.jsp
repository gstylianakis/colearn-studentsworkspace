<%@page import="java.util.Properties"%>
<%@page import="blackboard.blti.message.Role"%>
<%@page import="blackboard.blti.message.BLTIMessage"%>
<%@page import="general.BasicLtiConsumption"%>
<html>
<head>
  <title>IMS Basic Learning Tools Interoperability</title>
</head>
<body style="font-family:sans-serif">

<%@ page import="org.imsglobal.basiclti.BasicLTIUtil" %>
<%@ page import="javax.servlet.http.HttpServletRequest" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="net.oauth.OAuth" %>
<%@ page import="net.oauth.OAuthMessage" %>
<%@ page import="net.oauth.OAuthConsumer" %>
<%@ page import="net.oauth.OAuthAccessor" %>
<%@ page import="net.oauth.OAuthValidator" %>
<%@ page import="net.oauth.SimpleOAuthValidator" %>
<%@ page import="net.oauth.signature.OAuthSignatureMethod" %>
<%@ page import="net.oauth.server.HttpRequestMessage" %>
<%@ page import="net.oauth.server.OAuthServlet" %>
<%@ page import="net.oauth.signature.OAuthSignatureMethod" %>

<pre>
<%

/*  Enumeration en = request.getParameterNames();
  while (en.hasMoreElements()) {
    String paramName = (String) en.nextElement();
    out.println(paramName + " = " + request.getParameter(paramName) );
  }

  OAuthMessage oam = OAuthServlet.getMessage(request, null);

  OAuthValidator oav = new SimpleOAuthValidator();
  String oauth_consumer_key = request.getParameter("oauth_consumer_key");
  if ( oauth_consumer_key == null ) {
    out.println("<b>Missing oauth_consumer_key</b>\n");
    return;
  }
  OAuthConsumer cons = null;
  if ( "lmsng.school.edu".equals(oauth_consumer_key) ) {
    cons = new OAuthConsumer("http://call.back.url.com/", "lmsng.school.edu", "secret", null);
  } else if ( "12345".equals(oauth_consumer_key) ) {
    cons = new OAuthConsumer("http://call.back.url.com/", "12345", "secret", null);
  } else {
    out.println("<b>oauth_consumer_key="+oauth_consumer_key+" not found.</b>\n");
    return;
  }

  OAuthAccessor acc = new OAuthAccessor(cons);

  try {
    out.println("\n<b>Base Message</b>\n</pre><p>\n");
    
    out.println(OAuthSignatureMethod.getBaseString(oam));
    out.println("<pre>\n");
    oav.validateMessage(oam,acc);
    out.println("Message validated");
    response.sendRedirect("http://147.27.41.106:8084/jamwiki-1.0/en/StartingPoints?loginUsername='xenia'&loginPassword='xenia'");

  } catch(Exception e) {
    out.println("<b>Error while valdating message:</b>\n");
    out.println(e);
  }
*/
//sos sxolia
// gia kathe 

  //String secret = request.getParameter("secret");
  //String key = request.getParameter("key");
String key=null,secret=null;//ayta kanonika diavazontai apo tin vasi dedomenwn

  if ( key == null || key.compareTo("undefined")==0)
      key = "12345";
  String org_id = request.getParameter("org_id");

  if ( org_id == null || org_id.compareTo("undefined")==0)
      org_id = "lmsng.school.edu";
  String org_desc = request.getParameter("org_desc");

  if ( org_desc == null || org_desc.compareTo("undefined")==0)
      org_desc = "University of School";
  String org_secret = request.getParameter("org_secret");

  if ( (secret == null || secret.compareTo("undefined")==0) && org_secret == null)
      secret = "secret";
  if ( org_secret == null || org_secret.compareTo("undefined")==0)
      org_secret = "";

  String endpoint = request.getParameter("endpoint");
  if ( endpoint == null )
      endpoint = "http://147.27.41.129:8084/jforum/redirect2Jforum.jsp";
  String context_id = null;//request.getParameter("context_id");
  if(context_id==null || context_id.compareTo("undefined")==0)
    context_id="456434New";
  String context_title = request.getParameter("context_title");
  if(context_title==null)
      context_title = "no_title_";
  String context_label = request.getParameter("context_label");
  if(context_label==null)
      context_label = "no_label_specified";
  String email = request.getParameter("email");

  if(email==null)
      email="no_email_specified";
  String user_id = request.getParameter("user_id");

  if(user_id==null)
      user_id="11";
  String username = request.getParameter("username");
  if(username==null)
      username="1s";
  String resource_id = request.getParameter("resource_id");
  if(resource_id==null)
      resource_id = "working?A?";//prepei na einai integer opote ta team tha prepei na exoun id

  String resource_title = request.getParameter("resource_title");
  if(resource_title==null)
      resource_title = "testing forum capability";

  String resource_label = request.getParameter("resource_label");
  if(resource_label==null)
      resource_label = "no_label_given";

  String resource_description = request.getParameter("resource_description");
  if(resource_description==null)
      resource_description = "no_description_given";


  Properties postProp = new Properties();
  postProp.setProperty("resource_link_id",resource_id);
  postProp.setProperty("user_id",user_id);
  postProp.setProperty("roles","Instructor");
  postProp.setProperty("lis_person_name_given",username);
  postProp.setProperty("lis_person_contact_email_primary",email);
  postProp.setProperty("lis_person_sourcedid","tuc:user");
  postProp.setProperty("context_id",context_id);
  postProp.setProperty("context_title",context_title);
  postProp.setProperty("context_label",context_label);
  postProp.setProperty("resource_link_title",resource_title);
  postProp.setProperty("resource_link_label",resource_label);
  postProp.setProperty("resource_link_description",resource_description);

  //getLMSDummyData(postProp);


  String lmspwstr = request.getParameter("lmspw");
  boolean lmspw = lmspwstr == null || ! lmspwstr.equals("Resource");


  // Ignore the organizational info if this is not an LMS password
  if ( ! lmspw ) org_id = null;

  // Off to the races with BasicLTI...

  if ( org_secret.equals("") ) org_secret = null;
  postProp = BasicLTIUtil.signProperties(postProp, endpoint, "POST", key, secret, org_secret, org_id, org_desc);
  String postData = BasicLTIUtil.postLaunchHTML(postProp, endpoint, false);
  out.println(postData);
%>
</pre>

