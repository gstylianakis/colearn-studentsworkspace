<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="general.ActivityStructureElement"%>
<%@page import="org.json.JSONObject"%>
<%@page import="general.ActivityElement"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="java.io.StringReader"%>
<%@page import="org.xml.sax.InputSource"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>

<jsp:useBean id="parser" scope="page" class="general.XMLParser"/>
<%
    try {
        
        long start=0;
	workspaceintegrationservices.RuntimeWSService service = new workspaceintegrationservices.RuntimeWSService();
	workspaceintegrationservices.RuntimeWS port = service.getRuntimeWSPort();
	 // TODO initialize WS operation arguments here
	String username = request.getParameter("username");
        String run_id = request.getParameter("run_id");      
   
        // TODO process result here
        java.util.List<java.lang.Object> uols = port.listUolsForUser(username);  
        int uol = (Integer)(uols.toArray()[0]);
        java.util.List<java.lang.Object> runId = port.listRunsForUser(username);
        int runid = (Integer)(runId.toArray())[0];

        if(run_id!=null || run_id!=""){
            runid = Integer.parseInt(run_id);
            uol = Integer.parseInt(port.getUOLFromRunID(runid));
        }

        java.lang.String result = port.getLessonDescription(runid,uol);

        java.lang.String status = port.activitiesStatus(runid);

        result = result.replaceAll("\\?","!--");
        result = result.replaceAll("\\?","--");
  
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(result));
     

        Document doc = db.parse(is);
        Document status_doc = null;
        if(status!=null){
          
            status = status.replaceAll("\\?","!--");

            status = status.replaceAll("\\?","--");

            is.setCharacterStream(new StringReader(status));
            status_doc = db.parse(is);
        }
 
        String information = "no information specified for this lesson";
        
        //out.print(port.getMetadataForUol((Integer)uols.toArray()[0]));
        Node metadataElement = doc.getElementsByTagName("metadata").item(0);
        //NodeList activities = doc.getElementsByTagName("learning-activity");
        NodeList acts = doc.getElementsByTagName("act");
        NodeList users = doc.getElementsByTagName("user");
        Node lom = null;
       
        if(parser.getChildsElementNodeByName(metadataElement,"lom").size()!=0)            
            lom = (Node)(parser.getChildsElementNodeByName(metadataElement,"lom").get(0));
        //activity = parser.getChildElementNode(metadataElement);
  //System.out.println("to result einai "+result);
        
        if(lom!=null){            
            int size=0,prefixIndex=0;
            Node node_child = null;
            String node_name="";
            information="";            
            for(int i=0;i<lom.getChildNodes().getLength();i++){
                if(lom.getChildNodes().item(i).getNodeType()==1){
                    size = lom.getChildNodes().item(i).getChildNodes().getLength();
                    
                    for(int j=0;j<size;j++){                        
                        node_child = lom.getChildNodes().item(i).getChildNodes().item(j);
                        if(node_child.getNodeType()!=1)
                            continue;
                        node_name = node_child.getNodeName();
                        
                        prefixIndex = node_name.indexOf(":");
                        if(prefixIndex!=-1){
                            information += "<b>"+node_name.substring(prefixIndex+1,node_name.length()).toUpperCase()+":</b><br/>";                            
                            if(parser.getChildsElementNodeByName(node_child,"langstring").size()!=0){                                
                                information += ((Node)parser.getChildsElementNodeByName(node_child,"langstring").get(0)).getTextContent()+"<br/>";

                            }
                        }
                    }
                }
            }
        }

     start = System.currentTimeMillis();
        Hashtable activityStatus = new Hashtable();
        Hashtable visibilityStatus = new Hashtable();
        if(status_doc!=null){
            NodeList activityStatusNodes = status_doc.getElementsByTagName("activity");
            Node activityStatusNode = null;
            Node propValue = null;
            String isVisible = null;
            String value = "";
            for(int i=0;i<activityStatusNodes.getLength();i++){                
                activityStatusNode = (Node)activityStatusNodes.item(i);
                propValue = (Node)parser.getChildsElementNodeByName(activityStatusNode,"value").get(0);
                value = ((Node)parser.getChildsElementNodeByName(propValue,"completed").get(0)).getTextContent();
                isVisible = ((Node)parser.getChildsElementNodeByName(propValue,"isvisible").get(0)).getTextContent();
                activityStatus.put(activityStatusNodes.item(i).getAttributes().getNamedItem("id").getTextContent()+"-"+activityStatusNodes.item(i).getAttributes().getNamedItem("user").getTextContent(), value);
                visibilityStatus.put(activityStatusNodes.item(i).getAttributes().getNamedItem("id").getTextContent()+"-"+activityStatusNodes.item(i).getAttributes().getNamedItem("user").getTextContent(), isVisible);
            }
        }

        String phase_name = "";
        Node act = null;
        Hashtable actTable = new Hashtable();
        Vector actOrderTable = new Vector();

        Vector activityTable = new Vector();
        Hashtable ActActivities = new Hashtable();
        ArrayList role_parts = null;
        String act_id = "",activity_id="",role_id="",userid="",learning_id="",activity_strucutreId="",userType="";
        Node learning_activity = null;
        ArrayList activity_structures = null;
        Node activity_structure = null;
        ActivityStructureElement structure = null,structureTmp=null;
        Hashtable roleidTousername = new Hashtable();
        Hashtable usernames=new Hashtable();
        //Hashtable roleidTousertype = new Hashtable();

        ActivityElement activityElem = null;
        String further_information = "";
        for(int i=0;i<users.getLength();i++){
            userid = ((Node)parser.getChildsElementNodeByName(users.item(i),"userid").get(0)).getTextContent();
            role_id = ((Node)parser.getChildsElementNodeByName(users.item(i),"roleid").get(0)).getTextContent();
            //userType = ((Node)parser.getChildsElementNodeByName(users.item(i),"roleType").get(0)).getTextContent();
            ArrayList userNames = new ArrayList();
            
            if(!usernames.contains(userid))
                usernames.put(userid,role_id);

            if(!roleidTousername.containsKey(role_id)){
                userNames.add(userid);
                roleidTousername.put(role_id, userNames);
            }
            else
                ((ArrayList)roleidTousername.get(role_id)).add(userid);
        }
       
        if(acts!=null){

            for(int i=0;i<acts.getLength();i++){
                act = acts.item(i);            

                act_id = act.getAttributes().getNamedItem("identifier").getTextContent(); 
                role_id = act.getParentNode().getParentNode().getAttributes().getNamedItem("role").getTextContent();
          
                if(!actTable.containsKey(act_id)){
                    actOrderTable.add(act_id);
                    actTable.put(act_id, act);
                 }

               role_parts = parser.getChildsElementNodeByName(act,"role-part");
                Hashtable learningActivityInAct = new Hashtable();
                
                for(int j=0;j<role_parts.size();j++){
                    learning_activity = null;
                    activity_structures = null;

                    if(parser.getChildsElementNodeByName((Node)role_parts.get(j),"learning-activity").size()!=0){
                        learning_activity = (Node)parser.getChildsElementNodeByName((Node)role_parts.get(j),"learning-activity").get(0);
                        
                        activity_id = learning_activity.getAttributes().getNamedItem("identifier").getTextContent();

                        if(!activityTable.contains(activity_id)){
                           
                            activityTable.add(activity_id);
                            ActivityElement activity = new ActivityElement(role_id,learning_activity,activity_id);
                            if(ActActivities.containsKey(act_id)){
                                ((Hashtable)ActActivities.get(act_id)).put(activity_id,activity);
                            }
                            else{
                                learningActivityInAct.put(activity_id,activity);
                                ActActivities.put(act_id,learningActivityInAct);
                            }
                        }
                        else{
                            activityElem = (ActivityElement)(((Hashtable)ActActivities.get(act_id)).get(activity_id));
                            activityElem.getRoles().add(role_id);
                        }
                    }
                    if(parser.getChildsElementNodeByName((Node)role_parts.get(j),"activity-structure").size()!=0){                        
                        activity_structures = parser.getChildsElementNodeByName((Node)role_parts.get(j),"activity-structure");
                        for(int k=0;k<activity_structures.size();k++){
                            activity_structure = (Node)activity_structures.get(k);
                            activity_strucutreId = activity_structure.getAttributes().getNamedItem("identifier").getTextContent();
                            if(!activityTable.contains(activity_strucutreId)){
                                activityTable.add(activity_strucutreId);
                                ActivityStructureElement activityStructure = new ActivityStructureElement(role_id,activity_structure,activity_strucutreId);

                                if(ActActivities.containsKey(act_id))
                                    ((Hashtable)ActActivities.get(act_id)).put(activity_strucutreId,activityStructure);

                                else{
                                    learningActivityInAct.put(activity_strucutreId,activityStructure);
                                    ActActivities.put(act_id,learningActivityInAct);
                                }
                            }
                            else
                                ((ActivityStructureElement)(((Hashtable)ActActivities.get(act_id)).get(activity_strucutreId))).getRoles().add(role_id);
                        }
                    }
                    if(activity_structure==null && learning_activity==null)
                        continue;                    
                }                
            }

     
            Hashtable learningActivityInAct = new Hashtable();
            String usernameRows = "";
            String aboutNum = "";
            information += "<br/><b>About the process:</b>";
            information += "<p>The lesson is organized in "+actTable.size()+" phase(s)<br/>";
            information += "The description of each phase, the activities that includes and the user participation in each activity is displayed in tables bellow<br/><br/>";
            information += "<img src='./images/warning.gif'/>Note: To start an activity that several users participate, each user must have his previous activities completed <br/>";
            information +="Additionally, activities with multiple users are considered as completed only when all users have complete<br/><br/></p><p><img src='images/man-icon.png'/>:participant - not completed activity<br/><br/><img style='margin-right:8px' src='images/tick.png'/>:activity completed</p>";                        

            int activity_size = 0;
            int structure_size = 0;
            int odd_even = 0;
            String nameTmp = "";
            String classColumn="even";
            String classColumn2="";
            for(Iterator usernamesIt = usernames.keySet().iterator(); usernamesIt.hasNext();){
                usernameRows += "<th scope='col'>"+(String)usernamesIt.next()+"</th>";
            }

            Object[] phaseKeyIt = actTable.keySet().toArray();
            String structureTree="",structureTreeString="";
            ActivityElement tmpactivity = null;
            //for(int iterator=(phaseKeyIt.length-1);iterator>=0;iterator--){

            for(int iterator=0;iterator<actOrderTable.size();iterator++){
                //act_id = (String)phaseKeyIt[iterator];
                act_id = (String)actOrderTable.get(iterator);

                activity_size = 0;
                structure_size = 0;
                act = (Node)actTable.get(act_id);
                odd_even = 0;
                phase_name = ((Node)parser.getChildsElementNodeByName(act,"title").get(0)).getTextContent();                                          
                information +="<p><b>"+phase_name+" Phase:</b><br/>";
                learningActivityInAct = (Hashtable)ActActivities.get(act_id);

                //size = learningActivityInAct.values().size();
                //information += //phase_name+" Phase is organized in "+size+" activity(ies) and "+size+" structure(s)<br/></p>";
                further_information = "<table class='tableDef'><tr class='odd'><td></td>";
                further_information += usernameRows;//+"<th scope='col'>Status</th>";
                //for(Iterator activitiesIt = learningActivityInAct.values().iterator(); activitiesIt.hasNext();){

                  for(int k=0;k<activityTable.size();k++){
                    if(!learningActivityInAct.containsKey(activityTable.get(k)))
                        continue;
                    structureTreeString = "";
                    if(odd_even%2==0)
                        classColumn="even";
                    else
                        classColumn="odd";                    
                    odd_even++;
                    
                    //activityElem = (ActivityElement)activitiesIt.next();
                    activityElem = (ActivityElement)learningActivityInAct.get(activityTable.get(k));
                    if(activityElem.getType().compareTo("Structure")==0)
                        structure_size++;
                    else
                        activity_size++;

                    learning_activity = activityElem.getActivityNode();
                    learning_id = activityElem.getActivityId();
                    if(activityElem.getType().compareTo("Structure")==0){
                        structure = new ActivityStructureElement( activityElem.getActivityNode(),activityElem.getActivityId());
                        structureTree += "<ul id='structureTree' class='filetree'>"+
                                             "<li><span class='folder'>"+((Node)parser.getChildsElementNodeByName(learning_activity,"title").get(0)).getTextContent()+"</span><ul>";
                        structureTreeString = ((ActivityStructureElement)activityElem).getinnerActivities(activityElem.getActivityNode(), structure,structureTree);
                        structureTreeString += "</ul></li></ul>";
                     
                         
                    }
                    else
                        structureTreeString = activityElem.getActivityName();
                    
                    classColumn2 = "";

                    if(((String)visibilityStatus.get(learning_id+"-"+username)).compareTo("true")==0 && ((String)activityStatus.get(learning_id+"-"+username)).compareTo("true")!=0){
                        classColumn2 = "visible";
                    }

                    further_information +="<tr><td id='"+learning_id+"' class='"+classColumn+" "+classColumn2+"'>"+activityElem.getType()+": "+structureTreeString+"</td>";
            //System.out.println(activityElem.getActivityId()+" "+activityElem.getRoles());
                    for(Iterator usernamesIt = usernames.keySet().iterator(); usernamesIt.hasNext();){
                        nameTmp = (String)usernamesIt.next();
                        if(activityElem.getRoles().contains(usernames.get(nameTmp))){
//System.out.println(activityElem.getActivityId()+" "+activityElem.getRoles()+" "+activityStatus.get(learning_id+"-"+nameTmp));
                            if(activityStatus.get(learning_id+"-"+nameTmp)==null){
                                //further_information +="<td class='"+classColumn+"'><tr><td>ewfwef</td></tr></td>";
                                continue;
                            }
                            if(((String)activityStatus.get(learning_id+"-"+nameTmp)).compareTo("true")==0)
                                further_information +="<td id='"+learning_id+"-"+nameTmp+"' class='"+classColumn+"Tick'></td>";
                            else
                                further_information +="<td id='"+learning_id+"-"+nameTmp+"' class='"+classColumn+"Edit'></td>";
                                
                            
                        }
                        else
                            further_information +="<td class='"+classColumn+"'></td>";
                    }
                    //information +="<b>"+((Node)parser.getChildsElementNodeByName(learning_activity,"title").get(0)).getTextContent()+" Activity:</b><br/>";
                    //information +="<p>Users that participates to this activity</b><p/>";
                    further_information += "</tr>";
System.out.println("10_"+iterator+"_"+k+5);                    
                }
                aboutNum = "'"+phase_name+"' Phase is organized in "+activity_size+" activity(ies) and "+structure_size+" structure(s)<br/></p>";
                further_information+="</table>";
                information += aboutNum+further_information;

            } 
System.out.println("11");             
      }
long end = System.currentTimeMillis();


      //obj.put("description",information);
    //obj.put("num",new Integer(100));
        information = "<script>$('#accordion').accordion();</script>"+information;
      out.print(information);
      System.out.println("vgainw apo to description of uol");
    } catch (Exception ex) {        
	ex.printStackTrace();
    }
%>