<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="java.io.StringReader"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.xml.sax.InputSource"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%
    try {
	workspaceintegrationservices.RuntimeWSService service = new workspaceintegrationservices.RuntimeWSService();
	workspaceintegrationservices.RuntimeWS port = service.getRuntimeWSPort();
	 // TODO initialize WS operation arguments here
	java.lang.String username = request.getParameter("username");

        int runID = (Integer)port.listRunsForUser(username).get(0);

        java.util.List<java.lang.Object> uols = port.listUolsForUser(username);

	// TODO process result here
	java.lang.String result = port.userProfile(runID,(Integer)(uols.toArray()[0]),username);

	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(result));
        Document doc = db.parse(is);

        JSONObject user_data = new JSONObject();
        String fullName = doc.getElementsByTagName("Name").item(0).getTextContent()+" "+doc.getElementsByTagName("LastName").item(0).getTextContent();
        String email = doc.getElementsByTagName("email").item(0).getTextContent();
        String userType = doc.getElementsByTagName("roleType").item(0).getTextContent();
        user_data.put("fullName", fullName);
        user_data.put("email",email);
        user_data.put("roleType",userType);
     
        out.print(user_data);
    } catch (Exception ex) {
	ex.printStackTrace();
    }
%>
