<%-- 
    Document   : getAvailableLDs
    Created on : 11 ??? 2012, 6:36:13 ??
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="java.io.StringReader"%>
<%@page import="org.xml.sax.InputSource"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%
try {
        String result = "";
	workspaceintegrationservices.RuntimeWSService service = new workspaceintegrationservices.RuntimeWSService();
	workspaceintegrationservices.RuntimeWS port = service.getRuntimeWSPort();
        java.lang.String availableLDs = port.getAvailableLDs();
       
        availableLDs = availableLDs.replaceAll("\\?","!--");
        availableLDs = availableLDs.replaceAll("\\?","--");

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(availableLDs));
        Document doc = db.parse(is);
        NodeList lds = doc.getElementsByTagName("LD");

        Node title;
        for(int i=0;i<lds.getLength();i++){
            title = lds.item(i);
            result += "<tr><td "+title.getAttributes().item(0)+" width=\"300px\" style=\"cursor:pointer\" >"+title.getTextContent()+"</td></tr>";
            
        }
        JSONObject available_lds = new JSONObject();
	available_lds.put("count", lds.getLength());
        available_lds.put("result", result);
        out.print(available_lds);
       

    } catch (Exception ex) {
	ex.printStackTrace();
    }
%>