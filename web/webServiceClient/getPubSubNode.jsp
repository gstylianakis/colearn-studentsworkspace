<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
	workspaceintegrationservices.RuntimeWSService service = new workspaceintegrationservices.RuntimeWSService();
	workspaceintegrationservices.RuntimeWS port = service.getRuntimeWSPort();
	 
        // TODO initialize WS operation arguments here
	java.lang.String username = request.getParameter("username");
        java.lang.String run_id = request.getParameter("run_id");
System.out.println("getPubSubNode "+username+" run_id"+run_id);        
	java.util.List<java.lang.Object> result = port.listRunsForUser(username);
	if(run_id!=null)
            out.print(port.getNodeOfTeam(Integer.parseInt(run_id)));
        else
            out.print(port.getNodeOfTeam((Integer)result.toArray()[0]));
    } catch (Exception ex) {
	ex.printStackTrace();
        throw ex;
    }
    %>