<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="java.io.StringReader"%>
<%@page import="org.xml.sax.InputSource"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>

<%
    try {
	workspaceintegrationservices.RuntimeWSService service = new workspaceintegrationservices.RuntimeWSService();
	workspaceintegrationservices.RuntimeWS port = service.getRuntimeWSPort();
	 // TODO initialize WS operation arguments here
	String username = request.getParameter("username");

        int runId = Integer.parseInt(request.getParameter("runId"));
        java.util.List<java.lang.Object> uols = port.listUolsForUser(username);
	// TODO process result here
	java.lang.String result = port.getGlobalProperties(String.valueOf((Integer)(uols.toArray()[0])));
	result = result.replaceAll("\\?","!--");
        result = result.replaceAll("\\?","--");
        
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(result));
        Document doc = db.parse(is);

        JSONObject globalProp = new JSONObject();
        JSONArray globalProps = new JSONArray();
        NodeList titles = doc.getElementsByTagName("title");
       
        Node title;
        Node defaultValue;
        Node prop;
        
//username=xenia&runId=103&uolId=105&propertyId=globprop-1351102344012
        for(int i=0;i<titles.getLength();i++){

            title = titles.item(i);
            if(title.getTextContent().compareTo("isusedTofinishphaseAct")!=0){
                prop = title.getParentNode().getParentNode().getParentNode();
                globalProp.put("propertyId",prop.getChildNodes().item(0).getTextContent());
                globalProp.put("propertyName",title.getTextContent());
                globalProp.put("propertyType",prop.getChildNodes().item(1).getTextContent());
                globalProp.put("propertyValue",port.retrieveProperty(prop.getChildNodes().item(0).getTextContent(), runId, (Integer)(uols.toArray()[0]), username));
                 
       
                globalProps.put(globalProp);
            }
            
            //System.out.println(defaultValue.getChildNodes().item(1));
        }
        
        out.print(globalProps);

    } catch (Exception ex) {
	ex.printStackTrace();
    }
%>
