<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
	workspaceintegrationservices.RuntimeWSService service = new workspaceintegrationservices.RuntimeWSService();
	workspaceintegrationservices.RuntimeWS port = service.getRuntimeWSPort();

        java.lang.String activityIdentifier = request.getParameter("activityId");
	java.lang.String activityType = request.getParameter("activityType");
	int runId = Integer.parseInt(request.getParameter("runId"));
	java.lang.String username = request.getParameter("username");
        //java.lang.String rolePartId = request.getParameter("rolePartId");
	//port.completeActivity(activityIdentifier, activityType, runId, username);

        boolean res = port.completeActivity(activityIdentifier,activityType,runId,username);
        if(res==false)
            throw new Exception("error while requesting to complete activity");
        
        
    } catch (Exception ex) {
	System.out.print(ex);
    }
%>
