<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.HashSet"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Hashtable"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.xml.sax.InputSource"%>
<%@page import="java.io.StringReader"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="general.XMLParser"%>
<%try {
        JSONObject endActivityAfterPeriod = new JSONObject();
	workspaceintegrationservices.RuntimeWSService service = new workspaceintegrationservices.RuntimeWSService();
	workspaceintegrationservices.RuntimeWS port = service.getRuntimeWSPort();
	
        int runId = Integer.parseInt(request.getParameter("runId"));
        String username = request.getParameter("username");
	//String activityId = request.getParameter("activityId");
	XMLParser parser = new XMLParser();
        java.util.List<java.lang.Object> uols = port.listUolsForUser(username);


        java.lang.String actions = port.getConditions((Integer)(uols.toArray()[0]));
        java.lang.String result = port.getLessonDescription(runId,(Integer)(uols.toArray()[0]));
        result = result.replaceAll("\\?","!--");
        result = result.replaceAll("\\?","--");
        DocumentBuilderFactory dbfR = DocumentBuilderFactory.newInstance();
        DocumentBuilder dbr = dbfR.newDocumentBuilder();
        InputSource isR = new InputSource();
        isR.setCharacterStream(new StringReader(result));

        Document doc = dbr.parse(isR);
        NodeList learningActivities = doc.getElementsByTagName("learning-activity");
        Hashtable learningActivitiesID_title = new Hashtable();
        String activityId="",activityTitle="";
        Node activity = null, activityTitleNode=null;
        for(int i=0;i<learningActivities.getLength();i++){
            activity = (Node)(learningActivities.item(i));
            activityId = (activity.getAttributes().getNamedItem("identifier")).getTextContent();
            activityId = activityId.replaceAll("-","");
            if(parser.getChildsElementNodeByName(activity,"title").size()!=0){
                activityTitleNode = (Node)(parser.getChildsElementNodeByName(activity,"title").get(0));
                activityTitle = activityTitleNode.getChildNodes().item(0).getTextContent();
            }
            learningActivitiesID_title.put(activityId, activityTitle);

        }

        String UOLstartTime = port.getUOLStartTime(runId);
        int startYearIndex = UOLstartTime.indexOf("-");
        int startYear = Integer.parseInt(UOLstartTime.substring(0,startYearIndex));

        int startMonthIndex = UOLstartTime.indexOf("-",startYearIndex+1);
        int startMonth = Integer.parseInt(UOLstartTime.substring(startYearIndex+1,startMonthIndex));

        int startDayIndex = UOLstartTime.indexOf(" ",startMonthIndex+1);
        int startDay = Integer.parseInt(UOLstartTime.substring(startMonthIndex+1,startDayIndex));

        int startHourIndex = UOLstartTime.indexOf(":",startDayIndex+1);
        int startHour = Integer.parseInt(UOLstartTime.substring(startDayIndex+1,startHourIndex));

        int startMinutesIndex = UOLstartTime.indexOf(":",startHourIndex+1);
        int startMinute = Integer.parseInt(UOLstartTime.substring(startHourIndex+1,startMinutesIndex));
   //System.out.println(startYear+" "+startMonth+" "+startDay+" "+startHour+" "+startMinute);

        actions = actions.replaceAll("\\?","!--");
        actions = actions.replaceAll("\\?","--");
System.out.println("to conditions einai "+actions);

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(actions));
        Document actionsDoc = db.parse(is);
        NodeList conditionsElem = actionsDoc.getElementsByTagName("time-unit-of-learning-started");
        Node propertyEndPeriodValue = null, condition = null;
        String activityIDToBeEndedAfterPeriod = "-1";
        String periodToCompleteActivity = "";
        int year = 0, month = 0, day = 0, hour = 0, minutes = 0;
        String toEnd = "00";

        for(int i=0;i<conditionsElem.getLength();i++){
            //paw na dw pio tha einia to xroniko diastima pou teleiwnei to activity...
            propertyEndPeriodValue = (conditionsElem.item(i).getParentNode()).getLastChild();
            periodToCompleteActivity = propertyEndPeriodValue.getTextContent();
            year = Integer.parseInt(periodToCompleteActivity.substring(1,periodToCompleteActivity.indexOf("Y")));
            month = Integer.parseInt(periodToCompleteActivity.substring(periodToCompleteActivity.indexOf("Y")+1,periodToCompleteActivity.indexOf("M")));
            day = Integer.parseInt(periodToCompleteActivity.substring(periodToCompleteActivity.indexOf("M")+1,periodToCompleteActivity.indexOf("D")));
            hour = Integer.parseInt(periodToCompleteActivity.substring(periodToCompleteActivity.indexOf("T")+1,periodToCompleteActivity.indexOf("H")));
            minutes = Integer.parseInt(periodToCompleteActivity.substring(periodToCompleteActivity.indexOf("H")+1,periodToCompleteActivity.lastIndexOf("M")));
            toEnd = (startYear+year)+"-"+(startMonth+month)+"-"+(startDay+day)+" "+(startHour+hour)+":"+(startMinute+minutes);
            condition = (conditionsElem.item(i).getParentNode()).getParentNode().getParentNode().getParentNode();
            activityIDToBeEndedAfterPeriod = condition.getLastChild().getFirstChild().getAttributes().getNamedItem("ref").getNodeValue();
            activityIDToBeEndedAfterPeriod = activityIDToBeEndedAfterPeriod.replaceAll("-","");
            
            activityTitle = (String)learningActivitiesID_title.get(activityIDToBeEndedAfterPeriod);
            toEnd += ","+activityTitle;
            endActivityAfterPeriod.put(activityIDToBeEndedAfterPeriod,toEnd);
        }
        System.out.println(endActivityAfterPeriod);
       // System.out.println(endActivityAfterPeriod.length());
        out.print(endActivityAfterPeriod);
    } catch (Exception ex) {
	ex.printStackTrace();}
%>