<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="org.xml.sax.InputSource"%>
<%@page import="java.io.StringReader"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="com.sun.msv.datatype.xsd.ngimpl.DataTypeLibraryImpl"%>
<%try {
	workspaceintegrationservices.RuntimeWSService service = new workspaceintegrationservices.RuntimeWSService();
	workspaceintegrationservices.RuntimeWS port = service.getRuntimeWSPort();
	 // TODO initialize WS operation arguments here
	int runId = Integer.parseInt(request.getParameter("runId"));
        String dataType = request.getParameter("dataType");
        String ldId = request.getParameter("ldId");
	// TODO process result here
	java.lang.String status = port.getLDElementStatus(dataType,ldId,runId);	
        Document status_doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        if(status!=null){
            status = status.replaceAll("\\?","!--");
            status = status.replaceAll("\\?","--");
            is.setCharacterStream(new StringReader(status));
            status_doc = db.parse(is);
        }
        String finished = "true";
        NodeList completedStatus = status_doc.getElementsByTagName("completed");
        if(completedStatus.getLength()==0)
            finished="false";
        for(int i=0;i<completedStatus.getLength();i++){
            if(completedStatus.item(i).getTextContent().compareTo("false")==0){
                finished="false";
                break;
            }
        }
        out.print(finished);
        System.out.println(finished);
    } catch (Exception ex) {
	// TODO handle custom exceptions here
    }%>
