<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="java.io.StringReader"%>
<%@page import="org.xml.sax.InputSource"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<jsp:useBean id="parser" scope="page" class="general.LDElementParser"/>
<%
    try {
	workspaceintegrationservices.RuntimeWSService service = new workspaceintegrationservices.RuntimeWSService();
	workspaceintegrationservices.RuntimeWS port = service.getRuntimeWSPort();

        java.lang.String username = request.getParameter("username");
        
        
        String result = "";
        HashMap uolRunid = new HashMap();
        
        for(Object run_id: port.listRunsForUser(username)){
            uolRunid.put(Integer.parseInt(port.getUOLFromRunID((Integer)run_id)),
                    (Integer)run_id);
        }
        service = new workspaceintegrationservices.RuntimeWSService();
	port = service.getRuntimeWSPort();
        java.lang.String availableLDs = port.getAvailableLDs();
       
        availableLDs = availableLDs.replaceAll("\\?","!--");
        availableLDs = availableLDs.replaceAll("\\?","--");

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(availableLDs));
        Document doc = db.parse(is);
        NodeList lds = doc.getElementsByTagName("LD");
        Node title;
        int id = 0;
        int count = 0;
        for(int i=0;i<lds.getLength();i++){
            title = lds.item(i);
            id = Integer.parseInt(title.getAttributes().getNamedItem("id").getNodeValue());             

            if(uolRunid.get(id) == null)
                continue;
            
            count++;
            result += "<tr><td id='"+uolRunid.get(id)+"' width=\"300px\" style=\"cursor:pointer\" >"+title.getTextContent()+"</td></tr>";
            
        }
        JSONObject available_lds = new JSONObject();
	available_lds.put("count", count);
        available_lds.put("result", result);
        out.print(available_lds);
    } catch (Exception ex) {
	ex.printStackTrace();
    }
%>