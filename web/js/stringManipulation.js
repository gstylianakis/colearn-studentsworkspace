/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


   function StringtoXML(text){
                try{
                    if (window.ActiveXObject){
                      var doc=new ActiveXObject('Microsoft.XMLDOM');
                      doc.async='false';
                      doc.loadXML(text);
                    } else {
                      var parser=new DOMParser();
                      var doc = parser.parseFromString(text,'text/xml');
                    }
                }
                catch(e){
                    alert("error while loading manifest "+e);
                }
                return doc;
            }

function getYouTubeVideoId(src){
    if(src.indexOf("youtube")==-1)
        return -1;
    var id = src.substr(src.indexOf("=")+1, src.length)
    return id;
}