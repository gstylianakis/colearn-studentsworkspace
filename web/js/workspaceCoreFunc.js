/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var $completionConfirm;
function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }  
$(document).ready(function() {
    $completionConfirm =
        $("<div></div>").html("This activity is collaborative. Other students haven't finished. You want to proceed anyway?").dialog({
        resizable: false,
        height:140,
        modal: true,
        autoOpen:false,
        zIndex: 10000,
        buttons: {
            "Yes": function() {                
                getActivityTree(activityTree.username,activityTree.runId);
                
                //twra den to kanw publish to complete alla to kanw apo to engine, giati me to publish an den eisai sindemenos ekeini ti stigmh iparxei provlima
                activityTree.publish_action({
                    fromUser: activityTree.username,
                    service:"getActivity",
                    activityId: organizeData.activityId,
                    activityTitle:organizeData.activityTitle,
                    rolePartId: organizeData.rolePartId,
                    msg:"user "+activityTree.username+" finished activity "+organizeData.activityTitle+" and proceeded to the next one"
                });
                checkTotalStatus("learning-activity",activityTree.runId, organizeData.activityId)
                $( this ).dialog( "close" );
            },
            "No": function(){
                var tree = Ext.getCmp('phases-tree');               
                tree.getNodeById(organizeData.activityId).attributes['qtip']= 'activity is finished'
                tree.root.reload();
                
                activityTree.publish_action({
                    fromUser: activityTree.username,
                    service:"getActivity",
                    activityId: organizeData.activityId,
                    activityTitle:organizeData.activityTitle,
                    rolePartId: organizeData.rolePartId,
                    msg:"user "+activityTree.username+" finished activity "+organizeData.activityTitle+" and waits you all to complete it",
                    option:"notGetActivity"
                });
                $( this ).dialog( "close" );
            }
        }
    });
})
var username;

var groupCalendar = {
    connection: null,
    service: null,
    node: null,
    username:null,
    calendarPersistElem:[],   
    NS_DATA_FORMS: "jabber:x:data",
    NS_PUBSUB: "http://jabber.org/protocol/pubsub",
    NS_PUBSUB_OWNER: "http://jabber.org/protocol/pubsub#owner",
    NS_PUBSUB_ERRORS: "http://jabber.org/protocol/pubsub#errors",
    NS_PUBSUB_NODE_CONFIG: "http://jabber.org/protocol/pubsub#node_config",
    on_event: function (msg) {
        try{
           
            if ($(msg).find('x').length > 0) {
                var service = $(msg).find('field[var="service"] value').text();
                var index = -1;
                if(service=="calendar_newEvent" || service=="calendar_deleteEvent"){
                    var fromUser = $(msg).find('field[var="fromUser"] value').text();
                    var userName = $(msg).find('field[var="username"] value').text();
                    var title = $(msg).find('field[var="title"] value').text();
                    var start = $(msg).find('field[var="start"] value').text();
                    var end = $(msg).find('field[var="end"] value').text();
                    var allDay = $(msg).find('field[var="allDay"] value').text();
                    var event_id = $(msg).find('field[var="event_id"] value').text();
                    if(service!="calendar_deleteEvent"){
                         groupCalendar.calendarPersistElem.push({cal_title:title,cal_start:start,cal_end:end,cal_allDay:allDay,cal_event_id:event_id});

                    }
                    else{
                        index = groupCalendar.calendarPersistElem.indexOfEventID(event_id)//groupCalendar.indexOfPersistedItems.get(event_id)                        
                        if(index!=-1)
                            groupCalendar.calendarPersistElem.splice(index,1);
                    }
                    if(fromUser!=groupCalendar.connection.jid){
                        if(calendar!=null){
                            if(allDay=='false')
                                var allDayBool = false;
                            else
                                allDayBool = true;
                            if(service=="calendar_newEvent"){                                
                                $('#calendar').fullCalendar('renderEvent',{
                                    id:event_id,
                                    title: title,
                                    start: start,
                                    end: end,
                                    allDay: allDayBool
                                },
                                true // make the event "stick"

                                );
                            }
                            else if(service=="calendar_deleteEvent"){
                                calendar.fullCalendar('removeEvents',event_id);
                            }
                        }
                        $().toastmessage('showToast', {
                            text     : "user "+userName+" made a change on project dates",
                            sticky   : true,
                            position : 'top-right',
                            type     : 'notice',
                            closeText: ''
                        });
                    }
                }
            }
        }
        catch(e){
            console.log(e);
        }
        return true;
    },
    publish_action: function (action) {
        try{           
            groupCalendar.connection.sendIQ(
                $iq({to: groupCalendar.service, type: "set"})
                    .c('pubsub', {xmlns: groupCalendar.NS_PUBSUB})
                    .c('publish', {node: groupCalendar.node})
                    .c('item')
                    .c('x', {xmlns: groupCalendar.NS_DATA_FORMS,
                             type: "result"})
                    .c('field', {"var": "fromUser"})
                    .c('value').t(groupCalendar.connection.jid)
                    .up().up()
                    .c('field', {"var": "event_id"})
                    .c('value').t('' +action.event_id)
                    .up().up()
                    .c('field', {"var": "username"})
                    .c('value').t('' +action.username)
                    .up().up()
                    .c('field', {"var": "title"})
                    .c('value').t('' + action.title)
                    .up().up()
                    .c('field', {"var": "start"})
                    .c('value').t('' + action.start)
                    .up().up()
                    .c('field', {"var": "end"})
                    .c('value').t('' + action.end)
                    .up().up()
                    .c('field', {"var": "allDay"})
                    .c('value').t('' + action.allDay)
                    .up().up()
                    .c('field', {"var": "service"})
                    .c('value').t('' + action.service)
                    
                );
            }
            catch(e){
                alert("error while trying to inform about changes in dates " +e);
            }


    },
    showPersistItems:function(){
        var item;        
        if(calendar!=null){
            console.log(groupCalendar.calendarPersistElem.length)
            for(var i=0;i<groupCalendar.calendarPersistElem.length;i++){
                item=groupCalendar.calendarPersistElem[i];
                if(item.cal_allDay=='false')
                    var allDayBool = false;
                else
                    allDayBool = true;
        //alert(item.cal_start+ " " +item.cal_end)
                        $('#calendar').fullCalendar('renderEvent',{
                                    id:item.cal_event_id,
                                    title: item.cal_title,
                                    start: item.cal_start,
                                    end: item.cal_end,
                                    allDay: allDayBool
                        },
                        true );//make event stick
                     
                     
            }
        }
    }
}
var workspaceMicroblogging = {
    connection: null,
    service: null,
    node: null,
    username:null,
    password:null,
    PersistElem:[],
    NS_DATA_FORMS: "jabber:x:data",
    NS_PUBSUB: "http://jabber.org/protocol/pubsub",
    NS_PUBSUB_OWNER: "http://jabber.org/protocol/pubsub#owner",
    NS_PUBSUB_ERRORS: "http://jabber.org/protocol/pubsub#errors",
    NS_PUBSUB_NODE_CONFIG: "http://jabber.org/protocol/pubsub#node_config",

    on_event: function (msg) {
        try{
          
            if ($(msg).find('x').length > 0) {
 
 
 //console.log("to message "+$(msg).find('field[var="message"] value').text());

                var service = $(msg).find('field[var="service"] value').text();
                var delayed = $(msg).children("delay").length > 0 || $(msg).children("x[xmlns='jabber:x:delay']").length > 0;
                var info_name = $(msg).find('field[var="info_name"] value').text(),
                info_size = $(msg).find('field[var="info_size"] value').text();
                var message = $(msg).find('field[var="message"] value').text();
                var date = $(msg).find('field[var="date"] value').text();
//console.log("deleyed "+delayed+" "+delayed.html())
                if(service==="microblogging"){
 ///console.log($(msg).text());
 console.log(Strophe.serialize(msg));
 console.log(Strophe.serialize($(msg).textContent));
 console.log($(msg).toString());
 console.log($(msg));
 console.log("ston kombo "+workspaceMicroblogging.node);
                    var from = $(msg).find('field[var="fromUser"] value').text();                    
                    var d = new Date();
                    var html= "<ul id='comments'><li><p class='info'>Added on "+date+" from "+from+" :</p>"+
                              "<div class='body'><p>"+message+"</p>"+
                              "</div></li></ul>";
                    $('#messagewindow').append(html);
                    workspaceMicroblogging.PersistElem.push({service:"microblogging",date:date,from:from,message:message});
                }
                else if(service=="fileShareInBlogging"){
                     from = $(msg).find('field[var="fromUser"] value').text();
                     d = new Date();

                     html= "<ul id='comments'><li><p class='info'>File Added on "+date+" from "+from+" :</p>"+
                              "<div class='body'><p>"+info_name+"<input type='button' onclick=workspaceMicroblogging.downloadFile('"+message+"','"+info_name+"') value='download'/></p>"+
                              "</div></li></ul>";
                    $('#messagewindow').append(html);
                    workspaceMicroblogging.PersistElem.push({service:"fileShareInBlogging",date:date,from:from,info_name:info_name,message:message});
                }
            }
        }
        catch(e){
            console.log(e);
        }
        return true;
    },

    // subscription callbacks
    subscribed: function (iq) {
        //console.log("edw2 "+Strophe.serialize(iq));
        Ext.Msg.alert("you are ready to communicate with your collegues");
    },
    
    subscribe_error: function (iq) {        
        var error = workspaceMicroblogging.make_error_from_iq(iq);
        if(error.indexOf("item-not-found")!=-1){
            var createiq = $iq({to: workspaceMicroblogging.service,type: "set"})
                            .c('pubsub', {xmlns: workspaceMicroblogging.NS_PUBSUB})
                            .c('create',{node:workspaceMicroblogging.node});

            workspaceMicroblogging.connection.sendIQ(createiq,workspaceMicroblogging.created,workspaceMicroblogging.create_error);
            return;
        }
        
       workspaceMicroblogging.show_error("Subscription failed with " +error);
        
    },
    publish_error: function (iq) {
        workspaceMicroblogging.show_error("publish failed with " +
                              workspaceMicroblogging.make_error_from_iq(iq));
    },

    // error handling helpers
    make_error_from_iq: function (iq) {
        var error = $(iq)
            .find('*[xmlns="' + Strophe.NS.STANZAS + '"]')
            .get(0).tagName;
        var pubsub_error = $(iq)
            .find('*[xmlns="' + workspaceMicroblogging.NS_PUBSUB_ERRORS + '"]');
        if (pubsub_error.length > 0) {
            error = error + "/" + pubsub_error.get(0).tagName;
        }

        return error;
    },

    show_error: function (msg) {
        workspaceMicroblogging.connection.disconnect();
        workspaceMicroblogging.connection = null;
        workspaceMicroblogging.service = null;
        workspaceMicroblogging.node = null;
        $('#error_dialog p').text(msg);
        $('#error_dialog').dialog('open');
    },

    // node creation callbacks
    created: function () {        
        var configiq = $iq({to: workspaceMicroblogging.service,
                            type: "set"})
            .c('pubsub', {xmlns: workspaceMicroblogging.NS_PUBSUB_OWNER})
            .c('configure', {node: workspaceMicroblogging.node})
            .c('x', {xmlns: workspaceMicroblogging.NS_DATA_FORMS,
                     type: "submit"})
            .c('field', {"var": "FORM_TYPE", type: "hidden"})
            .c('value').t(workspaceMicroblogging.NS_PUBSUB_NODE_CONFIG)
            .up().up()
            .c('field', {"var": "pubsub#deliver_payloads"})
            .c('value').t("1")
            .up().up()
            .c('field', {"var": "pubsub#send_last_published_item"})
            .c('value').t("never")
            .up().up()
            .c('field', {"var": "pubsub#persist_items"})
            .c('value').t("1")
            .up().up()
            .c('field', {"var": "pubsub#max_items"})
            .c('value').t("1000")
            .up().up()
            .c('field', {"var": "pubsub#publish_model"})
            .c('value').t("subscribers")
            .up().up()
            .c('field', {"var": "pubsub#item_expire"})
            .c('value').t("999999999999");
            

            workspaceMicroblogging.connection.sendIQ(configiq,
                                     workspaceMicroblogging.configured,
                                     workspaceMicroblogging.configure_error);

    },

    create_error: function (iq) {
        var error = workspaceMicroblogging.make_error_from_iq(iq);
        if(error.indexOf("conflict")!=-1){
            alert("conflict because node existance")
            workspaceMicroblogging.subscribe();
            return;
        }
        
        workspaceMicroblogging.show_error("microblogging creation failed with " +error);
    },

    configured: function (iq) {        
        Ext.Msg.alert('ready to contact your colleques');
       //console.log("mallon edw "+Strophe.serialize(iq))
        //workspaceMicroblogging.subid =
    },

    configure_error: function (iq) {
        workspaceMicroblogging.show_error("microblogging configuration failed with " +
                              workspaceMicroblogging.make_error_from_iq(iq));
    },

    publish_action: function (action) {
            try{
            var date = new Date();
            workspaceMicroblogging.connection.sendIQ(
                $iq({to: workspaceMicroblogging.service, type: "set"})
                    .c('pubsub', {xmlns: workspaceMicroblogging.NS_PUBSUB})
                    .c('publish', {node: workspaceMicroblogging.node})
                    .c('item')
                    .c('x', {xmlns: workspaceMicroblogging.NS_DATA_FORMS,
                             type: "result"})
                    .c('field', {"var": "fromUser"})
                    .c('value').t(action.fromUser)
                    .up().up()
                    .c('field', {"var": "info_name"})
                    .c('value').t('' +action.info_name)
                    .up().up()
                    .c('field', {"var": "info_size"})
                    .c('value').t('' +action.info_size)
                    .up().up()
                    .c('field', {"var": "message"})
                    .c('value').t('' + action.message)
                    .up().up()
                    .c('field', {"var": "service"})
                    .c('value').t('' + action.service)
                    .up().up()
                    .c('field', {"var": "date"})
                    .c('value').t('' +date.getDate()+"/"+date.getMonth() ),null,workspaceMicroblogging.publish_error);
            }
            catch(e){
                alert(e);
            }


    },

    disconnect: function () {       
        workspaceMicroblogging.connection.disconnect();
        workspaceMicroblogging.connection = null;
        workspaceMicroblogging.service = null;
        workspaceMicroblogging.node = null;
        //$('#login_dialog').dialog('open');
    },
    discoverNodes: function(option){
       var subiq = $iq({to: workspaceMicroblogging.service,
                         type: "get",
                         id:'subscriptions1'
                     })
                        .c('pubsub', {xmlns: workspaceMicroblogging.NS_PUBSUB})
                        .c('subscriptions',{node:workspaceMicroblogging.node});

            if(option==="getSubid")
                workspaceMicroblogging.connection.sendIQ(subiq,
                                     workspaceMicroblogging.get_nodes,
                                     workspaceMicroblogging.discover_error);
            else
                workspaceMicroblogging.connection.sendIQ(subiq,
                                     workspaceMicroblogging.show_nodes,
                                     workspaceMicroblogging.discover_error);

    },
    show_nodes: function(iq){
        var exists = null;        
        $(iq).find("subscription").each(function(){            
            if(workspaceChat.jid_to_id($(this).attr("jid"))==workspaceChat.jid_to_id(workspaceMicroblogging.connection.jid)){                
                if($(this).attr("node")===workspaceMicroblogging.node){
                    exists = true;                                     
                    return false;
                }
            }
        })        
        if(exists===null)
            workspaceMicroblogging.subscribe();
    },
    get_nodes: function(iq){
        workspaceMicroblogging.subid = -1 ;
        $(iq).find("subscription").each(function(){
            if(workspaceChat.jid_to_id($(this).attr("jid"))==workspaceChat.jid_to_id(workspaceMicroblogging.connection.jid) && $(this).attr("node")==workspaceMicroblogging.node){
                workspaceMicroblogging.subid = $(this).attr("subid")                
            }
        })
        getPersistItems(workspaceMicroblogging.subid)
    },
    discover_error: function(iq){
       workspaceMicroblogging.show_error("microblogging configuration failed with " +
                              workspaceMicroblogging.make_error_from_iq(iq));

    },
    subscribe: function(){
        try{
            //var jid = workspaceChat.jid_to_id(workspaceMicroblogging.connection.jid)
            //alert("full jid "+workspaceMicroblogging.connection.jid+" kai jid "+jid)

            var subiq = $iq({to: workspaceMicroblogging.service,
                         type: "set"})
                        .c('pubsub', {xmlns: workspaceMicroblogging.NS_PUBSUB})
                        .c('subscribe', {node: workspaceMicroblogging.node,
                                         jid: Strophe.getBareJidFromJid(workspaceMicroblogging.connection.jid)                                         
                                        });
            workspaceMicroblogging.connection.sendIQ(subiq,
                                     workspaceMicroblogging.subscribed,
                                     workspaceMicroblogging.subscribe_error);
        }
        catch(e){
            alert("error while subscribing "+e)
        }
    },
    sharing: function(){
        try{
console.log("trasfering")
        }
        catch(e){
            console.log("error while tranfering file "+e);
        }
    },
    sharingError:function(iq){
        console.log("eskase ")
        workspaceMicroblogging.show_error("sharing failed with " +
                              workspaceMicroblogging.make_error_from_iq(iq));
    },
    downloadFile:function(msg,filename){
              
        upload.toServer(filename,msg)
        //window.location = "downloadFileFromServer.jsp?tmp_filename="+uniqid()+filename+"&filename="+filename+"&content="+msg;
        
        /*
        $('<iframe id="fileFrame" ></iframe>').appendTo("body");
        var fileName = filename+".txt";
        var eIFRAME=document.getElementById("fileFrame");
        var eFrameDocument=eIFRAME.contentWindow.document;
        eFrameDocument.open();
        eFrameDocument.write(msg);
        eFrameDocument.execCommand('SaveAs',null,'C:\test_java.text');
        */
    },
    persistItemsResponse: function(msg){
        var service="",info_name="",info_size="",message="",date="",from="",html="";
        var fromUser,title,start,end,allDay,event_id,cal_item;
        var tmp_calendarNodeDeleted = [];
        $(msg).find("item").each(function(){
            //console.log($(this))
            service = $(this).find('field[var="service"] value').text();
            info_name = $(this).find('field[var="info_name"] value').text(),
            info_size = $(this).find('field[var="info_size"] value').text();
            message = $(this).find('field[var="message"] value').text();
            date = $(this).find('field[var="date"] value').text();
            from = $(this).find('field[var="fromUser"] value').text();
           
            if(service=="microblogging"){
                    
                    /*html= "<ul id='comments'><li><p class='info'>Added on "+date+" from "+from+" :</p>"+
                              "<div class='body'><p>"+message+"</p>"+
                              "</div></li></ul>";*/
                    workspaceMicroblogging.PersistElem.push({service:"microblogging",date:date,from:from,message:message});
                    //$('#messagewindow').append(html);
            }
            else if(service=="fileShareInBlogging"){                
                /*html= "<ul id='comments'><li><p class='info'>File Added on "+date+" from "+from+" :</p>"+
                              "<div class='body'><p>"+info_name+"<input type='button' onclick=workspaceMicroblogging.downloadFile('"+message+"','"+info_name+"') value='download'/></p>"+
                              "</div></li></ul>";*/
                 workspaceMicroblogging.PersistElem.push({service:"fileShareInBlogging",date:date,from:from,info_name:info_name,message:message});
                //$('#messagewindow').append(html);
           }
           else if(service=="calendar_newEvent"){
                    //fromUser = $(msg).find('field[var="fromUser"] value').text();
                    title = $(this).find('field[var="title"] value').text();
                    start = $(this).find('field[var="start"] value').text();
                    end = $(this).find('field[var="end"] value').text();
                    allDay = $(this).find('field[var="allDay"] value').text();
                    event_id = $(this).find('field[var="event_id"] value').text();
                    groupCalendar.calendarPersistElem.push({cal_title:title,cal_start:start,cal_end:end,cal_allDay:allDay,cal_event_id:event_id});                    

           }
           else if(service=="calendar_deleteEvent"){
                tmp_calendarNodeDeleted.push($(this).find('field[var="event_id"] value').text())                
           }
        })
        var index = -1;
        for(var k=0;k<tmp_calendarNodeDeleted.length;k++){
            cal_item = tmp_calendarNodeDeleted[k];
            index = groupCalendar.calendarPersistElem.indexOfEventID(cal_item)//groupCalendar.indexOfPersistedItems.get(cal_item)           
            if(index!=-1)
                groupCalendar.calendarPersistElem.splice(index,1);
        }
        
    },
    persistItemsErrorResponse : function(err){
        console.log("error"+ workspaceMicroblogging.make_error_from_iq(err))
    },
    showPersistItems: function(){
        var html;
        
        for(var i=0;i<workspaceMicroblogging.PersistElem.length;i++){
            if(workspaceMicroblogging.PersistElem[i].service=="microblogging"){
                html= "<ul id='comments'><li><p class='info'>Added on "+workspaceMicroblogging.PersistElem[i].date+" from "+workspaceMicroblogging.PersistElem[i].from+" :</p>"+
                              "<div class='body'><p>"+workspaceMicroblogging.PersistElem[i].message+"</p>"+
                              "</div></li></ul>";

            }
            else{
                html= "<ul id='comments'><li><p class='info'>File Added on "+workspaceMicroblogging.PersistElem[i].date+" from "+workspaceMicroblogging.PersistElem[i].from+" :</p>"+
                              "<div class='body'><p>"+workspaceMicroblogging.PersistElem[i].info_name+"<input type='button' onclick=workspaceMicroblogging.downloadFile('"+workspaceMicroblogging.PersistElem[i].message+"','"+workspaceMicroblogging.PersistElem[i].info_name+"') value='download'/></p>"+
                              "</div></li></ul>";
            }
            $('#messagewindow').append(html);
        }
        
    }

}
//otan teleiewsei auth h sinartisih tha les telos initializing environment
var workspaceChat = {
    username:null,
    connection: null,
    on_roster: function(iq){
        //Ext.MessageBox.updateProgress(i, Math.round(100*i)+'% completed');

        getActivityTreeFromRunId(workspaceChat.username);        
        
        $(iq).find('item').each(function(){

            var jid = $(this).attr('jid');         
            var jid_id = workspaceChat.jid_to_id(jid);
            var name = $(this).attr('name');
            var group = $(this).find('group').eq(0).text();            
            organizeData.groupMembers.push(name);
            var chat_name="";
            
            if(organizeData.roleType=="staff")
                chat_name = name+"("+group+")  offline";
            else
                chat_name = name+"  offline"
            MyDesktop.AccordionWindow.list.push(
                {id:jid_id,
                 text:chat_name,
                 iconCls:'user',
                 leaf:true,
                 listeners:{
                     click: function(node,evt){
                        try{
                            chatWith(name,jid);
                            
                        }
                        catch(e){
                            alert(e);
                        }
                     }
                 }
             });
             
        });
        Ext.MessageBox.hide();
        workspaceChat.connection.addHandler(workspaceChat.on_presence, null, "presence");
        workspaceChat.connection.send($pres());
       
    },

    presence_value: function(elem){
        if(elem.hasClass('online'))
            return 2;
        else if(elem.hasClass('away'))
            return 1;
        return 0;
    },
    jid_to_id: function (jid) {
        return Strophe.getBareJidFromJid(jid)
        .replace("@", "-")
        .replace(".", "-");
    },
    
    on_presence: function(presence) {
        try{
    
    //var pres = workspaceChat.presence_value(presence.find('.roster'))
            var ptype = $(presence).attr('type');
            var from = workspaceChat.jid_to_id($(presence).attr('from'));
   console.log("kanw presence "+organizeData.user_fullName);
            var name = Strophe.getNodeFromJid($(presence).attr('from'));
        //console.log($(presence).html());
            if (ptype !== 'error') {

                var contactIndex = -1;
                Ext.each(MyDesktop.AccordionWindow.list, function(item,index){                    
                    if(item.id==from){
                        contactIndex = index;
                        return false;//stop iteration
                    }

                });
              
                if(contactIndex==-1)
                    return false;
                //if (ptype === 'unavailable') {
                    //MyDesktop.AccordionWindow.list[contactIndex].text = name+"  offline";
                    var groupName="";
                    if(organizeData.roleType=="staff"){
                        var first = MyDesktop.AccordionWindow.list[contactIndex].text.indexOf("(")
                        var second = MyDesktop.AccordionWindow.list[contactIndex].text.indexOf(")")
                        console.log(first+" mexri "+second)
                        groupName = MyDesktop.AccordionWindow.list[contactIndex].text.substr(first,(second-first)+1)
                       console.log(groupName)
                    }
                //} else {
                    var show = $(presence).find("show").text();
                    if (show === "" || show === "chat") {
                        MyDesktop.AccordionWindow.list[contactIndex].text = name+groupName+" online";
                    } else {
                        MyDesktop.AccordionWindow.list[contactIndex].text = name+"  away";
                    }
                  
                //}
               
                var tree = Ext.getCmp('im-tree');
                if(tree){
                    tree.body.mask('Loading', 'x-mask-loading');
                    tree.root.reload();
                    tree.root.collapse(true, false);
                    setTimeout(function(){ // mimic a server call
                    tree.body.unmask();
                    tree.root.expand(true, true);
                    }, 1000);
                }
            //var li = contact.parent();
            //li.remove();
            //Gab.insert_contact(li);
            }
        }
        catch(e){
            console.log("error on roster presence"+e);
        }
        return true;
    },
    on_message: function(message) {
        try{
            var jid = Strophe.getBareJidFromJid($(message).attr('from'));
            //var jid_id = Gab.jid_to_id(jid);
            var name = Strophe.getNodeFromJid(jid);
            createChatBox(name,jid);            
            $('#chatbox_'+name+' .chatboxtitle').text(name+' says...');
            $('#chatbox_'+name+' .chatboxhead').toggleClass('chatboxblink');
            var body = $(message).find("html > body");
            if (body.length === 0) {
                body = $(message).find("body");
                if (body.length > 0) {
                    body = body.text();
                } else {
                    body = null;
                }
            } else {
                body = body.contents();
                var span = $("<span></span>");
                body.each(function () {
                    if (document.importNode) {
                        $(document.importNode(this, true)).appendTo(span);
                    } else {
                        // IE workaround
                        span.append(this.xml);
                    }
                });
                body = span;
            }
            if (body) {
    // add the new message
                $("#chatbox_"+name+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+name+':&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'+body+'</span></div>');
            }
        }
        catch(e){
            console.log(e);
        }
        return true;

    },
    error_onroster: function(){
       Ext.MessageBox.hide();
       Ext.Msg.alert("initialising environment fault..Maybe errors arise");
    }
    
}
var activityTree = {
    runId:null,
    connection: null,
    service: null,
    node: null,
    username:null,
    deadlines: null,
    NS_DATA_FORMS: "jabber:x:data",
    NS_PUBSUB: "http://jabber.org/protocol/pubsub",
    NS_PUBSUB_OWNER: "http://jabber.org/protocol/pubsub#owner",
    NS_PUBSUB_ERRORS: "http://jabber.org/protocol/pubsub#errors",
    NS_PUBSUB_NODE_CONFIG: "http://jabber.org/protocol/pubsub#node_config",
    on_event: function (msg) {
        try{
//prepei na ginei apo olous tous rolous complete ena activity, estw kai ana den simeetexeis se auto wste telika na
//emfanistei to parakatw activity...
            if ($(msg).find('x').length > 0) {
                
                var service = $(msg).find('field[var="service"] value').text();
                var userJid = $(msg).find('field[var="userJid"] value').text();

                if(service=="getActivity"){
                    var activityId = $(msg).find('field[var="activityId"] value').text();
                    var fromUser = $(msg).find('field[var="fromUser"] value').text();
                    var message = $(msg).find('field[var="msg"] value').text();
                    var option = $(msg).find('field[var="option"] value').text();
                    $("#"+activityId+"-"+fromUser).css("background-image","url('images/tick.png')")
                    //$("#"+activityId+"-"+fromUser).css("background-image","url('images/cur_activityUser.png')")
                    if(Strophe.getBareJidFromJid(userJid)!=Strophe.getBareJidFromJid(activityTree.connection.jid)){
                        //var activityId = $(msg).find('field[var="activityId"] value').text();
                        //var fromUser = $(msg).find('field[var="fromUser"] value').text();
                        var activityTitle = $(msg).find('field[var="activityTitle"] value').text();
                        var rolePartId = $(msg).find('field[var="rolePartId"] value').text();
                        if(activityTree.runId!=null){
                            var text = "";
                            
                            if(option!="notGetActivity")
                                getActivityTree(activityTree.username,activityTree.runId);
                            
                            //if(moduleData.userActivities.containsKey(activityId)){
                                //checkCompleted(activityTree.username, activityTree.runId, activityId,fromUser,activityTitle)
                                
                            //}
                            //else{
                                
                                
                                text = message;
                                $().toastmessage('showToast', {
                                    text     : text,
                                    sticky   : true,
                                    position : 'top-right',
                                    type     : 'notice'
                                });
                            //}
                        }
                    }
                    /*var module = MyDesktop.getModule('umlEditor');
                    if(module!=null){
                        module.init();
                        module.createWindow();
                    }*/
                }
                else if(service=="notification"){
                    var notificationArray = StringtoXML($(msg).find('field[var="notificationsArray"] value').text());
                    var roles = notificationArray.getElementsByTagName("role");
                    
                    for(var i=0;i<roles.length;i++){                        
                        if(roles[i].textContent==organizeData.role_id){
                            text = "More information: ";
                            text+=roles[i].parentNode.parentNode.getElementsByTagName("subject")[0].textContent;
                            $().toastmessage('showToast', {
                                    text     : text,
                                    sticky   : true,
                                    position : 'top-right',
                                    type     : 'notice'
                                });
                        }
                    }
                }
                else if(service=="confirmFinishing"){                   
                    var toFinish_activityId = $(msg).find('field[var="activityId"] value').text();
                    if(Strophe.getBareJidFromJid(userJid)!=Strophe.getBareJidFromJid(activityTree.connection.jid)){
                        fromUser = $(msg).find('field[var="fromUser"] value').text();
                        activityTitle = $(msg).find('field[var="activityTitle"] value').text();
                        if(moduleData.userActivities!=null){
                            if(moduleData.userActivities.containsKey(toFinish_activityId)){

                            alert("contains key")
                            var answer = "";
                            if(confirm("user "+fromUser+" wants to complete "+activityTitle))
                                answer = "true"
                            else
                                answer = "false";
                            //alert("from "+Strophe.getBareJidFromJid(userJid)+" to "+)
                                    activityTree.publish_action({
                                        fromUser: activityTree.username,
                                        service:"answerOnFinishing",
                                        activityTitle:answer,
                                        rolePartId:userJid
                                    });
                            }
                        }
                    }
                }
                else if(service=="answerOnFinishing"){
                    
                    var initSender = $(msg).find('field[var="rolePartId"] value').text();                    
                    if(Strophe.getBareJidFromJid(initSender)==Strophe.getBareJidFromJid(activityTree.connection.jid)){
                        fromUser = $(msg).find('field[var="fromUser"] value').text();
                        answer = $(msg).find('field[var="activityTitle"] value').text();
                        /*if(answer=="true"){
                            completeActivity(organizeData.activityId,"learning-activity",activityTree.runId,activityTree.username);
                            completeActivityForEveryOne(organizeData.activityId,activityTree.runId,organizeData.rolePartId);
                        }
                        else if(answer=="false"){
                            if(confirm("user "+fromUser+" wants to complete "+activityTitle)){
                                completeActivity(organizeData.activityId,"learning-activity",activityTree.runId,activityTree.username);
                                completeActivityForEveryOne(organizeData.activityId,activityTree.runId,organizeData.rolePartId);
                            }
                        }*/
                      
                    }
                }
            }
        }
        catch(e){
            alert(e);
        }
        return true;
    },
    publish_action: function (action) {
        try{            
            activityTree.connection.sendIQ(
                $iq({to: activityTree.service, type: "set"})
                    .c('pubsub', {xmlns: activityTree.NS_PUBSUB})
                    .c('publish', {node: activityTree.node})
                    .c('item')
                    .c('x', {xmlns: activityTree.NS_DATA_FORMS,
                             type: "result"})
                    .c('field', {"var": "fromUser"})
                    .c('value').t(action.fromUser)
                    .up().up()                    
                    .c('field', {"var": "service"})
                    .c('value').t('' + action.service)
                    .up().up()
                    .c('field', {"var": "userJid"})
                    .c('value').t('' + activityTree.connection.jid)
                    .up().up()
                    .c('field', {"var": "activityId"})
                    .c('value').t('' + action.activityId)
                    .up().up()
                    .c('field', {"var": "activityTitle"})
                    .c('value').t('' + action.activityTitle)
                    .up().up()
                    .c('field', {"var": "rolePartId"})
                    .c('value').t('' + action.rolePartId)
                    .up().up()
                    .c('field', {"var": "msg"})
                    .c('value').t('' + action.msg)
                    .up().up()
                    .c('field', {"var": "option"})
                    .c('value').t('' + action.option)

                );
            }
            catch(e){
                alert(e);
            }
    },
    publish_notification: function (action) {
        try{
            activityTree.connection.sendIQ(
                $iq({to: activityTree.service, type: "set"})
                    .c('pubsub', {xmlns: activityTree.NS_PUBSUB})
                    .c('publish', {node: activityTree.node})
                    .c('item')
                    .c('x', {xmlns: activityTree.NS_DATA_FORMS,
                             type: "result"})                                       
                    .c('field', {"var": "service"})
                    .c('value').t('' + action.service)
                    .up().up()
                    .c('field', {"var": "notificationsArray"})
                    .c('value').t('' + action.notification)                                        
                );

        }
        catch(e){
            alert(e);
        }
    }
}
$(document).ready(function () {
    try{ 
              
        if(getUrlVars()["username"] != null){
            $('#login_dialog').css("display","none");
            
            $('#ODS_login_dialog').dialog({
                autoOpen: true,
                draggable: false,
                modal: true,
                title: 'Connect to workspace',
                buttons: {
                    "Accept": function () {
                        $("#browsingloader_ODS").show();
                        $(document).trigger('connect', {
                            jid: getUrlVars()["username"] +"@"+ $('#ODS_jid').attr('name'),
                            password: getUrlVars()["username"],
                            username: getUrlVars()["username"]
                        });                   
                        
                    }
                }
            });
        }
        else if(getUrlVars()["username"] == null){
            $('#ODS_login_dialog').css("display","none");
            $('#login_dialog').dialog({
                autoOpen: true,
                draggable: false,
                modal: true,
                title: 'Connect to workspace',
                buttons: {
                    "Connect": function () {
                        $("#browsingloader").css("display","block");
                        $(document).trigger('connect', {
                            jid: $('#jid').val() +"@"+ $('#jid').attr('name'),
                            password: $('#psw').val(),
                            username: $('#jid').val()
                        });                   
                        $('#psw').val('');
                        
                    }
                }
            });
        }
        $('#error_dialog').dialog({
            autoOpen: false,
            draggable: false,
            modal: true,
            title: 'Whoops!  Something Bad Happened!',
            buttons: {
                "Ok": function () {
                    $(this).dialog('close');
                    $('#login_dialog').dialog('open');
                }
            }
        });

       
        /*$('#counter').html($('#message').val().length + ' skata');
        $('#message').keyup(function(){alert('a');
                            // get new length of charactersalert('a')
            $('#counter').html($(this).val().length);
        });*/

    }
    catch(e){
        alert(e);
    }
});

var timeoutTimer = null;

$(document).bind('connect', function (ev,data) {
    try{
        var conn = new Strophe.Connection('http://147.27.41.121:7070/http-bind/');
        //timeoutTimer = setTimeout(function(){timeout()},7000);
        //conn.addTimedHandler(10,timeout(ev))
        conn.connect(data.jid, data.password, function (status) {
            
            if(status == Strophe.Status.AUTHFAIL){
                alert("The username or password you entered is incorrect");
                $("#browsingloader").hide();
                $("#browsingloader_ODS").hide();
                //clearTimeout(timeoutTimer);
            }
            else if (status == Strophe.Status.CONNECTED) {
                if(getUrlVars()["username"] == null){                                       
                    $("#selectLD_dialog").dialog("open");
                    $("#login_dialog").dialog("close");
                    $("#browsingloader_1").css("display","block");                    
                    getLDs(data);
                    
                } else{
                    getPubSubNode(data, "");
                }
                    
                username = data.username;
                //clearTimeout(timeoutTimer);
                workspaceChat.username = Strophe.getNodeFromJid(data.jid);                
                workspaceChat.connection = conn;
                //microblogging service
                workspaceMicroblogging.username = Strophe.getNodeFromJid(data.jid);
                workspaceMicroblogging.connection = conn;
                workspaceMicroblogging.service = "pubsub." + $('#jid').attr('name');
                workspaceMicroblogging.password = data.password;
                //activity services
                activityTree.username = workspaceMicroblogging.username;
                activityTree.connection = conn;
                activityTree.service = "pubsub." + $('#jid').attr('name');
                
                //calendar service
                groupCalendar.connection = conn;
                groupCalendar.service = "pubsub." + $('#jid').attr('name');

                //getPubSubNode(data, "");

                
                 
/*class diagram*/
            //$('#status').html('Connecting...');
                SketchCast.connection = conn;
               
                SketchCast.service = "pubsub." + $('#jid').attr('name');
    /*class diagram*/
                //$(document).trigger('connected',data);
            }
            else if (status === Strophe.Status.DISCONNECTED) {
               
                $(document).trigger('disconnected');
            }
        });
    }
    catch(e){
        console.log("error "+e);
        //alert(e)
    }
});

$(document).bind('connected', function (env,data) {
    
    console.log("connected!!!!");
    try{
        
        Ext.MessageBox.show({           
           msg: 'Initializing Run Time Environment',
           progressText: 'Initializing...',
           wait:true,
           width:300,
           waitConfig: {interval:100}
           
       });
        console.log("connected  ");
        moduleData.username = data.username;  
        
        moduleData.run_id = run_id;
        if(run_id=="")
           moduleData.run_id = gup('run_id'); 
       
        console.log(" moduleData.run_id  "+ moduleData.run_id+"username "+moduleData.username);
        var iq = $iq({type: 'get'}).c('query', {xmlns: 'jabber:iq:roster'}); //perneis to roster kai kaneis kai presence     
        workspaceChat.connection.sendIQ(iq, workspaceChat.on_roster,workspaceChat.error_onroster,150000);
        
        //chat messages handler
        workspaceChat.connection.addHandler(workspaceChat.on_message,null, "message", "chat");     
        //microblogging messages
        workspaceMicroblogging.connection.addHandler(
                workspaceMicroblogging.on_event,
                null, "message", null, null, workspaceMicroblogging.service);
       
        /*class diagram*/
        $('#status').html("Connected.");
  
        //sketchCast
        /*SketchCast.joined = false;
        SketchCast.participants = {};
    // send negative presence send we’re not a chat client
        SketchCast.connection.send($pres().c("priority").t("-1"));
    //

        //prosthetw handler gia ta minimata poy stelnontai se oti afora to uml diagram
        SketchCast.connection.addHandler(
            SketchCast.on_event,
            null, "message", null, null, SketchCast.service);
        //prosthetw handler gia to muliuser chat
        SketchCast.connection.addHandler(
            SketchCast.on_presence,
            null, "presence");
        SketchCast.connection.addHandler(SketchCast.on_public_message,
            null, "message", "groupchat");
*/
        

//calendar service
        groupCalendar.connection.addHandler(
                groupCalendar.on_event,
                null, "message", null, null, groupCalendar.service);
     
        //activityTree service
        activityTree.connection.addHandler(
                activityTree.on_event,
                null, "message", null, null, activityTree.service);
                
                 
       //load activities description
       //prepei na kserw ta atoma tis omadas toy prin fortwsw tin sinartisi...To xreiazetai i sinartisi
       //pou elenxei ta deadlines
        //getActivityTreeFromRunId(data.username);

        //get user data
        getUserInfo(data.username);
        //maybe check role....
        
        workspaceMicroblogging.discoverNodes('getSubid');

        /*class diagram*/
    }
    //}
    catch(e){
        console.log(e);
    }
});

$(document).bind('disconnected', function () {

    try{
        alert('disconnect');
    //unsuscribe from node
        workspaceMicroblogging.connection.sendIQ(
                $iq({to: workspaceMicroblogging.service,
                     type: "set"})
                    .c('pubsub', {xmlns: workspaceMicroblogging.NS_PUBSUB_OWNER})
                    .c('unsubscribe', {node: workspaceMicroblogging.node,
                                       jid: workspaceMicroblogging.connection.jid}));
    //delete node
        /*workspaceMicroblogging.connection.sendIQ(
               $iq({to: workspaceMicroblogging.service,
                   type: "set"})
                   .c('pubsub', {xmlns: workspaceMicroblogging.NS_PUBSUB_OWNER})
                   .c('delete', {node: workspaceMicroblogging.node}));
*/
        workspaceChat.connection.disconnect();
        workspaceChat.connection = null;
        workspaceMicroblogging.connection = null;
        activityTree.connection = null;
    }
    catch(e){
        alert(e)
    }
 });

//gia to multiuser chat
$(document).bind('room_joined', function () {
   SketchCast.joined = true;
   $("#chat").append("<div class='notice'>*** Room joined.</div>")
});
$(document).bind('user_joined', function (ev, nick) {
    SketchCast.add_message("<div class='notice'>*** " + nick +" joined.</div>");
});

/*function timeout(){
    try{
        alert(conn)
        conn.connection.disconnect();
    }
    catch(e){
        alert(e)
    }
  
}*/

Array.prototype.indexOfEventID = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from].cal_event_id === elt)
        return from;
    }
    return -1;
  };