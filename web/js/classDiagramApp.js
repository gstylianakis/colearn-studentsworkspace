/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/*window.onload = function(){
    try{
        Joint.paper("world", 640, 380);
    }
    catch(e){
        alert('eskase edw '+e);
    }
};*/

var uml = Joint.dia.uml;
var devs = Joint.dia.devs;
var all = [];
var allArrows = [];
var ID=0;
function getElementByID(id){
   
    for(var i=0;i<all.length;i++){
        if(all[i].properties._identity==id)
            return all[i];
    }
    return null;
}
function toggle(id){
    var fieldset = document.getElementById(id);
    var hidden = fieldset.className == "hidden";
    fieldset.className = (hidden) ? "" : "hidden";

    var i = fieldset.childNodes.length - 1;
    while (i-- > 2){
	if (fieldset.childNodes[i].nodeType !== 1){
	    continue;
	}
	fieldset.childNodes[i].style.display = (hidden) ? null : "none";
    }
}

function opt(module, id, value){
    var key = module + "-" + id;
    
    var inp = document.getElementById(key);
    if (value !== undefined){	// setter
	inp.value = value;
    }
    var valStr = inp.value;
    var valInt = parseInt(valStr);
    if (!isNaN(valInt)){
	return valInt;
    }
    return valStr;
}

function attrs(){
    var attrs = {};
    attrs.fill = opt("attrs", "fill");
    attrs.stroke = opt("attrs", "stroke");
    return attrs;
}
function createJoint(module,arrowName,id){
    if (arrowName == undefined){
	arrowName = "arrow";
    }

    var arrow = Joint.dia[module][arrowName];
    //arrow.
    var label = opt(module, "label");

    if (label){
	arrow.label = label;
    }
    arrow._identity = id;
    
    allArrows.push(Joint({x: 100, y: 100}, {x: 200, y: 200},arrow).registerForever(all));
    

}
function newJoint(module, arrowName){
    
    createJoint(module,arrowName,ID);
    SketchCast.publish_action({
                    type: "createNewArrow",
                    id: ID,
                    toX: 0,
                    toY: 0,
                    arrowName: arrowName
                });
        ID++;
   
   
}

function toggleGhosting(){
    var l = all.length;
    while (l--){all[l].toggleGhosting();}
}


/**
 * UML.
 */

function createUmlClass(identifier){
    try{
        console.log("identifier: "+identifier);
        var
            attributes = opt("uml", "attributes"),
            methods = opt("uml", "methods");
            attributes = attributes ? attributes.split(",") : undefined;

            methods = methods ? methods.split(",") : undefined;
            var properties = {
                attrs: attrs(),
                rect: {x: 100, y: 100, width: 125, height: 100},
                label: opt("uml", "label"),
                attributes: attributes,
                methods: methods,
                id: identifier
            };
       
            var classObject = uml.Class.create(properties);
            all.push(classObject);
    }
    catch(e){
        alert(e);
    }
}

function newUmlClass(){
    try{
     
        createUmlClass(ID);//an to exw auto tote kanei create kai meta afou einai subscribed ksana dimiourgei afou erxetai to event
        SketchCast.publish_action({
                    type: "createNewClass",
                    id: ID,
                    toX: 0,
                    toY: 0,
                    arrowName: ""
                });
        ID++;
    }
    catch(e){
        alert(e);
    }
}
function move_umlClass(action){
    try{
    //console.log("mpiws kanw move??")
        for(var i=0;i<all.length;i++){            
            if(all[i].properties._identity==action.id){
                all[i].translate(action.x, action.y)
                
            }
        }
    }
    catch(e){
        alert(e);
    }
}

function connect_arrow(action){
    try{
      console.log("eimai edw")
        for(var i=0;i<allArrows.length;i++){           
            if(allArrows[i]._identity == action.arrow_id){
                var obj = null;
                //console.log("to from_object einia "+action.from_objectId);
                //console.log("to to_object einia "+action.to_objectId);
                if(action.to_objectId!="null" && action.to_objectId){
                    
                     obj= getElementByID(action.to_objectId);
                }
                else if(action.from_objectId!="null" && action.from_objectId){
                    
                    obj= getElementByID(action.from_objectId);
                }
                if(obj!=null){                    
                    allArrows[i].drawConnection(obj,action.capType);

                }
            }
        }
    }
    catch(e){
        alert(e)
    }

}
var SketchCast = {
    // drawing state
    room:null,
    NS_MUC: "http://jabber.org/protocol/muc",
    joined:null,
    participants:null,

    old_pos: null,
    color: '000',
    line_width: 4,

    // xmpp state
    connection: null,
    service: null,
    node: null,
    username:null,
    NS_DATA_FORMS: "jabber:x:data",
    NS_PUBSUB: "http://jabber.org/protocol/pubsub",
    NS_PUBSUB_OWNER: "http://jabber.org/protocol/pubsub#owner",
    NS_PUBSUB_ERRORS: "http://jabber.org/protocol/pubsub#errors",
    NS_PUBSUB_NODE_CONFIG: "http://jabber.org/protocol/pubsub#node_config",

    // pubsub event handler
    on_event: function (msg) {
        
        if ($(msg).find('x').length > 0) {

            var service_type = $(msg).find('field[var="service"] value').text();
      
            if(service_type=="sketchcast"){
               //ignore services that are not sketchcast...
            
                var action_type = $(msg).find('field[var="type"] value').text();

                var class_id = $(msg).find('field[var="class_id"] value').text();

                var toX = $(msg).find('field[var="toX"] value').text();
                var toY = $(msg).find('field[var="toY"] value').text();
                var arrowName = $(msg).find('field[var="arrowName"] value').text();

                var arrow_id = $(msg).find('field[var="arrow_id"] value').text();
                var to_object = $(msg).find('field[var="to_objectId"] value').text();
                var from_object = $(msg).find('field[var="from_objectId"] value').text();

                var capType = $(msg).find('field[var="capType"] value').text();
                var publisher = $(msg).find("x").attr("from");
                /*var from_pos = $(msg).find('field[var="from_pos"] value').text()
                    .split(',');
                var to_pos = $(msg).find('field[var="to_pos"] value').text()
                    .split(',');

                var action = {
                    color: color,
                    line_width: line_width,
                    from: {x: parseFloat(from_pos[0]),
                           y: parseFloat(from_pos[1])},
                    to: {x: parseFloat(to_pos[0]),
                         y: parseFloat(to_pos[1])}
                };*/

                if(action_type=="createNewClass" && publisher!=SketchCast.connection.jid){
                    createUmlClass(class_id);
                }
                else if(action_type=="moveTo" && publisher!=SketchCast.connection.jid){
                     move_umlClass({id:class_id,x:toX,y:toY});
                }
                else if(action_type=="createNewArrow" && publisher!=SketchCast.connection.jid){
                    createJoint("uml",arrowName,class_id);
                }
                else if(action_type=="connectArrow" && publisher!=SketchCast.connection.jid){
                    connect_arrow({arrow_id:arrow_id,to_objectId:to_object,from_objectId:from_object,capType:capType});
                }
                //SketchCast.render_action(action);
            }
            else if(service_type=="completed"){
                var fromUser = workspaceChat.jid_to_id($(msg).find('field[var="fromUser"] value').text())
                $().toastmessage('showToast', {
                            text     : "user "+fromUser+" finished activity",
                            sticky   : true,
                            position : 'top-right',
                            type     : 'notice',
                            closeText: ''
                        });
                 var module = MyDesktop.getModule('wiki');
                 //alert(module)
                 if(module!=null){
                    module.init();
                    module.createWindow();
                 }
            }
        }
        else if ($(msg).find('delete[node="' + SketchCast.node + '"]')
                   .length > 0) {
            SketchCast.show_error("SketchCast ended by presenter.");
        }

        return true;
    },

    on_old_items: function (iq) {
        $(iq).find('item').each(function () {
            SketchCast.on_event(this);
        });
    },

    // subscription callbacks
    subscribed: function (iq) {
        $(document).trigger("reception_started");
    },

    subscribe_error: function (iq) {
        SketchCast.show_error("Subscription failed with " +
                              SketchCast.make_error_from_iq(iq));
    },

    // error handling helpers
    make_error_from_iq: function (iq) {
        var error = $(iq)
            .find('*[xmlns="' + Strophe.NS.STANZAS + '"]')
            .get(0).tagName;
        var pubsub_error = $(iq)
            .find('*[xmlns="' + SketchCast.NS_PUBSUB_ERRORS + '"]');
        if (pubsub_error.length > 0) {
            error = error + "/" + pubsub_error.get(0).tagName;
        }

        return error;
    },

    show_error: function (msg) {
        SketchCast.connection.disconnect();
        SketchCast.connection = null;
        SketchCast.service = null;
        SketchCast.node = null;

        $('#error_dialog p').text(msg);
        $('#error_dialog').dialog('open');
    },

    // node creation callbacks
    created: function (iq) {
        // find pubsub node
        var node = $(iq).find("create").attr('node');
        SketchCast.node = node;
        
        // configure the node
        var configiq = $iq({to: SketchCast.service,
                            type: "set"})
            .c('pubsub', {xmlns: SketchCast.NS_PUBSUB_OWNER})
            .c('configure', {node: node})
            .c('x', {xmlns: SketchCast.NS_DATA_FORMS,
                     type: "submit"})
            .c('field', {"var": "FORM_TYPE", type: "hidden"})
            .c('value').t(SketchCast.NS_PUBSUB_NODE_CONFIG)
            .up().up()
            .c('field', {"var": "pubsub#deliver_payloads"})
            .c('value').t("1")
            .up().up()
            .c('field', {"var": "pubsub#send_last_published_item"})
            .c('value').t("never")
            .up().up()
            .c('field', {"var": "pubsub#persist_items"})
            .c('value').t("true")
            .up().up()
            .c('field', {"var": "pubsub#max_items"})
            .c('value').t("20");

        SketchCast.connection.sendIQ(configiq,
                                     SketchCast.configured,
                                     SketchCast.configure_error);

    },

    create_error: function (iq) {
        console.log(SketchCast.make_error_from_iq(iq))
        SketchCast.show_error("SketchCast creation failed with " +
                              SketchCast.make_error_from_iq(iq));
    },

    configured: function (iq) {
        
        $(document).trigger("broadcast_started");
    },

    configure_error: function (iq) {
        SketchCast.show_error("SketchCast configuration failed with " +
                              SketchCast.make_error_from_iq(iq));
    },

    publish_action: function (action) {        
            try{                
            SketchCast.connection.sendIQ(
                $iq({to: SketchCast.service, type: "set"})
                    .c('pubsub', {xmlns: SketchCast.NS_PUBSUB})
                    .c('publish', {node: SketchCast.node})
                    .c('item')
                    .c('x', {xmlns: SketchCast.NS_DATA_FORMS,
                             type: "result",
                             from: SketchCast.connection.jid})
                    .c('field', {"var": "type"})
                    .c('value').t(action.type)
                    .up().up()
                    .c('field', {"var": "class_id"})
                    .c('value').t('' + action.id)
                    .up().up()
                    .c('field', {"var": "toX"})
                    .c('value').t('' + action.toX)
                    .up().up()
                    .c('field', {"var": "toY"})
                    .c('value').t('' + action.toY)
                    .up().up()
                    .c('field', {"var": "arrowName"})
                    .c('value').t('' + action.arrowName)
                    .up().up()
                    .c('field', {"var": "service"})
                    .c('value').t('' + 'sketchcast')
                );
            }
            catch(e){
                alert(e);
            }
                    

    },
    connectArrow: function (action) {
            try{
             SketchCast.connection.sendIQ(
                $iq({to: SketchCast.service, type: "set"})
                    .c('pubsub', {xmlns: SketchCast.NS_PUBSUB})
                    .c('publish', {node: SketchCast.node})
                    .c('item')
                    .c('x', {xmlns: SketchCast.NS_DATA_FORMS,
                             type: "result",
                             from: SketchCast.connection.jid})
                    .c('field', {"var": "type"})
                    .c('value').t(action.type)
                    .up().up()
                    .c('field', {"var": "arrow_id"})
                    .c('value').t('' +action.arrow_id)
                    .up().up()
                    .c('field', {"var": "to_objectId"})
                    .c('value').t('' + action.to_objectId)
                    .up().up()
                    .c('field', {"var": "from_objectId"})
                    .c('value').t('' + action.from_objectId)
                    .up().up()                    
                    .c('field', {"var": "capType"})
                    .c('value').t('' + action.capType)
                    .up().up()
                    .c('field', {"var": "service"})
                    .c('value').t('' + 'sketchcast')

                );
            }
            catch(e){
                alert(e);
            }


    },
    render_action: function (action) {
        // render the line segment
        var ctx = $('#sketch').get(0).getContext('2d');
        ctx.strokeStyle = '#' + action.color;
        ctx.lineWidth = action.line_width;
        ctx.beginPath();
        ctx.moveTo(action.from.x, action.from.y);
        ctx.lineTo(action.to.x, action.to.y);
        ctx.stroke();
        //console.log(moveTo)
    },

    disconnect: function () {
        $('#erase').click();
        SketchCast.connection.disconnect();
        SketchCast.connection = null;
        SketchCast.service = null;
        SketchCast.node = null;
        $('#login_dialog').dialog('open');
    },
    on_presence: function (presence) {
        
        var from = $(presence).attr('from');
        
        var room = Strophe.getBareJidFromJid(from);
       
         $(presence).children().each(function(){
                console.log($(this))
            })
      //console.log("kanw presence sto room "+SketchCast.room+" kai eimai "+room)
        // make sure this presence is for the right room
        if (room.toLowerCase() === SketchCast.room.toLowerCase()) {
       
            var nick = Strophe.getResourceFromJid(from);   
            if ($(presence).attr('type') === 'error' && !SketchCast.joined) {
    // error joining room; reset app
                SketchCast.connection.disconnect();
            }
            else if (!SketchCast.participants[nick] && $(presence).attr('type') !== 'unavailable') {
    // add to participant list

                SketchCast.participants[nick] = true;

                $('#participant-list').append('<li>' + nick + '</li>');
                if (SketchCast.joined) {
                    $(document).trigger('user_joined', nick);
                }
            }
            if ($(presence).attr('type') !== 'error' && !SketchCast.joined) {
    // check  to see if it’s our own presence
            var responseWho = "";
            $(presence).find("item").each(function(){
                responseWho = $(this).attr("jid");
                return false;
            })
                if ($(presence).find("item").length > 0)
                    if(responseWho==SketchCast.connection.jid){
    // check if server changed our nick
                    if ($(presence).find("status[code='210']").length > 0) {
                        SketchCast.nickname = Strophe.getResourceFromJid(from);
                    }
    // room join complete
                    $(document).trigger("room_joined");
               }
            }
        }
        return true;
    },
    on_public_message: function (message) {
        var from = $(message).attr('from');
        var room = Strophe.getBareJidFromJid(from);
        var nick = Strophe.getResourceFromJid(from);
     console.log("public message....from "+from+" to room "+room+" nickname "+nick)
// make sure message is from the right place
        if (room.toLowerCase() === SketchCast.room.toLowerCase()) {
// is message from a user or the room itself?
            var notice = !nick;
// messages from ourself will be styled differently
            var nick_class = "nick";
            if (nick === SketchCast.nickname) {
                nick_class += " self";
            }
        console.log("public message....")
            var body = $(message).children('body').text();
            var delayed = $(message).children("delay").length > 0 || $(message).children("x[xmlns='jabber:x:delay']").length > 0;
            
            if (!notice) {
                var delay_css = delayed? "delayed":"";
                SketchCast.add_message("<div class='message"+delay_css+"'>" +
                "&lt;<span class='" + nick_class + "'>" +
                nick + "</span>&gt; <span class='body'>" +
                body + "</span></div>");
            } else {
                SketchCast.add_message("<div class='notice'>*** " + body +"</div>");
            }
        }
        return true;
    },
    add_message: function (msg) {
// detect if we are scrolled all the way down
        
        var chat = $("#chat");

        var at_bottom = chat.scrollTop >= chat.scrollHeight - chat.clientHeight;
        $("#chat").append(msg);
// if we were at the bottom, keep us at the bottom
        if (at_bottom) {
            chat.scrollTop = chat.scrollHeight;
        }
    }
};

/*$(document).ready(function () {
    try{
    $('#login_dialog').dialog({
        autoOpen: true,
        draggable: false,
        modal: true,
        title: 'Connect to a SketchCast',
        buttons: {
            "Connect": function () {
                $(document).trigger('connect', {
                    jid: $('#jid').val(),
                    password: $('#password').val(),
                    service: $('#service').val(),
                    node: $('#node').val()
                });

                $('#password').val('');
                $(this).dialog('close');
            }
        }
    });

    $('#error_dialog').dialog({
        autoOpen: false,
        draggable: false,
        modal: true,
        title: 'Whoops!  Something Bad Happened!',
        buttons: {
            "Ok": function () {
                $(this).dialog('close');
                $('#login_dialog').dialog('open');
            }
        }
    });

    }
    catch(e){
        alert(e);
    }
});

$(document).bind('connect', function (ev, data) {
    $('#status').html('Connecting...');

    var conn = new Strophe.Connection(
        'http://localhost:7070/http-bind/');//'http://localhost:5280/http-bind');

    conn.connect(data.jid, data.password, function (status) {
        if (status === Strophe.Status.CONNECTED) {
            $(document).trigger('connected');
        } else if (status === Strophe.Status.DISCONNECTED) {
            $(document).trigger('disconnected');
        }
    });

    SketchCast.connection = conn;
    SketchCast.service = data.service;
    SketchCast.node = data.node;
});

$(document).bind('connected', function () {
    $('#status').html("Connected.");

    // send negative presence send we’re not a chat client
    SketchCast.connection.send($pres().c("priority").t("-1"));

    if (SketchCast.node.length > 0) {
        // a node was specified, so we attempt to subscribe to it

        // first, set up a callback for the events
        SketchCast.connection.addHandler(
            SketchCast.on_event,
            null, "message", null, null, SketchCast.service);

        // now subscribe
        var subiq = $iq({to: SketchCast.service,
                         type: "set"})
            .c('pubsub', {xmlns: SketchCast.NS_PUBSUB})
            .c('subscribe', {node: SketchCast.node,
                             jid: SketchCast.connection.jid});
        SketchCast.connection.sendIQ(subiq,
                                     SketchCast.subscribed,
                                     SketchCast.subscribe_error);
    } else {
        // a node was not specified, so we start a new sketchcast
        var createiq = $iq({to: SketchCast.service,
                            type: "set"})
            .c('pubsub', {xmlns: SketchCast.NS_PUBSUB})
            .c('create');
        SketchCast.connection.sendIQ(createiq,
                                     SketchCast.created,
                                     SketchCast.create_error);
    }
});*/

/*$(document).bind('broadcast_started', function () {
    $('#status').html('Broadcasting at service: <i>' +
                      SketchCast.service + '</i> node: <i>' +
                      SketchCast.node + "</i>");
    $('#disconnect').click(function () {
        $('.button').addClass('disabled');
        $('#sketch').addClass('disabled');
        $('#erase').attr('disabled', 'disabled');
        $('#disconnect').attr('disabled', 'disabled');

        SketchCast.connection.sendIQ(
            $iq({to: SketchCast.service,
                 type: "set"})
                .c('pubsub', {xmlns: SketchCast.NS_PUBSUB_OWNER})
                .c('delete', {node: SketchCast.node}));

        SketchCast.disconnect();
    });
});*/

$(document).bind('reception_started', function () {
    //$('#status').html('Receiving SketchCast.');

    /*
    $('#disconnect').click(function () {
        $('#disconnect').attr('disabled', 'disabled');
        SketchCast.connection.sendIQ(
            $iq({to: SketchCast.service,
                 type: "set"})
                .c('pubsub', {xmlns: SketchCast.NS_PUBSUB_OWNER})
                .c('unsubscribe', {node: SketchCast.node,
                                   jid: SketchCast.connection.jid}));

        SketchCast.disconnect();
    });*/

    // get missed events
    SketchCast.connection.sendIQ(
        $iq({to: SketchCast.service, type: "get"})
            .c('pubsub', {xmlns: SketchCast.NS_PUBSUB})
            .c('items', {node: SketchCast.node}),
        SketchCast.on_old_items);
});

function UMLcompleted(){
    try{
        
            SketchCast.connection.sendIQ(
                $iq({to: SketchCast.service, type: "set"})
                    .c('pubsub', {xmlns: SketchCast.NS_PUBSUB})
                    .c('publish', {node: SketchCast.node})
                    .c('item')
                    .c('x', {xmlns: SketchCast.NS_DATA_FORMS,
                             type: "result",
                             from: SketchCast.connection.jid})
                    .c('field', {"var": "fromUser"})
                    .c('value').t(SketchCast.connection.jid)
                    .up().up()
                    .c('field', {"var": "service"})
                    .c('value').t('' + 'completed')
                );
            }
            catch(e){
                alert(e);
            }
}