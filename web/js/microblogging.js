/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var sharingFile;
function publishStatus(msg){
    
     try{
        

     if(workspaceMicroblogging.node==null){
         alert("there is a problem with microblogging service..\nDisconnect and login again..")
     }


     workspaceMicroblogging.publish_action({
                    fromUser: workspaceMicroblogging.username,
                    message:msg,
                    service:"microblogging"
                   
                });
     }
     catch(e){
         console.log(e);
     }
}
function dragenter(e) {
        try{
            e.stopPropagation();
            e.preventDefault();
        }
        catch(e){
            alert("error while dragging file");
            console.log("error in dragenter function ");
        }
    }
    function dragover(e) {
        try{
            e.stopPropagation();
            e.preventDefault();
        }
        catch(e){
            alert("error while dragging file");
            console.log("error in dragover function ");
        }
    }
    function drop(e) {
        try{
            e.stopPropagation();
            e.preventDefault();

            var dt = e.dataTransfer;
            var files = dt.files;
            handleFiles(files);

        }
        catch(e){
            alert("error while dropping file");
            console.log("error in dropping function "+e);
        }
    }
    function handleFiles(files){
        try{
            var file;
            var list = document.createElement("ul");
            
            for (var i = 0; i < files.length; i++) {
                //file = files[i];
                var li = document.createElement("li");
                list.appendChild(li);
                file = document.createElement("div");
                //console.log(window.webkitURL)//einai ston mozilla window.URL.
                var ua = $.browser;
                if ( ua.mozilla)
                    file.textContent = window.URL.createObjectURL(files[i]);
                else
                    file.textContent = window.webkitURL.createObjectURL(files[i]);
                li.appendChild(file);
             console.log(files[i])
                //$("#uploadFile").appendChild(list);
                $("#dropBoxFileArea").val(files[i].name)
                $("#dropBoxFileArea").attr("title",files[i].name);
                sharingFile = files[i];
            }
        }
        catch(e){
            alert("error while handling files "+e)
            console.log("error while handling files "+e)
        }
    }
    function uploadFile(){
         try{
            var reader = new FileReader();
            var ctrl = $("#throbber");
            ctrl.css("display","block");
            console.log("paw gia upload ");
            reader.onload  = function(evt){
              
                workspaceMicroblogging.publish_action({
                    fromUser: workspaceMicroblogging.username,
                    message:evt.target.result,
                    info_name:sharingFile.name,
                    info_size:sharingFile.size,
                    service:"fileShareInBlogging"

                });
                //publishStatus(evt.target.result);
                ctrl.css("display","none");
                $("#dropBoxArea").val("");
            }
            //reader.readAsBinaryString(sharingFile);
            
            reader.readAsDataURL(sharingFile);

            /*var reader = new FileReader();
            var xhr = new XMLHttpRequest();
  
  reader.onload = function(evt) {
      console.log("to evt einia "+evt)
   xhr.open("GET", "http://localhost:8084/studentsWorkspace/fileSharing.jsp?username=a&password=123&fileData="+evt.target.result,true);
    xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');
  
  };
  reader.readAsBinaryString(sharingFile);
  xhr.send(null);
            reader.readAsBinaryString(sharingFile);*/
            

            
            }            
            

        catch(e){
            alert("error while uploading files "+e)
            console.log("error while uploading files "+e)
        }

    }
    function shareFile(){
        try{
            if($("#dropBoxFileArea").val()==""){
                alert("No file selected. Drop a file in the area")
                return;
            }
            uploadFile()
            //var iq = $iq({to: workspaceMicroblogging.service, type: "set", from:Strophe.getBareJidFromJid(workspaceMicroblogging.connection.jid)}).c('query', {xmlns: 'jabber:iq:oob'}).c('url').t($("#dropBoxFileArea").val()).up().c('desc').t("testing file sharing");
            //console.log("to einia "+iq);
            //workspaceMicroblogging.connection.sendIQ(iq, workspaceMicroblogging.sharing, workspaceMicroblogging.sharingError);
        }
        catch(e){
            console.log("error while sharing file "+e)
        }
    }

    function getPersistItems(subid){
        try{
            var iq = $iq({to: workspaceMicroblogging.service, type: "get",id:'getPersistItems',from:workspaceMicroblogging.connection.jid})
            .c('pubsub', {xmlns: workspaceMicroblogging.NS_PUBSUB})
            .c('items', {node:workspaceMicroblogging.node,max_items:500,subid:subid});
            
            workspaceMicroblogging.connection.sendIQ(iq,workspaceMicroblogging.persistItemsResponse,workspaceMicroblogging.persistItemsErrorResponse)

        }
        catch(e){
            console.log("error while getting items "+e)
        }
    }