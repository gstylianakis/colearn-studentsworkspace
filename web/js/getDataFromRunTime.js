/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    //147.27.41.106
    
    function getPubSubNode(data, option){       
        
        try{
            
            var xmlhttp = GetXmlHttpObject();

            //$("#browsingloader").show();
            
            if (xmlhttp == null)
            {
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
            
            var url="";
            if(option == ""){
                url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getPubSubNode.jsp?username="+data.username;
                if(getUrlVars()["run_id"] != null){
                    url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getPubSubNode.jsp?username="+data.username+"&run_id="+getUrlVars()["run_id"];
                }
            } else{
                
                url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getPubSubNode.jsp?username="+data.username+"&run_id="+option;
            }
            //url=url+"?q="+str;

            //dataFromRunTime.contentID = "StudentActivityTree";
            //loadingGifID = "#browsingloader";            
          
           xmlhttp.onreadystatechange = function(){
            try{
               console.log(xmlhttp.readyState+" status "+xmlhttp.status); 
                if (xmlhttp.readyState==4 && (xmlhttp.status == 200 || xmlhttp.status == 0) ){                    
              
                   workspaceMicroblogging.node = xmlhttp.responseText;
                   console.log("einai to node "+workspaceMicroblogging.node);
                       //workspaceMicroblogging.subscribe();
                   workspaceMicroblogging.discoverNodes('');
                   //auta ta 3 oxi edw....
                   groupCalendar.node = workspaceMicroblogging.node;
                   activityTree.node = workspaceMicroblogging.node;
                   SketchCast.node = workspaceMicroblogging.node;

                   //multiuser chat room
                   //exw dimiourgisei to service ston openfire,
                   //pigainontas create service sta group chat room
                   SketchCast.room = SketchCast.node+"@multiuserchatservice.petrolma-6fa201";
                   
                   run_id = option;

                   $(document).trigger('connected',data);    
                   
                   $('#browsingloader').hide();                   
                   $('#browsingloader_ODS').hide();
                   $('#login_dialog').dialog('close');
                   $('#ODS_login_dialog').dialog('close');
                   $('#selectLD_dialog').dialog('close');
                }

             
            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }
    /*function getSpecificPubSubNode(username,serviceId){
        try{
            var xmlhttp = GetXmlHttpObject();
            if (xmlhttp==null)
            {
                alert ("Your browser does not support Ajax HTTP");
                return;
            }

            var url="http://localhost:8084/studentsWorkspace/webServiceClient/getPubSubNodeForSpecificTeamService.jsp?username="+username+"&serviceId="+serviceId;
            //url=url+"?q="+str;

            //dataFromRunTime.contentID = "StudentActivityTree";
            //loadingGifID = "#browsingloader";

           xmlhttp.onreadystatechange = function(){
            try{
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    //alert("sketchcast "+xmlhttp.responseText);
                   SketchCast.node = xmlhttp.responseText;
                   
            }


            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }*/
    function getActivityTreeAndActivityDeadlines(username){
        try{
            var xmlhttp = GetXmlHttpObject();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
            var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/timerScheduler.jsp?username="+username+"&runId="+activityTree.runId;
            xmlhttp.onreadystatechange = function(){
            try{
                var date = "";
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                   activityTree.deadlines = eval('('+xmlhttp.responseText+')');
                   //kanw completed ta activities pou exei parelthei o xronos tous....
                   var learningActivityId = "";
                   
                   for (var i in  activityTree.deadlines){
                   learningActivityId =  i.substr(i.indexOf("la")+2);
                   learningActivityId = "la-" + learningActivityId;

                   
                   //elenxw an perase o xronos tou deadline wste an exei perasei otan imoun ektos
                   //sindesis, na kanw to activity completed..Meta ginetai completed kai den elenxw deuteri fora...
                   date = activityTree.deadlines[i].substr(0,activityTree.deadlines[i].indexOf(","));
             
                   if(new Date(date)<new Date()){
                        completeActivity(learningActivityId,"learning-activity",activityTree.runId,activityTree.username,"Totallycompleted");
                        completeActivityForEveryOne(learningActivityId,activityTree.runId,organizeData.rolePartId);

                    }
                   }
                   getActivityTree(username,activityTree.runId);
                   getActivityTreeUnPlug(username,activityTree.runId)


            }


            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }
    function getActivityTreeFromRunId(username){
        try{
            var xmlhttp = GetXmlHttpObject();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
           console.log("einai run_id "+run_id); 
            var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getRunId.jsp?username="+username+"&run_id="+run_id;
            xmlhttp.onreadystatechange = function(){
            try{                
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                       activityTree.runId = xmlhttp.responseText;
                       getActivityTreeAndActivityDeadlines(username);
                       getAllGlobalProperties(username, activityTree.runId);
                }
            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }
    function getActivityTreeUnPlug(username,runId){
        try{
            var xmlhttp = GetXmlHttpObject();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
           var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getVisibleActivityTree.jsp?username="+username+"&runId="+runId;
           xmlhttp.onreadystatechange = function(){
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){

                    if(xmlhttp.responseText=="null")
                       alert("error while requesting data about activities");
                   else{
                        var doc = StringtoXML(xmlhttp.responseText);
                        var userActivities = organizeData.getActivitiesForUser(doc.getElementsByTagName("learning-activity"));                        
                        moduleData.userActivities = userActivities;
                        
                   }
                }                
           }
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert("error while trying to load activities of user "+username+" "+e)
        }
    }
     function getActivityTree(username,runId){
        try{
       
            var xmlhttp = GetXmlHttpObject();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }

           var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getActivityTree.jsp?username="+username+"&runId="+runId;
           xmlhttp.onreadystatechange = function(){
            try{
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
          
                    if(xmlhttp.responseText=="null")
                       alert("error while requesting data about activities");
                   else{
                 
                       var doc = StringtoXML(xmlhttp.responseText);
                       var acts = doc.getElementsByTagName("act");
                       var tree = Ext.getCmp('phases-tree');
                       var activities = null;
                       var learningActivityId="",learningActivityIdTmp="", role_partId="",tooltip="activity is finished",activityTitle;
                       organizeData.role_id = doc.getElementsByTagName("learning-design")[0].getAttribute("role");
                       var completed = "false";  
                      //var treeNodeList = new Array();
                      
                       moduleData.MyDesktop_LDActs_list=[];
                       var activitiesDetails;
                       var actFeedback="";
                       var completionCondition = "",timer="";
                       for(var i=0;i<acts.length;i++){                           
                           
                           activitiesDetails = {"data":[]}
                           activities = acts[i].getElementsByTagName("learning-activity");
                           
                           for(var j=0;j<activities.length;j++){
                                timer="";
                                learningActivityId = activities[j].getAttribute("identifier");
                                if(activities[j].getAttribute("user-choice")=="true")
                                    completionCondition = "user-choice";
                                else if(activities[j].getAttribute("time-limit")=="true"){
                                    completionCondition = "time-limit";
                                    for (var k in  activityTree.deadlines){
                                        learningActivityIdTmp =  k.substr(k.indexOf("la")+2);
                                        learningActivityIdTmp = "la-" + learningActivityIdTmp;                                      
                                        if(learningActivityIdTmp==learningActivityId){
                                            timer = activityTree.deadlines[k]
                                        }
                                    }
                                }
                                role_partId = activities[j].parentNode.getAttribute("identifier");
                                completed = activities[j].getAttribute("completed");
                                activityTitle = activities[j].getElementsByTagName("title")[0].textContent;
                                
                                if(acts[i].getAttribute("completed")=="true"){
                                   organizeData.getPhaseFeedBack(acts[i].getAttribute('identifier'),runId,username)
                                }
                                if(completed=="false"){
                                    tooltip = "click to browse activity";
                                    $("#"+learningActivityId).css("color","#eeb420");
                              console.log(learningActivityId);      
                                }
                                
                                else{
                                    tooltip="activity is finished";
                               console.log("kai "+learningActivityId);     
                                    $("#"+learningActivityId).css("color","#678197");
                                }

                                activitiesDetails.data.push({
                                    id:learningActivityId,
                                    text: activityTitle,
                                    leaf: true,
                                    qtip:tooltip,
                                    data:{
                                        ldId:learningActivityId,
                                        rolePartId:role_partId,
                                        user_name:username,
                                        run_id:runId,
                                        completionCondition:completionCondition,
                                        time:timer
                                    }
                                    
                                  })

                           }
                          
                           moduleData.MyDesktop_LDActs_list.push({
                               id: acts[i].getAttribute('identifier'),
                               text:acts[i].getElementsByTagName('title')[0].textContent+actFeedback,
                               expanded:true,
                               children:activitiesDetails.data
                           })

                       }                       
                       if(tree){
                           
                          tree.setRootNode(new Ext.tree.AsyncTreeNode({
                                            text: "assigned activities",
                                            children:moduleData.MyDesktop_LDActs_list
                                        }));
                           
                           
                           tree.root.collapse(true, false);

                           tree.body.unmask();
                           tree.root.expand(true, true);
                      }

               }

            }


            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }
    function getLoContent(username,runId,loId,supportedFormat){
        try{
            var xmlhttp = GetXmlHttpObject();
            //$("#browsingloader").show();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
            var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getActivityContent.jsp?username="+username+"&ldType=learning-object&runId="+runId+"&ldId="+loId;

           xmlhttp.onreadystatechange = function(){
            try{
                var resource,contentTitle,content,resourceType,itemId;

                //                                                                                  
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    var response = (StringtoXML(xmlhttp.responseText));
                    var items = response.getElementsByTagName("item");
                    var tabPanel = Ext.getCmp("learning_objectsTabPanel");
                    for(var k=0;k<items.length;k++){
                        content = "";
                        resourceType="";
                        itemId = -1;
                        itemId = items[k].getAttribute("identifier");
                        resource = items[k].getAttribute('url');
                        if(items[k].childNodes[0]!=null)
                            contentTitle = items[k].childNodes[0].textContent;

                            if(contentTitle=="")
                                contentTitle = "resource";
                            
                            content = " <a class='media {width:1100, height:700}' src ='"+resource+"'>Title:"+contentTitle+"</a>"
                            resourceType = resource.substr(resource.lastIndexOf(".")+1)
                            if(!(resourceType in oc(organizeData.supportedFormat))){
                                var VIDEO_ID = getYouTubeVideoId(resource);
                                var src = resource;
                                if(VIDEO_ID!=-1)
                                    src = "http://www.youtube.com/embed/"+VIDEO_ID;
                                
                                content = " <iframe class=\"youtube-player\" allowfullscreen frameborder=\"0\" src ='"+src+"' width='100%' height='700'>"+
                                +"<p>Your browser does not support iframes.</p></iframe> "
                            }
                            //content = "<video width='400' height='300' durationHint='32.2' src="+resource+" ></video>";

                        tabPanel.add({
                                    id:itemId,                                    
                                    title: items[k].getElementsByTagName("title")[0].textContent,
                                    html:content                                    
                                    }
                                )
                        tabPanel.setActiveTab(k);
                        tabPanel.doLayout();
                        tabPanel.setActiveTab(0);
                        
                     }                                      
                   
                    Ext.getCmp("learningObjectsTab").doLayout();
                   Ext.getCmp("activityInformation-tabs").doLayout();
                   if($('.media')!=null){
                            $('.media').media();
                   }
                   
                   //console.log("to content einai "+$("#"+itemId).html()+" kai to prototype content "+content);
                }


            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }
    function getActivityContent(username,runId,ldId,ldTitle,completionCondition,time){
        try{
            
            var xmlhttp = GetXmlHttpObject();
            //$("#browsingloader").show();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
            var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getActivityContent.jsp?username="+username+"&ldType=learning-activity&runId="+runId+"&ldId="+ldId.ldId;

           xmlhttp.onreadystatechange = function(){
            try{

                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    var learningActivityElem = StringtoXML(xmlhttp.responseText);
                    organizeData.activityId = ldId.ldId;
                    organizeData.rolePartId = ldId.rolePartId;
                    organizeData.activityTitle = ldTitle;
                    //organizeData.showLearningActivity(learningActivityElem);                   
                    if(completionCondition=="user-choice")
                        $('#completionCondition').html("<p>Press the button to finish specific activity</p> <p><input type='button' style='width:55px' onclick='organizeData.publishFinishingActivity()' value='complete'/></p>");
                    else if(completionCondition=="time-limit")
                        $('#completionCondition').html("<p>This activity will be finished at "+time);

                    getEnvironmentTree(username,runId,ldId.ldId,learningActivityElem);
                   
                }


            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }
    
    function getEnvironmentTree(username,runId,environmentId,learningActivityElem){
        try{
            var xmlhttp = GetXmlHttpObject();
            //$("#browsingloader").show();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
            var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getEnvironmentTree.jsp?username="+username+"&runId="+runId+"&environmentId="+environmentId;

           xmlhttp.onreadystatechange = function(){
            try{

                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    
                    var EnvironmentTree = StringtoXML(xmlhttp.responseText);
                    organizeData.showLearningActivity(learningActivityElem,EnvironmentTree,runId);

                }


            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }
    function checkCompleted(username,runId,ldId,fromUser,activityTitle){
        try{
            var xmlhttp = GetXmlHttpObject();
            //$("#browsingloader").show();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
            //var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getActivityContent.jsp?username="+username+"&ldType=learning-activity&runId="+runId+"&ldId="+ldId;
           var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getActivityTree.jsp?username="+username+"&runId="+runId;

           xmlhttp.onreadystatechange = function(){
            try{
                var text="";
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    var learningActivityElem = StringtoXML(xmlhttp.responseText);
                    var activities = learningActivityElem.getElementsByTagName("learning-activity");
                    var activity=null;
                    for(var i=0;i<activities.length;i++){
                        if(activities[i].getAttribute("identifier")==ldId){
                            activity = activities[i];
                            break;
                        }
                    }
                    if(activity==null)
                        text = "user "+fromUser+" finished activity "+activityTitle+" and waits for you to complete it";

                    else{
                        if(activity.getAttribute("completed")=="true"){
                            text = "user "+fromUser+" finished activity "+activityTitle+" and you can continue to the next activity";
                            getActivityTree(username,runId);
                        }
                        else
                            text = "user "+fromUser+" finished activity "+activityTitle+" and waits for you to complete it";
                    }
                     
                    $().toastmessage('showToast', {
                                text     : text,
                                sticky   : true,
                                position : 'top-right',
                                type     : 'notice'

                            });
                }


            }catch(e){
                alert(e);
                return false;
            }
            
        };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }

    function checkOtherCompletes(runId,ldId,dataType){
        try{
            var xmlhttp = GetXmlHttpObject();
            //$("#browsingloader").show();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }            
           var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getLDIdStatus.jsp?runId="+runId+"&ldId="+ldId+"&dataType="+dataType;

           xmlhttp.onreadystatechange = function(){
            try{
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){                   
                    if(xmlhttp.responseText.indexOf("false")!=-1){
                        
                        $completionConfirm.dialog("open");
                    }
                    else{
                        
                        $().toastmessage('showToast', {
                                    text     : "Activity '"+organizeData.activityTitle+"'is finished",
                                    sticky   : true,
                                    position : 'top-right',
                                    type     : 'notice'

                        });
                        getActivityTree(activityTree.username,activityTree.runId);
                        activityTree.publish_action({
                            fromUser: activityTree.username,
                            service:"getActivity",
                            activityId: organizeData.activityId,
                            activityTitle:organizeData.activityTitle,
                            rolePartId: organizeData.rolePartId,
                            msg:"Activity '"+organizeData.activityTitle+"'is finished"
                       });
                       organizeData.getNotifications(organizeData.activityId,activityTree.username);
                    }
                }


            }catch(e){
                alert(e);
                return false;
            }

        };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }

    function checkTotalStatus(dataType,runId,ldId){
        try{
            var xmlhttp = GetXmlHttpObject();
            //$("#browsingloader").show();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
           var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getLDIdStatus.jsp?runId="+runId+"&ldId="+ldId+"&dataType="+dataType;

           xmlhttp.onreadystatechange = function(){
            try{

                if (xmlhttp.readyState==4 && xmlhttp.status == 200){                    
                    if(xmlhttp.responseText.indexOf("true")!=-1)                       
                        organizeData.getNotifications(organizeData.activityId,activityTree.username);
                }


            }catch(e){
                alert(e);
                return false;
            }

        };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }
    function completeActivityOfnoneParticipants(activityId,activityType,runId,username){
        try{
            var xmlhttp = GetXmlHttpObject();

            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
//alert(activityId+" "+username)
            var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/completeActivity.jsp?activityId="+activityId+"&activityType="+activityType+"&username="+username+"&runId="+runId;

            xmlhttp.onreadystatechange = function(){
            try{
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    //getActivityTree(activityTree.username,activityTree.runId);
                    //closeChildWindows();
                    //organizeData.getFeedBack(activityId,runId,username);

                }


            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
    }
   function completeActivity(activityId,activityType,runId,username,option){
    try{
            var xmlhttp = GetXmlHttpObject();

            //$("#browsingloader").show();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }

            var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/completeActivity.jsp?activityId="+activityId+"&activityType="+activityType+"&username="+username+"&runId="+runId;
            var tree = Ext.getCmp('phases-tree');
           xmlhttp.onreadystatechange = function(){
            try{
                if(option=="Totallycompleted"){
                    if(tree!=null)
                        tree.body.mask('Loading', 'x-mask-loading');
                }
                
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    if(option=="Totallycompleted")
                        getActivityTree(activityTree.username,activityTree.runId);
                    else
                        checkOtherCompletes(runId,activityId,"learning-activity")


                    //closeChildWindows();
                    organizeData.getFeedBack(activityId,runId,username);                    
                                    
                }


            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            alert(e);
        }
   }
   function getUserInfo(username){
       try{
           var xmlhttp = GetXmlHttpObject();
           if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
           }
           var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/userData.jsp?&username="+username;
           xmlhttp.onreadystatechange = function(){
            try{
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    var user_data = eval('(' + xmlhttp.responseText +')');
                    organizeData.user_email = user_data.email;
                    organizeData.user_fullName = user_data.fullName;
                    organizeData.roleType = user_data.roleType;
                    
                    
                }


            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);

       }
       catch(e){
           alert('error while getting user information')
       }
   }
   function getAllGlobalProperties(username, runId){
       /*for(var i in organizeData.groupMembers){
           MyDesktop.propertiesGridData.push([i])
       }*/
       try{
        
           var xmlhttp = GetXmlHttpObject();
           if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
           }
           var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getGlobalContent.jsp?username="+username+"&runId="+runId;
           xmlhttp.onreadystatechange = function(){
            try{
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    organizeData.adaptiveLearningProps = eval('(' + xmlhttp.responseText +')');
                     
                    for(var i=0; i<organizeData.adaptiveLearningProps.length;i++){
                        MyDesktop.propertiesGridData.push([organizeData.adaptiveLearningProps[i].propertyName,organizeData.adaptiveLearningProps[i].propertyValue]);
                        
                    }
                    //pare to get property gia tin timh
                    
                   //organizeData.adaptiveLearningProps[0].propertyName
                }


            }catch(e){
                alert(e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);

       }
       catch(e){
           alert('error while getting user information')
       }



   }

    function GetXmlHttpObject(){
        if (window.XMLHttpRequest)
        {
           return new XMLHttpRequest();
        }
        if (window.ActiveXObject)
        {
          return new ActiveXObject("Microsoft.XMLHTTP");
        }
        return null;
    }
