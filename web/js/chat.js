/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */




var chatboxFocus = new Array();
var chatBoxes = new Array();
function restructureChatBoxes() {
	try{
            var align = 0;
            for (var x=0;x<chatBoxes.length;x++) {

                    chatboxtitle = chatBoxes[x];
                    if ($("#chatbox_"+chatboxtitle).css('display') != 'none') {
                            if (align == 0) {
                                    $("#chatbox_"+chatboxtitle).css('right', '10px');
                            } else {
                                    width = (align)*(225+7)+10;
                                    $("#chatbox_"+chatboxtitle).css('right', width+'px');
                            }
                            align++; 
                    }
                    
            }
        }
        catch(e){
            console.log(e);
        }
}

function chatWith(chatuser,jid) {
	try{
            createChatBox(chatuser,jid);
            $("#chatbox_"+chatuser+" .chatboxtextarea").focus();
        }
        catch(e){
            console.log(e)
        }
}

function createChatBox(chatboxtitle,jid) {
	try{
            if ($("#chatbox_"+chatboxtitle).length > 0) {                
                    if ($("#chatbox_"+chatboxtitle).css('display') == 'none') {
                            $("#chatbox_"+chatboxtitle).css('display','block');
                            restructureChatBoxes();
                    }
                    
                    $("#chatbox_"+chatboxtitle+" .chatboxtextarea").focus();
                    return;
            }
            

            $(" <div />" ).attr("id","chatbox_"+chatboxtitle)
            .addClass("chatbox")
            .html('<div class="chatboxhead"><div class="chatboxtitle">'+chatboxtitle+'</div><div class="chatboxoptions"><a href="javascript:void(0)" onclick="javascript:toggleChatBoxGrowth(\''+chatboxtitle+'\')">-</a> <a href="javascript:void(0)" onclick="javascript:closeChatBox(\''+chatboxtitle+'\')">X</a></div><br clear="all"/></div><div class="chatboxcontent"></div><div class="chatboxinput"><textarea class="chatboxtextarea" onkeydown="javascript:return checkChatBoxInputKey(event,this,\''+chatboxtitle+'\',\''+jid+'\');"></textarea></div>')
            .appendTo($( "body" ));

            $("#chatbox_"+chatboxtitle).css('bottom', '0px');

            var chatBoxeslength = 0;

            for (var x=0; x<chatBoxes.length;x++) {
		if ($("#chatbox_"+chatBoxes[x]).css('display') != 'none') {
			chatBoxeslength++;
		}
            }

            if (chatBoxeslength == 0) {                
                    $("#chatbox_"+chatboxtitle).css('right', '10px');
            } else {
                    width = (chatBoxeslength)*(225+7)+10;
                    $("#chatbox_"+chatboxtitle).css('right', width+'px');
            }

            chatBoxes.push(chatboxtitle);

	
            //chatboxFocus[chatboxtitle] = false;

            $("#chatbox_"+chatboxtitle+" .chatboxtextarea").blur(function(){
                    //chatboxFocus[chatboxtitle] = false;
                    $("#chatbox_"+chatboxtitle+" .chatboxtextarea").removeClass('chatboxtextareaselected');
            }).focus(function(){
                //chatboxFocus[chatboxtitle] = true;
                $('#chatbox_'+chatboxtitle+' .chatboxtitle').text(chatboxtitle);
                $('#chatbox_'+chatboxtitle+' .chatboxhead').removeClass('chatboxblink');
		$("#chatbox_"+chatboxtitle+" .chatboxtextarea").addClass('chatboxtextareaselected');

            });            

            $("#chatbox_"+chatboxtitle).click(function() {
                    
                    if ($('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display') != 'none') {
                            $("#chatbox_"+chatboxtitle+" .chatboxtextarea").focus();
                    }
            });
            
            $("#chatbox_"+chatboxtitle).show();
        }
        catch(e){
            console.log(e);
        }
}

function closeChatBox(chatboxtitle) {
	$('#chatbox_'+chatboxtitle).css('display','none');
	restructureChatBoxes();	

}

function toggleChatBoxGrowth(chatboxtitle) {
        
	if ($('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display') == 'none') {		
		$('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','block');
		$('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','block');
		$("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop($("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
	} else {		
		$('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','none');
		$('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','none');
	}

}

function checkChatBoxInputKey(event,chatboxtextarea,chatboxtitle,jid) {
    try{
	if(event.keyCode == 13)  {
		$(chatboxtextarea).focus();
                $(chatboxtextarea).css('height','44px');
                event.preventDefault();
                var body = $(chatboxtextarea).val();
		body = body.replace(/^\s+|\s+$/g,"");
		workspaceChat.connection.send($msg({
                to: jid,
                "type": "chat"
                }).c('body').t(body));
                $("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom">'+workspaceChat.username+':&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'+body+'</span></div>');

                $(chatboxtextarea).val('');
	
		return false;
	}

	var adjustedHeight = chatboxtextarea.clientHeight;
	var maxHeight = 94;

	if (maxHeight > adjustedHeight) {
		adjustedHeight = Math.max(chatboxtextarea.scrollHeight, adjustedHeight);
		if (maxHeight)
			adjustedHeight = Math.min(maxHeight, adjustedHeight);
		if (adjustedHeight > chatboxtextarea.clientHeight)
			$(chatboxtextarea).css('height',adjustedHeight+8 +'px');
	} else {
		$(chatboxtextarea).css('overflow','auto');
	}
    }
    catch(e){
        console.log(e);
    }
}
