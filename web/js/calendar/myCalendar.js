/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var calendar;
function triggerCalendar(userName){

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
                var id = "";
                
                var calendar_element = Ext.get('calendar');
		calendar = $('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			selectable: true,
			selectHelper: true,
			select: function(start, end, allDay) {                           
				var title = prompt('Event Title:');
				//console.log(title+" "+start+"  "+end+" "+allDay)
                                if(title) {
                                        id = new Date().getTime();

					calendar.fullCalendar('renderEvent',
						{
                                                        id:id,
							title: title,
							start: start,
							end: end,
							allDay: allDay
						},
						true // make the event "stick"
					);
                                        try{
                                            if(groupCalendar.node==null){
                                                alert("there is a problem with microblogging service..\nDisconnect and login again..")
                                            }
                                            groupCalendar.publish_action({
                                                service:"calendar_newEvent",
                                                username:userName,
                                                title: title,
                                                start: start,
                                                end: end,
                                                allDay: allDay,
                                                event_id:id
                                                
                                            });
                                        }
                                        catch(e){
                                             alert(e);
                                        }
                                        
				}
				calendar.fullCalendar('unselect');

			},
			editable: true,
                       	
                        eventRightClick: function(calEvent, jsEvent, view) {                                                                                    
                            try{
                                id = calEvent.id;
                                //calendar_element.fireEvent('contextmenu',calendar_element)
                                
                                $(this).css('border-color', 'red');
                            }
                            catch(e){
                                console.log("error on right click "+e)
                            }
                            
                        }


		});
                
                calendar_element.Menu = new Ext.menu.Menu({                    

                    items: [{
                                text: 'delete',
                                handler:function(){                                   
                                   $('#calendar').fullCalendar('removeEvents',id);
                                   
                                   if(groupCalendar.node==null){
                                        alert("there is a problem with microblogging service..\nDisconnect and login again..")
                                   }                                   
                                   groupCalendar.publish_action({
                                        service:"calendar_deleteEvent",
                                        username:userName,
                                        event_id:id,
                                        title: "",
                                        start: "",
                                        end: "",
                                        allDay: ""

                                   });
                                },
                                scope: this
                            }]
                });
                calendar_element.on('contextmenu',function(e) {
                    try{
                        e.stopEvent();
                        calendar_element.Menu.showAt(e.getXY());
                    }
                    catch(e){
                        cosnole.log("error on function triggered on right click")
                    }
                }, this);
}
/*function getCalendarPersistItems(subid){
        try{
            var iq = $iq({to: groupCalendar.service, type: "get",id:'getPersistItems',from:groupCalendar.connection.jid})
            .c('pubsub', {xmlns: groupCalendar.NS_PUBSUB})
            .c('items', {node:groupCalendar.node,max_items:100,subid:subid});

            groupCalendar.connection.sendIQ(iq,groupCalendar.persistCalendarItemsResponse,groupCalendar.persistCalendarItemsErrorResponse)

        }
        catch(e){
            console.log("error while getting items "+e)
        }
    }*/