/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var xmlhttp;
var contentID;
var loadingGifID;
function validate(username,psw)
{

    xmlhttp=GetXmlHttpObject();
    $("#browsingloader").show();
    if (xmlhttp==null)
    {
        alert ("Your browser does not support Ajax HTTP");
        return;
    }

    var url="validateUser.jsp?username="+username+"&psw="+psw;
    //url=url+"?q="+str;

    contentID = "validationPanel";
    loadingGifID = "#browsingloader";
    xmlhttp.onreadystatechange=getOutput;
    xmlhttp.open("GET",url,true);
    xmlhttp.send(null);
}

function getOutput()
{
    if (xmlhttp.readyState==4){
        if(xmlhttp.responseText.indexOf("true")!=-1){
            
            $('#validationPanel').val("false");
            $(document).trigger('connect', {
                        jid: $('#jid').val() + $('#jid').attr('name'),
                        password: $('#psw').val()
                    });
            $('#jid').val('');
        }
        else
            Ext.Msg.alert("Wrong username or password");
        $('#psw').val('');

        
    }
}

function GetXmlHttpObject()
{
    if (window.XMLHttpRequest)
    {
       return new XMLHttpRequest();
    }
    if (window.ActiveXObject)
    {
      return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}