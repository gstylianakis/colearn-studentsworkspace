/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    
var organizeData = {
    activityId:-1,
    rolePartId:-1,
    activityTitle:"",
    user_email:"",
    user_fullName:"",
    role_id:"-1",
    roleType:"",
    groupMembers:[],
    adaptiveLearningProps:[],
    supportedFormat:['aif','aiff','aac','au','bmp','gsm','mov','mid','midi','mpg','mpeg','mp4','m4a','psd','qt','qtif','qif','qti','snd','tif','tiff','wav','3g2','3pg','flv', 'mp3', 'swf','asx', 'asf', 'avi', 'wma', 'wmv','ra', 'ram', 'rm', 'rpm', 'rv', 'smi', 'smil','xaml','html', 'pdf'],
    showLearningActivity: function(learningActivityElem,evironmentElem,runId){
        try{
            var user_id = workspaceMicroblogging.password;//uniqid();
            Ext.getCmp("activityInformation-tabs").body.unmask();
            Ext.getCmp("activityInformation-tabs").setActiveTab(0);
            Ext.getCmp("activityInformation-tabs").setActiveTab(1);//gia na energopoihsw ta tabs
            Ext.getCmp("activityInformation-tabs").setActiveTab(2);
            Ext.getCmp("activityInformation-tabs").setActiveTab(3);
            Ext.getCmp("activityInformation-tabs").setActiveTab(4);
            //Ext.getCmp("activityInformation-tabs").setActiveTab(5);
            Ext.getCmp("activityInformation-tabs").setActiveTab(0);

            if(Ext.getCmp('learning_objectsTabPanel')!=null)
                    Ext.getCmp('learning_objectsTabPanel').destroy();
                                    

                if(Ext.getCmp('servicesTabPanel')!=null)
                    Ext.getCmp('servicesTabPanel').destroy();


            var objectives = learningActivityElem.getElementsByTagName("learning-objectives")[0];
            var description = learningActivityElem.getElementsByTagName("activity-description")[0];
            var prerequisities = learningActivityElem.getElementsByTagName("prerequisites")[0];
            var environments = evironmentElem.getElementsByTagName("environment");
            var learning_objects = null;

            var title = "",launchUrl="",parameters="",services=null,jsonParam=null,monitorElem=null;
            var environment=null,tmp=null,serviceProp=null,serviceTabPanel=null,content="";
            var loObjectsTab = Ext.getCmp("learningObjectsTab");


            if(environments!=null){
                environment = Ext.getCmp("servicesTab");
                
                for(var i=0;i<environments.length;i++){                   
                    if(environments[i].getElementsByTagName("title")!=null)
                        title = environments[i].getElementsByTagName("title")[0];

                    services = environments[i].getElementsByTagName("service");

                    if(environments[i].getElementsByTagName("learning-object")!=null){
                        learning_objects = environments[i].getElementsByTagName("learning-object");
                        loObjectsTab.add(
                            new Ext.TabPanel({
                                id:'learning_objectsTabPanel',                                
                                height:780
                            }).show()
                        );
                        Ext.getCmp('activityInformation-tabs').doLayout();
                        Ext.getCmp('learning_objectsTabPanel').doLayout();
                        var lo_id="";
                        for(var j=0;j<learning_objects.length;j++){
                                lo_id = "";
                                lo_id = learning_objects[j].getAttribute("identifier");
                                if(lo_id!=""){
                                    getLoContent(activityTree.username,activityTree.runId,lo_id)
                                    //Ext.getCmp("activityInformation-tabs").doLayout();
                                   
                                    
                                }
                            
                           
                        }
                    }
                    if(title==null && services==null)
                        continue;


                   //environment = Ext.getCmp(title.textContent);
                   
            
                   /*if(environment==null)
                        Ext.getCmp("activityInformation-tabs").add({
                            id: title.textContent,
                            title: title.textContent,
                            html : "<p><img src='./images/warning.gif'/>If the page does not corresponds as it should <a href='#' id="+launchUrl+" onclick='loadWindow(this.id)'>open page in new window<img src='./images/page_icon.gif'/></a>"+
                                "<br/><b>If you are not logged in</b> the service refresh page <b>by clicking activity again</b> or <b>open page in new window</b></p><iframe id='iFrame-"+title.textContent+"' src ="+launchUrl+" width='100%' height='100%'>"+
                                +"<p>Your browser does not support iframes.</p></iframe> "
                        })*/
                    //else{
                        if(services!=null){
                            environment.add(
                                new Ext.TabPanel({
                                    id:'servicesTabPanel',
                                    activeTab: 0,
                                    height:700
                            }))
                            serviceTabPanel = Ext.getCmp("servicesTabPanel");
                            for(j=0;j<services.length;j++){
                                content="";
                                serviceProp = services[j];
                                console.log(serviceProp.t)
                                monitorElem = serviceProp.getElementsByTagName("monitor")[0];
                                parameters = monitorElem.getAttribute("parameters");
                                //jsonParam = eval('(' + parameters + ')');
                                
                                var run = runId.trim();

                                var olaunchUrl = "http://147.27.41.106:8084/studentsWorkspace/BasicLTI/lti_consumer.jsp?params="+parameters+"&user_id="+user_id+"&username="+organizeData.user_fullName+"&email="+organizeData.user_email+"&resourceId="+run;//endpoint="+jsonParam.launch_url+"&key="+jsonParam.ResourceKey+"&secret="+jsonParam.ResourceSecret+"&org_id="+jsonParam.ConsumerKey+"&org_desc="+jsonParam.ConsumerName+"&org_secret="+jsonParam.ConsumerSecret+"&resource_id="+organizeData.activityId+"&user_id="+user_id+"&username="+organizeData.user_fullName+"&email="+organizeData.user_email+"&context_id="+organizeData.activityId+"&context_title="+jsonParam.context_title+run+"&context_label="+jsonParam.context_label+"-"+run;
                                launchUrl = olaunchUrl.split(' ').join("%20");

                                content = "<p><img src='./images/warning.gif'/>If the page does not corresponds as it should <a href='#' id="+launchUrl+" onclick='loadWindow(this.id)'>open page in new window<img src='./images/page_icon.gif'/></a>"+
                                    "<br/><b>If you are not logged in</b> the service refresh page <b>by clicking activity again</b> or <b>open page in new window</b></p><iframe id='iFrame-"+title.textContent+"' src ="+launchUrl+" width='100%' height='700px'>"+
                                    +"<p>Your browser does not support iframes.</p></iframe> "

                                serviceTabPanel.add({
                                    id:serviceProp.getAttribute("identifier"),
                                    title: serviceProp.getElementsByTagName("title")[0].textContent,
                                    html:content
                                    }
                                )
                                serviceTabPanel.doLayout();
                                serviceTabPanel.setActiveTab(j);
                                


                            if(document.getElementById("iFrame-"+title.textContent)!=null)
                                document.getElementById("iFrame-"+title.textContent).src = launchUrl;
                            }


                        }

                    //}
                    
                }
                Ext.getCmp("activityInformation-tabs").doLayout()
                
            }
            
            var content="";
            var contentTitle = "";
            var resource = "";
            var resourceType = "";            
            $("#activityDescriptionTab").html("")
            $("#learningObjectivesTab").html("");            
            $("#prerequisitesTab").html("");
            $("#feedback").html("");

            if(description!==undefined){               
                for(i=0;i<description.childNodes.length;i++){
                    if(description.childNodes[i].childNodes[0]!=null)
                        contentTitle = description.childNodes[i].childNodes[0].textContent;
                    if(contentTitle=="")
                        contentTitle = "resource";
                    resource = description.childNodes[i].getAttribute('url');
                    content = "<a class='media {width:1100, height:710}' src ='"+resource+"'>Title:"+contentTitle+"</a>"
                    resourceType = resource.substr(resource.lastIndexOf(".")+1)

                    if(!(resourceType in oc(organizeData.supportedFormat))){
                        
                        /*content = " <iframe src ='"+description.childNodes[i].getAttribute('url')+"' width='100%' height='710'>"+
                            +"<p>Your browser does not support iframes.</p></iframe> "*/
                         var VIDEO_ID = getYouTubeVideoId(description.childNodes[i].getAttribute('url'));
                         var src = description.childNodes[i].getAttribute('url');
                         if(VIDEO_ID!=-1)
                             src = "http://www.youtube.com/embed/"+VIDEO_ID;
                         content = " <iframe class=\"youtube-player\" src ='"+src+"' width='100%' height='710' allowfullscreen>"+
                            +"<p>Your browser does not support iframes.</p></iframe> ";
                    }
                }
                $("#activityDescriptionTab").html(content);
                
            }
            if(objectives!==undefined){

                for(i=0;i<objectives.childNodes.length;i++){
                    if(objectives.childNodes[i].childNodes[0]!=null)
                        contentTitle = objectives.childNodes[i].childNodes[0].textContent;
                    if(contentTitle=="")
                        contentTitle = "resource";
                    resource = objectives.childNodes[i].getAttribute('url');
                    content = "<a class=\"media {width:1100, height:710}\" src ='"+resource+"'>Title:"+contentTitle+"</a>"
                    resourceType = resource.substr(resource.lastIndexOf(".")+1)

                    if(!(resourceType in oc(organizeData.supportedFormat))){
                        src = objectives.childNodes[i].getAttribute('url');
                        VIDEO_ID = getYouTubeVideoId(objectives.childNodes[i].getAttribute('url'));
                        if(VIDEO_ID!=-1)
                            src = "http://www.youtube.com/embed/"+VIDEO_ID;
                        content = " <iframe class=\"youtube-player\" src ='"+src+"' width='100%' height='710' allowfullscreen>"+
                            +"<p>Your browser does not support iframes.</p></iframe> ";
                        /*content = " <iframe src ='"+objectives.childNodes[i].getAttribute('url')+"' width='100%' height='710'>"+
                            +"<p>Your browser does not support iframes.</p></iframe> "*/
                    }
                }
                
                $("#learningObjectivesTab").html(content);
                
           }
            
            if(prerequisities!==undefined){               
                for(i=0;i<prerequisities.childNodes.length;i++){
                    if(prerequisities.childNodes[i].childNodes[0]!=null)
                        contentTitle = prerequisities.childNodes[i].childNodes[0].textContent;
                    if(contentTitle=="")
                        contentTitle = "resource";
                    resource = prerequisities.childNodes[i].getAttribute('url');
                    
                    content = " <a class='media {width:1100, height:710}' src ='"+resource+"'>Title:"+contentTitle+"</a>"
                    resourceType = resource.substr(resource.lastIndexOf(".")+1)
                    if(!(resourceType in oc(organizeData.supportedFormat))){
                        src = prerequisities.childNodes[i].getAttribute('url');                        
                        VIDEO_ID = getYouTubeVideoId(prerequisities.childNodes[i].getAttribute('url'));
                        if(VIDEO_ID != -1)
                            src = "http://www.youtube.com/embed/"+VIDEO_ID;
                        content = " <iframe class=\"youtube-player\" src ='"+src+"' width='100%' height='710' allowfullscreen>"+
                            +"<p>Your browser does not support iframes.</p></iframe> ";
                        /*content = " <iframe src ='"+prerequisities.childNodes[i].getAttribute('url')+"' width='100%' height='710'>"+
                            +"<p>Your browser does not support iframes.</p></iframe> "*/
                    }
                }
               $("#prerequisitesTab").html(content)

            }
            if($('.media')!=null){                
                $('.media').media();
            }
        }
        catch(e){
            alert("error while presenting learning activity "+e)
        }
    },
    getActivitiesForUser: function(activities){
        try{
            var activitiesOfUser = new Hashtable();
            var length = activities.length;
            var activity=null;
            for(var i=0;i<length;i++){
                activity = activities[i];//.getElementsByTagName("title")[0].textContent;
                activitiesOfUser.put(activities[i].getAttribute("identifier"),activity);
            }
            return activitiesOfUser;
        }
        catch(e){
            console.log(e)
        }
    },
    askOthersForFinishing: function(){
        $().toastmessage('showToast', {
                                text     : "Please wait.. Checking if other members of the group are participating..",
                                sticky   : true,
                                position : 'top-right',
                                type     : 'notice'

        });
        activityTree.publish_action({
                fromUser: activityTree.username,
                service:"confirmFinishing",
                activityId: organizeData.activityId,
                activityTitle:organizeData.activityTitle
            });
    },
    publishFinishingActivity: function(){
        try{
            
            if(activityTree.node==null){
                alert("there is a problem when trying to load activity tree..\nDisconnect and login again..")
            }
            if(checkCollaborativeActivity(organizeData.activityId)){
                $().toastmessage('showToast', {
                                    text     : "You finished activity '"+organizeData.activityTitle+"'.\nChecking if this activity has more participants..\nPlease wait..",
                                    sticky   : true,
                                    position : 'top-right',
                                    type     : 'notice'

                });
                completeActivity(organizeData.activityId,"learning-activity",activityTree.runId,activityTree.username,"notTotally");
                completeActivityForEveryOne(organizeData.activityId,activityTree.runId,organizeData.rolePartId);
                
                
            }
            else{
                $().toastmessage('showToast', {
                                    text     : "You finished activity "+organizeData.activityTitle,
                                    sticky   : true,
                                    position : 'top-right',
                                    type     : 'notice'

                                });

                completeActivity(organizeData.activityId,"learning-activity",activityTree.runId,activityTree.username,"Totallycompleted");
                completeActivityForEveryOne(organizeData.activityId,activityTree.runId,organizeData.rolePartId);

                checkTotalStatus("learning-activity",activityTree.runId, organizeData.activityId)
                //twra den to kanw publish to complete alla to kanw apo to engine, giati me to publish an den eisai sindemenos ekeini ti stigmh iparxei provlima
                activityTree.publish_action({
                    fromUser: activityTree.username,
                    service:"getActivity",
                    activityId: organizeData.activityId,
                    activityTitle:organizeData.activityTitle,
                    rolePartId: organizeData.rolePartId,
                    msg: "user "+activityTree.username+" finished activity "+organizeData.activityTitle
                });
            }
     }
     catch(e){
         alert(e);
     }
    },
    getFeedBack:function(activityId,runId,username){
       try{
            var xmlhttp = GetXmlHttpObject();
            //$("#browsingloader").show();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
            var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getActivityContent.jsp?username="+username+"&ldType=learning-activity&runId="+runId+"&ldId="+activityId;

           xmlhttp.onreadystatechange = function(){
            try{
                var contentTitle = "";
                var resource = "",content="",resourceType=""
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    var activity = StringtoXML(xmlhttp.responseText);
       
                    if(activity.getElementsByTagName("feedback-description")==null)
                        return;
                    var feedback = activity.getElementsByTagName("feedback-description")[0];
                    if(feedback==null)
                        return;
        
                        var item = feedback.childNodes[0];
                        if(item==null)
                            return;
        
                        if(item.childNodes[0]!=null)
                            contentTitle = item.childNodes[0].textContent;
                        if(contentTitle=="")
                            contentTitle = "resource";
                        resource = item.getAttribute('url');

                        content = " <a class='media {width:1100, height:710}' src ='"+resource+"'>"+contentTitle+"</a>"
                        resourceType = resource.substr(resource.lastIndexOf(".")+1)

                        if(!(resourceType in oc(organizeData.supportedFormat))){                            
                            var src = item.getAttribute('url');
                            VIDEO_ID = getYouTubeVideoId(item.getAttribute('url'));                            
                            if(VIDEO_ID != -1)
                                src = "http://www.youtube.com/embed/"+VIDEO_ID;
                            content = " <iframe class=\"youtube-player\" src ='"+src+"' width='100%' height='710' allowfullscreen>"+
                            +"<p>Your browser does not support iframes.</p></iframe>";
                        }                        
                        Ext.getCmp('accordionActivityPanel').getLayout().setActiveItem(1);
                        Ext.getCmp("completionTab").setActiveTab(1);
                        $("#feedback").html(content);
                        
                        if($('a.media')!=null)
                            $('a.media').media();
                                
                }


            }catch(e){
                console.log("error on getting feedback "+e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            console.log("error on presenting feedback "+e);
        }
    },
    getPhaseFeedBack:function(actId,runId,username){
       try{
            var xmlhttp = GetXmlHttpObject();
            //$("#browsingloader").show();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
            var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getActivityContent.jsp?username="+username+"&ldType=act&runId="+runId+"&ldId="+actId;

           xmlhttp.onreadystatechange = function(){
            try{
                var contentTitle = "";
                var resource = "",content="",resourceType=""
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    var act = StringtoXML(xmlhttp.responseText);
                    var title = act.getElementsByTagName("title")[0].textContent;
                    if(act.getElementsByTagName("feedback-description")==null)
                        return;
                    var feedback = act.getElementsByTagName("feedback-description")[0];
                    if(feedback==null)
                        return;

                        var item = feedback.childNodes[0];
                        if(item==null)
                            return;

                        if(item.childNodes[0]!=null)
                            contentTitle = item.childNodes[0].textContent;
                        if(contentTitle=="")
                            contentTitle = "resource";
                        resource = item.getAttribute('url');

                        content = " <a class='media {width:1100, height:710}' src ='"+resource+"'>"+contentTitle+"</a>"
                        resourceType = resource.substr(resource.lastIndexOf(".")+1)

                        if(!(resourceType in oc(organizeData.supportedFormat))){
                            var src = item.getAttribute('url');
                            VIDEO_ID = getYouTubeVideoId(item.getAttribute('url'));                            
                            if(VIDEO_ID != -1)
                                src = "http://www.youtube.com/embed/"+VIDEO_ID;
                            content = " <iframe class=\"youtube-player\" src ='"+src+"' width='100%' height='710' allowfullscreen>"+
                            +"<p>Your browser does not support iframes.</p></iframe>";
                            /*content = " <iframe src ='"+item.getAttribute('url')+"' width='100%' height='710'>"+
                            +"<p>Your browser does not support iframes.</p></iframe> "*/
                        }

                        $("#phase_feedback").html(content);
                        Ext.getCmp("completionTab").getItem(2).title=title+' feedback';
                        Ext.getCmp('accordionActivityPanel').getLayout().setActiveItem(1);
                        Ext.getCmp("completionTab").setActiveTab(2);
                        if($('a.media')!=null)
                            $('a.media').media();

                }


            }catch(e){
                alert("error on getting feedback "+e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        }
        catch(e){
            console.log("error on presenting feedback "+e);
        }
    },
    getNotifications: function(activityId,username){
        
            var xmlhttp = GetXmlHttpObject();
            //$("#browsingloader").show();
            if (xmlhttp==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }
            var url="http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getNotificationsOfActivityCompletion.jsp?activityId="+activityId+"&username="+username

            xmlhttp.onreadystatechange = function(){
            try{
                var all_conditions = null,conditions=null,condition=null,notifications=null,notification=null,notificationsElem=null;
                var if_statement=null,notificationObject=null,notifiactionRecipients="";
                var recipients=null;
                var whenCompleteNotification = "<notifications>";
                var notificationString = "";
                if (xmlhttp.readyState==4 && xmlhttp.status == 200){
                    all_conditions =  StringtoXML(xmlhttp.responseText);                    
                    conditions = all_conditions.getElementsByTagName("conditions");
                    for(var i=0;i<conditions.length;i++){
                        condition = conditions[i].getElementsByTagName("condition");                        

                        for(var j=0;j<condition.length;j++){
                            if(condition[j].getElementsByTagName("if")!=null){
                                if_statement = condition[j].getElementsByTagName("if")[0];
                                if(if_statement.getElementsByTagName("complete")[0]!=null){
                                    if(if_statement.getElementsByTagName("complete")[0].getAttribute("ref")==activityId){
                                        if(condition[j].getElementsByTagName("actions")[0]!=null){
                                            notifications = (condition[j].getElementsByTagName("actions")[0]).getElementsByTagName("notification")
                                            if(notifications!=null){                                                
                                                for(var k=0;k<notifications.length;k++){
                                                    notification = notifications[k];
                                                    notificationString = "<notification>";
                                                    notifiactionRecipients = "<receiverRoles>";
                                                    recipients = notification.getElementsByTagName("email-data");
                                                    for(var l=0;l<recipients.length;l++){
                                                        notifiactionRecipients +="<role>"+(recipients[l].getAttribute("role-ref"))+"</role>"
                                                    }

                                                    notifiactionRecipients += "</receiverRoles>";
                                                 
                                                    notificationString += notifiactionRecipients;
                                                    notificationString += "<subject>"+notification.getElementsByTagName("subject")[0].textContent+"</subject>";
                                                    notificationString += "</notification>";

                                                    whenCompleteNotification += notificationString;

                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }                                
                    }
                    whenCompleteNotification += "</notifications>"
                    
                    activityTree.publish_notification({
                        service:"notification",
                        notification: whenCompleteNotification
                    });
                }


            }catch(e){
                alert("error while requesting notifications "+e);
            }
           };
           xmlhttp.open("GET",url,true);
           xmlhttp.send(null);
        
    },
    updateCalendar:function(deadlines){
        try{
            var learningActivityId = "";
            var full_date="",day="",month="",year="",yearIndex="",dayIndex="",len = "";
            var time="",hour="",minutes="",date="",title="";
            var defineDate = new Date();
            for (var i in  deadlines){
                learningActivityId =  i.substr(i.indexOf("la")+2);
                learningActivityId = "la-" + learningActivityId;

                date = deadlines[i].substr(0,deadlines[i].indexOf(","));
                title = deadlines[i].substr(deadlines[i].indexOf(",")+1,deadlines[i].length);

                time = date.substr(date.indexOf(" "));
                full_date = date.substr(0,date.indexOf(" "));
                
                len = full_date.length;
                hour = time.substr(0,time.indexOf(":"));
                minutes = time.substr(time.indexOf(":")+1,time.len);

                year = full_date.substr(0,full_date.indexOf("-"));
                yearIndex = full_date.indexOf("-");
                dayIndex = full_date.indexOf("-",yearIndex+1);

                day = full_date.substr(dayIndex+1,len);
                month = full_date.substr(yearIndex+1,(dayIndex-yearIndex-1))

                defineDate.setDate(day);
                defineDate.setMonth(month-1); // January = 0
                defineDate.setFullYear(year);
                if((hour-12)>0)
                    hour = (hour-12);
                
                defineDate.setHours(hour, minutes,"00","00");
                
                $('#calendar').fullCalendar('renderEvent',{                                  
                                  title: "ending of activity "+title,
                                  start: defineDate,
                                  end: defineDate,
                                  allDay: false,
                                  editable:false
                                },
                                true // make the event "stick"

                );

            }
        }
        catch(e){
            alert(e)
            console.log(e);
        }
    }

}
function completeActivityForEveryOne(activityId,runId,rolePartId){
    try{
        
        
        for(var i=0;i<organizeData.groupMembers.length;i++){
            //if()
            if($("#"+activityId+"-"+organizeData.groupMembers[i]).attr('class')==null){                
                completeActivityOfnoneParticipants(activityId,"learning-activity",runId,organizeData.groupMembers[i]);
            }
            completeActivityOfnoneParticipants(rolePartId,"role-part",runId,organizeData.groupMembers[i]);
            
        }
    }
    catch(e){
        alert(e);
    }
}
function checkCollaborativeActivity(activityId){
    for(var i=0;i<organizeData.groupMembers.length;i++){
        
        if($("#"+activityId+"-"+organizeData.groupMembers[i]).attr('class')!=null)
            return true;
    }
    return false;
}
function loadWindow(launchUrl){
    try{
        //openNewWindow(launchUrl,'width=200,height=200,scrollbars=no,menubar=no,directories=no,location=no,resizable=yes,left=0,top=100')
        window.open(launchUrl, '', 'width=200,height=200,scrollbars=no,menubar=no,directories=no,location=no,resizable=yes,left=0,top=100')
    }
    catch(e){
        alert(e)
    }
}

function oc(a)
{
  var o = {};
  for(var i=0;i<a.length;i++)
  {
    o[a[i]]='';
  }
  return o;
}