/*!
 * Ext JS Library 3.2.1
 * Copyright(c) 2006-2010 Ext JS, Inc.
 * licensing@extjs.com
 * http://www.extjs.com/license
 */

// Sample desktop configuration

MyDesktop = new Ext.app.App({
	init :function(){
		Ext.QuickTips.init();
                //workspaceMicroblogging.discoverNodes('getSubid');//get persist items
	},

	getModules : function(){
		return [
			//new MyDesktop.GridWindow(),
                        //new MyDesktop.TabWindow(),
                        new MyDesktop.AccordionWindow(),
                        new MyDesktop.Calendar(),
                        //new MyDesktop.BogusModule(),
                        new MyDesktop.Twilike(),
                        new MyDesktop.GeneralInformationAboutCourse(),
                        new MyDesktop.propertiesTable()
                        //new MyDesktop.GeneralInformationAboutCourseForStaff(),
                        
		];
	},

    // config for the start menu
    getStartConfig : function(){
        return {
            title: '',
            iconCls: 'user',
            toolItems: [{
                text:'Settings',
                iconCls:'settings',
                scope:this
            },'-',{
                text:'Logout',
                iconCls:'logout',
                scope:this,
                listeners: {
                    click: function(node, event){                        
                        $(document).trigger("disconnected");
                        $('#login_dialog').dialog('open');
                    }
                }
            }]

        };
    }
});



/*
 * Example windows
 */
 Ext.grid.dummyData = [
    ['3m Co',71.72,0.02,0.03,'9/1 12:00am'],
    ['Alcoa Inc',29.01,0.42,1.47,'9/1 12:00am'],
    ['American Express Company',52.55,0.01,0.02,'9/1 12:00am'],
    ['American International Group, Inc.',64.13,0.31,0.49,'9/1 12:00am'],
    ['AT&T Inc.',31.61,-0.48,-1.54,'9/1 12:00am'],
    ['Caterpillar Inc.',67.27,0.92,1.39,'9/1 12:00am'],
    ['Citigroup, Inc.',49.37,0.02,0.04,'9/1 12:00am'],
    ['Exxon Mobil Corp',68.1,-0.43,-0.64,'9/1 12:00am'],
    ['General Electric Company',34.14,-0.08,-0.23,'9/1 12:00am'],
    ['General Motors Corporation',30.27,1.09,3.74,'9/1 12:00am'],
    ['Hewlett-Packard Co.',36.53,-0.03,-0.08,'9/1 12:00am'],
    ['Honeywell Intl Inc',38.77,0.05,0.13,'9/1 12:00am'],
    ['Intel Corporation',19.88,0.31,1.58,'9/1 12:00am'],
    ['Johnson & Johnson',64.72,0.06,0.09,'9/1 12:00am'],
    ['Merck & Co., Inc.',40.96,0.41,1.01,'9/1 12:00am'],
    ['Microsoft Corporation',25.84,0.14,0.54,'9/1 12:00am'],
    ['The Coca-Cola Company',45.07,0.26,0.58,'9/1 12:00am'],
    ['The Procter & Gamble Company',61.91,0.01,0.02,'9/1 12:00am'],
    ['Wal-Mart Stores, Inc.',45.45,0.73,1.63,'9/1 12:00am'],
    ['Walt Disney Company (The) (Holding Company)',29.89,0.24,0.81,'9/1 12:00am']
];
MyDesktop.propertiesTable = Ext.extend(Ext.app.Module, {
    id:'grid-win',
    init : function(){
        this.launcher = {
            text: 'properties window',
            iconCls:'icon-grid',
            handler : this.createWindow,
            scope: this
        }
        MyDesktop.propertiesGridData = [];
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('grid-win');

        if(!win){
            win = desktop.createWindow({
                id: 'grid-win',
                title:'Grid Window',
                width:740,
                height:480,
                iconCls: 'icon-grid',
                shim:false,
                animCollapse:false,
                constrainHeader:true,

                layout: 'fit',
                items:
                    new Ext.grid.GridPanel({
                        border:false,
                        ds: new Ext.data.Store({
                            reader: new Ext.data.ArrayReader({}, [
                               {name: 'user_name'}
                               //{name: 'price', type: 'float'},
                               
                            ]),
                            data: MyDesktop.propertiesGridData
                        }),
                        cm: new Ext.grid.ColumnModel([
                            new Ext.grid.RowNumberer(),
                            {header: "user name", width: 120, sortable: true, dataIndex: 'user_name'}
                            //{header: "Price", width: 70, sortable: true, renderer: Ext.util.Format.usMoney, dataIndex: 'price'},
                            //{header: "Change", width: 70, sortable: true, dataIndex: 'change'},
                            //{header: "% Change", width: 70, sortable: true, dataIndex: 'pctChange'}
                        ]),

                        viewConfig: {
                            forceFit:true
                        }
                    })
            });
        }
        win.show();
        
    }
});



MyDesktop.TabWindow = Ext.extend(Ext.app.Module, {
    id:'tab-win',
    init : function(){
        this.launcher = {
            text: 'Tab Window',
            iconCls:'tabs',
            handler : this.createWindow,
            scope: this
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('tab-win');
        if(!win){
            win = desktop.createWindow({
                id: 'tab-win',
                title:'Tab Window',
                width:740,
                height:480,
                iconCls: 'tabs',
                shim:false,
                animCollapse:false,
                border:false,
                constrainHeader:true,

                layout: 'fit',
                items:
                    new Ext.TabPanel({
                        activeTab:0,

                        items: [{
                            title: 'Tab Text 1',
                            header:false,
                            html : '<p>Something useful would be in here.</p>',
                            border:false
                        },{
                            title: 'Tab Text 2',
                            header:false,
                            html : '<p>Something useful would be in here.</p>',
                            border:false
                        },{
                            title: 'Tab Text 3',
                            header:false,
                            html : '<p>Something useful would be in here.</p>',
                            border:false
                        },{
                            title: 'Tab Text 4',
                            header:false,
                            html : '<p>Something useful would be in here.</p>',
                            border:false
                        }]
                    }


                )
            });
        }
        win.show();
    }
});


MyDesktop.AccordionWindow = Ext.extend(Ext.app.Module, {
    id:'acc-win',
    list: new Array(),
    init : function(){       
        this.launcher = {
            text: 'Group Users',
            iconCls:'accordion',
            handler : this.createWindow,
            scope: this

        }
        MyDesktop.AccordionWindow.list = []/*[{
                                    text:'Jack',
                                    iconCls:'user',
                                    leaf:true
                                },{
                                    text:'Brian',
                                    iconCls:'user',
                                    leaf:true,
                                    listeners: {
                                        click: function(node, event){
                                            alert(node);
                                        }
                                    }
                                }]*/
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('acc-win');
        if(!win){
            win = desktop.createWindow({
                id: 'acc-win',
                title: 'Group Members',
                width:250,
                height:400,
                iconCls: 'accordion',
                shim:false,
                animCollapse:false,
                constrainHeader:true,

                tbar:[{
                    tooltip:{title:'Rich Tooltips', text:'Let your users know what they can do!'},
                    iconCls:'connect'
                },'-',{
                    tooltip:'Add a new user',
                    iconCls:'user-add'
                },' ',{
                    tooltip:'Remove the selected user',
                    iconCls:'user-delete'
                }],

                layout:'accordion',
                border:false,
                layoutConfig: {
                    animate:false
                },

                items: [
                    new Ext.tree.TreePanel({
                        id:'im-tree',
                        title: 'Users of Group',
                        //loader: new Ext.tree.TreeLoader(),
                        rootVisible:false,
                        lines:false,
                        autoScroll:true,
                        tools:[{
                            id:'refresh',
                            on:{
                                click: function(){                                    
                                    var tree = Ext.getCmp('im-tree');
                                    tree.body.mask('Loading', 'x-mask-loading');
                                    tree.root.reload();
                                    tree.root.collapse(true, false);
                                    setTimeout(function(){ // mimic a server call
                                        tree.body.unmask();
                                        tree.root.expand(true, true);
                                    }, 1000);
                                }
                            }
                        }],
                        root: new Ext.tree.AsyncTreeNode({                            
                            children:[{
                                text:'Users',
                                expanded:true,                                
                                children:MyDesktop.AccordionWindow.list

                            }]                         
                        })
                    })/*,
                    {
                        id:'onlineUsers',
                        title: 'online users',
                        html : '<p>no users online yet</p>'
                      

                         }

                        
                    ,{
                        title: 'My Stuff',
                        html : '<p>Something useful would be in here.</p>'
                    }*/
                ]
            });
        }
        win.show();
    }
});

/*s('#acc-win div').on('click', function(node) {
node.eachChild(function() {
alert('a');
});
});*/
// for example purposes
var windowIndex = 0;

MyDesktop.BogusModule = Ext.extend(Ext.app.Module, {
    id:'umlEditor',
    init : function(){
        this.launcher = {
            text: 'Uml Class Diagram Editor',
            iconCls:'bogus',
            handler : this.createWindow,
            scope: this,
            windowId:'UmlClassEditor'
        }

    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('bogusUmlClassEditor');
        if(!win){
            win = desktop.createWindow({
                id: 'bogusUmlClassEditor',
                title:'Uml Class Diagram Editor',
                width:640,
                height:480,
                html : umlClassDiagramHtml,
                iconCls: 'bogus',
                shim:false,
                animCollapse:false,
                constrainHeader:true
            });
        }
        win.show();
        try{
            Joint.paper("world", 640, 380);
            //gia to multiuser chat
        //create room if not exist
        SketchCast.connection.send($iq({
            to:SketchCast.room,
            type:'set'
        }).c('query',{xmlns:SketchCast.NS_MUC+"#owner"}).c('x',{xmlns:'jabber:x:data', type:'submit'}))
            //send presence
        SketchCast.connection.send($pres({
            to:SketchCast.room+"/"+moduleData.username
        }).c('x',{xmlns:SketchCast.NS_MUC+"#participant"}))

        $('#input').keypress(function (ev) {           
            if (ev.which === 13) {
                ev.preventDefault();
                var body = $(this).val();
                
                SketchCast.connection.send($msg({
                    to: SketchCast.room,
                    type: "groupchat"}).c('body').t(body)
                );
                $(this).val("");
            }
        });
        }
        catch(e){
            console.log('error '+e);
        }
    }
});

MyDesktop.Calendar = Ext.extend(Ext.app.Module, {
    init : function(){
        this.launcher = {
            text: 'calendar',
            iconCls:'bogus',
            handler : this.createWindow,
            scope: this,
            windowId:'calendar_window'
        }
        
    },

    createWindow : function(src){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow("calendar_window");
        if(!win){
            win = desktop.createWindow({
                id: "calendar_window",
                title:src.text,
                width:640,
                height:480,
                layout : 'absolute',
                items:[
                    {html : "<div id='calendar'></div>"}
                ],

                iconCls: 'bogus',
                shim:false,
                animCollapse:false,
                constrainHeader:true
                

            });
        
            try{
               
                triggerCalendar(moduleData.username);                
                groupCalendar.showPersistItems();
                //enimerwnw to imerologio                
                organizeData.updateCalendar(activityTree.deadlines);
                
            }
            catch(e){
                alert("error while loading calendar "+e);
            }
            win.show();
        }
  
    }
});
MyDesktop.Wiki = Ext.extend(Ext.app.Module, {
    id:'wiki',
    init : function(){
        this.launcher = {
            text: 'wiki',
            iconCls:'bogus',
            handler : this.createWindow,
            scope: this            
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow("wiki_window");
        if(!win){
            win = desktop.createWindow({
                id: "wiki_window",
                title:'wiki',
                width:640,
                height:480,
                layout : 'absolute',
                items:[
                    {html : " <iframe src ='http://147.27.41.106:8084/jamwiki-1.0/' width='100%' height='480'>"+
                              +"<p>Your browser does not support iframes.</p></iframe> "}
                ],

                iconCls: 'bogus',
                shim:false,
                animCollapse:false,
                constrainHeader:true


            });
        }
        try{
            //triggerCalendar(moduleData.username);
        }
        catch(e){
            //alert("error while loading calendar "+e);
        }
        win.show();


    }
});
MyDesktop.Twilike = Ext.extend(Ext.app.Module, {
    id:'twilike',
    init : function(){
        try{
            this.launcher = {
                text: 'Twilike',
                iconCls:'tabs',
                handler : this.createWindow,
                scope: this

            }
            
        }
        catch(e){
            console.log(e);
        }
    },

    createWindow : function(){
        try{
            var desktop = this.app.getDesktop();
            var win = desktop.getWindow('twilike');
            if(!win){
                win = desktop.createWindow({
                    id: 'twilike',
                    title:'Twilike',
                    width:740,
                    height:480,                    
                    iconCls: 'tabs',
                    plain:true,
                    layout: 'border',
                    
                    items:[
                        {
                         title: 'what are you doing',
                         region: 'west',
                         defaultType: 'textfield',                         
                         collapsible: true,                                                 
                         margins: '0 0 0 5',                         
                         html:                                
                                "<div id='container'><div id='arrow'></div>"+
                                "<div id='twilike'>"+
                                "<fieldset><legend><span id='counter'></span> Characters</legend>"+
				"<textarea id='message' name='message'></textarea><br/>"+
				"<input name='btnSign' class='submit' type='submit' id='submit' value='Update' style='margin:0px 0px 10px 120px'/>"+
                                "<fieldset id='uploadFile'><legend style='padding:0px 0px 0px 182px'>Drag and drop file area</legend>"+
                                "<textarea style='height:20px' id='dropBoxFileArea' name='dropBoxArea'></textarea><br/>"+
                                "<div style='margin-left:120px;display:none' id='throbber'><img src='./images/thubber.gif'/></div><br/>"+
                                "<input name='btnSign2' class='submit' type='submit' id='submitFile' value='Share File' style='margin:0px 0px 10px 110px' onclick='shareFile()'/>"+
                                "</fieldset>"+
                                "</fieldset></div></div>"
                               
                            
                        },{
                        title: 'the latest updates',
                        collapsible: false,                       
                        region:'center',
                        margins: '0 5 0 0',
                        autoScroll :true,
                        html:
                            "<div id='messagewindow'></div>"
                       
                        
                        }]
                });
            }
            win.show();
            //workspaceMicroblogging.discoverNodes('getSubid');
            workspaceMicroblogging.showPersistItems();
            Ext.get('submit').on('click', function() {
                try{
                    publishStatus($("#message").val());
                }
                catch(e){
                    console.log(e);
                }
                
            });
            $("#counter").html($("#message").val().length + "");
            $("#message").keyup(function(){
                $("#counter").html($(this).val().length);
            });
            var dropBox;
            dropBox = document.getElementById("dropBoxFileArea");
            dropBox.addEventListener("dragenter", dragenter, false);
            dropBox.addEventListener("dragover", dragover, false);
            dropBox.addEventListener("drop", drop, false);

            
        }
        catch(e){
            console.log(e)
        }
    }
});
var moduleData = {
    username:"",
    userActivities:new Array(),
    MyDesktop_LDActs_list: new Array(),
    ActivityTree:""
}
MyDesktop.GeneralInformationAboutCourse = Ext.extend(Ext.app.Module, {
    id:"generalInfoWindow",
    init : function(){
        this.launcher = {
            text: 'Activities',
            iconCls:'bogus',
            handler : this.createWindow,
            scope: this,
            windowId:'ldPanel'
        }
        
    },    
    createWindow : function(src){
        var desktop = this.app.getDesktop();
        
        var urlDescriptionOfUol = 'http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getDescriptionForUol.jsp?username='+moduleData.username+"&run_id="+ moduleData.run_id;
        Ext.QuickTips.init();
        if(organizeData.roleType=="staff")
            urlDescriptionOfUol = 'http://147.27.41.106:8084/studentsWorkspace/webServiceClient/getStaffDescriptionForUol.jsp?username='+moduleData.username;
        var win = desktop.getWindow('learningDesignPanel');
        if(!win){
            
            win = desktop.createWindow({
                id: 'learningDesignPanel',
                title:'Learning Process',
                width:640,
                height:480,              
                plain:true,
                layout: 'border',
                items:[
                        new Ext.TabPanel({
                            activeTab: 0,
                            width: 280,
                            plain:true,
                            region: 'west',
                            collapsible: true,
                            defaults:{autoScroll: true},
                            margins:'0 0 0 0',
                            resizeTabs:true,
                            
                            items:[{                                    
                                    height:480,                                  
                                    title: 'General Information',
                                    //html:'lala'
                                    autoLoad:{
                                        url:urlDescriptionOfUol,
                                        scripts:true
                                    }
                                 },
                                    new Ext.tree.TreePanel({
                                        id:'phases-tree',
                                        title: 'Phases',                                        
                                        expanded:true,
                                        rootVisible:true,                                        
                                        root: new Ext.tree.AsyncTreeNode({                                            
                                            text: "assigned activities",
                                            children:moduleData.MyDesktop_LDActs_list                                            
                                            
                                        }),
                                        listeners:{
                                            click: function(node){                                                
                                            
                                                getActivityContent(node.attributes.data.user_name,node.attributes.data.run_id,node.attributes.data,node.text,node.attributes.data.completionCondition,node.attributes.data.time)
                                                Ext.getCmp("activityInformation-tabs").body.mask('Loading', 'x-mask-loading');                                                
                                             }
                                        }
                                         
                                    })
                                    
                                ]
                            })
                        
                      ,new Ext.Panel({
                        id:'accordionActivityPanel',
                        title: 'Activities (click on headers to collapse)',
                        region:'center',
                        layout:'accordion',
                        items:[
                            new Ext.Panel({
                                title:'<b>Activity Presentation Panel</b>',
                            items:[
                           new Ext.TabPanel({
                                
                                id: 'activityInformation-tabs',
                                activeTab: 0,
                                height:780,                                
                                items: [{
                                        title: 'Activity Description',
                                        id: 'activityDescriptionTab'
                                        },
                                        {
                                        title: 'Learning Objectives',
                                        id: 'learningObjectivesTab'
                                        
                                        },{
                                        title: 'Prerequisites',
                                        id: 'prerequisitesTab'
                                        },{
                                        title: 'learningObjects',
                                        id: 'learningObjectsTab'
                                        },{
                                        title: 'Services',
                                        id: 'servicesTab'
                                        }
                                    ]
                                     
                                    
                            })
                            ]}),
                            new Ext.Panel({
                                title: '<b>Complete Activity</b>',
                                region:'south',
                                items:[
                                    new Ext.TabPanel({
                                        collapsable:false,
                                        activeTab: 0,
                                        id:'completionTab',
                                        items:[
                                            {
                                                id:'completionCondition',
                                                title:'complete Activity'
                                                
                                            },
                                            {
                                                title: 'feedback',
                                                id: 'feedback'
                                            },
                                            {
                                                title: 'phase feedback',
                                                id: 'phase_feedback'
                                            }

                                        ]
                                    })
                                ]
                                
                                
                            })
                            
                        ]})
                  ]
            });
        }        
        win.show();
        
    }
});
var store = Ext.create('Ext.data.ArrayStore', {
        fields: [
           {name: 'user name'},


        ],
        data: propertyData
    });


var umlClassDiagramHtml =  "<div style='width:500px;float:left'><fieldset id='uml' class='form'>"+
                            "<legend onclick='toggle('uml')'>UML Class Diagram Editor</legend>"+//
                            "<ul style='float:left'>"+
                                "<li><button id=\"newUmlClass\" onclick=\"newUmlClass()\">UML class</button></li>"+
                                "<li><button class=\"newJoint\" onclick=\"newJoint('uml', 'aggregationArrow')\">aggregation arrow</button></li>"+
                                "<li><button class=\"newJoint\" onclick=\"newJoint('uml', 'dependencyArrow')\">dependency arrow</button></li>"+
                                "<li><button class=\"newJoint\" onclick=\"newJoint('uml', 'generalizationArrow')\">generalization arrow</button></li>"+
                                "<li><button class=\"newJoint\" onclick=\"newJoint('uml', 'arrow')\">arrow</button></li>"+
                                "<li><button class=\"newJoint\" onclick=\"UMLcompleted()\">Done</button></li>"+
                                "<li><button id=\"clearDiagram\" onclick=\"Joint.resetPaper()\">clear diagram</button></li>"+
                            "</ul>"+
                            "<ul style='float:left'>"+
                                "<li><label for=\"uml-label\">label: </label><br/><input id=\"uml-label\" value=\"\"/><button onclick=\"opt('uml', 'label', '')\">clear</button></li>"+
                                "<li><label for=\"uml-attributes\">attributes (split by ','): </label><br/><input id=\"uml-attributes\" class=\"long\" value=\"\"/><button onclick=\"opt('uml', 'attributes', '')\">clear</button></li>"+
                                "<li><label for=\"uml-methods\">methods (split by ','): </label><br/><input id=\"uml-methods\" class=\"long\" value=\"\"/><button onclick=\"opt('uml', 'methods', '')\">clear</button></li>"+
                            "</ul>"+

                        "</fieldset>"+
                        //"<fieldset id=\"general\" class=\"form\">"+
                          //"<legend onclick=\"toggle('general')\">General</legend>"+
                          //"<label for=\"attrs-fill\">attrs.fill: </label>"+
                          "<input type='hidden' id=\"attrs-fill\" type=\"text\" value=\"white\"/>"+
                          //"<label for=\"attrs-stroke\">attrs.stroke: </label>"+
                          "<input type='hidden' id=\"attrs-stroke\" type=\"text\" value=\"black\"/>"+
                          //"<button id=\"toggleGhosting\" onclick=\"toggleGhosting()\">toggleGhosting</button>"+

                        //"</fieldset>"+
                        "<div id=\"world\"></div></div>"+
                        "<div style='float:right;width:400px;'><div style='float:left'><div id='chat'></div><textarea id='input' rows='30' cols='30'></textarea></div><div id='participants'><ul id='participant-list'></ul></div></div>";


var CalendarHtml =  "<div id='calendar'></div>";
                        

// Array data for the grid


var un_id = 0;
    function uniqid(){
        var newDate = new Date;
        un_id++;
        return parseFloat(newDate.getTime())+un_id;
    }

    