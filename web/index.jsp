<%-- 
    Document   : index
    Created on : 7 Οκτ 2010, 5:32:28 μμ
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="configuration" scope="page" class="general.configurations"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>    
     <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>student workspace</title>
    <link rel='stylesheet' href='css/jquery-ui-1.7.2.custom.css'/>
    <link rel="stylesheet" type="text/css" href="css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="css/desktop.css" />
    <link rel="stylesheet" type="text/css" href="css/chat.css" />
    <link rel="stylesheet" type="text/css" href="css/twilike.css"/>    
    <link rel="stylesheet" type="text/css" href="css/fullcalendar.css"/>
    <link rel="stylesheet" type="text/css" href="css/dataTable.css"/>
    <link rel="stylesheet" type="text/css" href="jqueryTree/jquery.treeview.css"/>
    <link rel="stylesheet" type="text/css" href="css/multiuserChatRoom.css"/>
    <!--<link rel="stylesheet" type="text/css" href="css/app.css" />-->
    <!-- GC -->   
 	<!-- LIBS -->
        <script type="text/javascript" src="extjs/ext-base.js"></script>
 	<!-- ENDLIBS -->
       
<!--XMPP & Jquery-->
        <script type="text/javascript" src='jquery/jquery-1.3.2.min.js'></script>       
        <script type="text/javascript" src='jquery/jquery-ui-1.7.2.custom.min.js'></script>
        
        <script type="text/javascript" src="strophe/strophe.js"></script>
        <script type="text/javascript" src="strophe/flXHR.js"></script>
        <script type="text/javascript" src="strophe/strophe.flxhr.js"></script>
<!---->
 <!--jquery media plugin epireazei to calendar!!!!-->
 
        <script type="text/javascript" src='jquery/jqueryMetadataPlugin.js'></script>
        <script type="text/javascript" src='jquery/mediaApi.js'></script>
 <!--<script type="text/javascript" src="mediaPlugins/mwEmbed-player-static.js"></script>
 <link rel="stylesheet" type="text/css" href="mediaPlugins/mwEmbed-player-static.css"/>-->
        <script type="text/javascript" src="extjs/ext-all-debug.js"></script>
<!--chat boxes-->
        <script type="text/javascript" src="js/chat.js"></script>
        <!--calendar-->
        <script type="text/javascript" src="js/calendar/fullcalendar.js"></script>
        <script type="text/javascript" src="js/calendar/myCalendar.js"></script>
        <!--uploadfile-->
        <script type="text/javascript" src="js/uploadFile.js"></script>
        <!--paginate-->
        <script type="text/javascript" src="js/paginate.js"></script>
<script type="text/javascript">
    run_id = gup('run_id');

    function setPaging(){
        try{
            var res = document.getElementById("totalPages");
            var mpage = document.getElementById("pageNum");
            if(!res || res==null){
                return;
            }
            if(!mpage || res==null){
                mpage=1;
            }else{
                mpage=document.getElementById("pageNum");
            }
            var ctr=res.innerHTML;
            if(ctr>0)
                $("#resultsPaging,#resultsPagingFooter").paginate({
                    count   : ctr,
                    start : mpage.innerHTML,
                    display : 15,
                    border  : true,
                    border_color    : '#fff',
                    text_color  : '#fff',
                    background_color    : 'black',
                    border_hover_color  : '#ccc',
                    text_hover_color    : '#000',
                    background_hover_color  : '#fff',
                    images: false,
                    mouse: 'press',
                    onChange  : function(page){
                        //getData(page,document.getElementById("searchTerm").innerHTML);
                        //$('#dialog-form').scrollTop(0);
                    }
                });
        }catch(e){
            alert("error "+e)
        }
    }
  function getLDs(data){
  
      $.ajax({
        type: "POST",
        url: "/studentsWorkspace/webServiceClient/getListOfUoLForUser.jsp?username="+data.username,
                              // Send the login info to this page
                    statusCode: {
                        404: function() {
                            alert('page not found');
                        }
                    },
                    success: function(msg){
                       var result = eval('(' + msg + ')');                
                       $("#resultsNum").html("Designs you participate: "+result.count);
                       $("#LDResults").children('tbody:first').html(result.result);
                       $('#LDResults').paginateTable({rowsPerPage:8});                   
                       $("#LDResults td:nth-child(1)").click(function(){
                           console.log("clicked "+$(this).context.id +"data "+data);
                           //debugger;
                           getPubSubNode(data, $(this).context.id);

                       });
                       $("#pagerId").css("display","block");
                       $("#browsingloader_1").css("display","none");
                    }
                    
            });
  }
  $(document).ready(function() {              
    $("#accordion").accordion();
    $('#selectLD_dialog').dialog({
        autoOpen: false,
        draggable: true,
        modal: true,
        title: 'Select one of the scenarios you participate',
        
    });
  });

    var un_id = 0;
    function uniqid(){
          var newDate = new Date;
          un_id++;
          return parseFloat(newDate.getTime())+un_id;

      }
      function gup( name ){
            try{
                name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
                var regexS = "[\\?&]"+name+"=([^&#]*)";
                var regex = new RegExp( regexS );
                var results = regex.exec( window.location.href );
                if( results == null )
                    return -1;
                else
                    return results[1];
            }
            catch(e){
                throw "error while reading username "+e
            }
        }
//auto einai gia multiuser chat
        /*$().ready(function() {
            try{
                var childWindowHandles = new Array();
                function openNewWindow(url, params) {
                    childWindowHandles[childWindowHandles.length] = window.open(url, '', params);
                }
                function closeChildWindows() {
                    for (var loop=0; loop<childWindowHandles.length; loop++) if (!childWindowHandles[loop].closed) childWindowHandles[loop].close();
                }

                function inputArea_keypress(ev){
                    if (ev.which === 13) {
                        ev.preventDefault();
                        var body = $(this).val();

                        SketchCast.connection.send($msg({
                            to: SketchCast.room,
                            type: "groupchat"}).c('body').t(body)
                        );
                        $(this).val("");
                    }
                }
           }
           catch(e){
                console.log(e);
            }
        });*/
    </script>

<!--notifications-->
<script type="text/javascript" src="js/toastMessages/jquery.toastmessage.js"></script>
<link rel="stylesheet" type="text/css" href="js/toastMessages/css/jquery.toastmessage.css"/>
<!--end of notifications-->


<!--microblogging-->
    <script type="text/javascript" src="js/microblogging.js"></script>

<!--runt time data-->
    <script type="text/javascript" src="js/getDataFromRunTime.js"></script>
    <script type="text/javascript" src="js/runtimeResources.js"></script>
    <script type="text/javascript" src="js/hashtable.js"></script>

<!--show phases and activity structures-->
    <!--jquery tree-->
    <script type="text/javascript" src="jqueryTree/jquery.treeview.js"></script>
<!-- DESKTOP -->
    <script type="text/javascript" src="js/StartMenu.js"></script>
    <script type="text/javascript" src="js/TaskBar.js"></script>
    <script type="text/javascript" src="js/Desktop.js"></script>
    <script type="text/javascript" src="js/App.js"></script>
    <script type="text/javascript" src="js/Module.js"></script>
    <script type="text/javascript" src="modules.js"></script>

    <script type="text/javascript" src='js/workspaceCoreFunc.js'></script>
    <!--<script type="text/javascript" src="js/validateUser.js"></script>-->
    <!--class diagram-->
    <script src="joint/raphael-min.js" type="text/javascript"></script>
    <script src="joint/joint.js" type="text/javascript"></script>
    <script src="joint/joint_004.js" type="text/javascript"></script>
    <script src="joint/joint.dia-min.js" type="text/javascript"></script>
    <script src="joint/joint_003.js" type="text/javascript"></script>
    <script src="joint/joint.dia.uml-min.js" type="text/javascript"></script>
    <script src="joint/joint_002.js" type="text/javascript"></script>
    <script src="js/classDiagramApp.js" type="text/javascript"></script>
      
    <!---->
    <!--general-->
    <script type="text/javascript" src="js/stringManipulation.js"></script>
    
   
    <script type="text/javascript" src="js/organizeData.js"></script>

    <!----->
</head>
<body scroll="no" onunload="closeChildWindows();">
    <div id='ODS_login_dialog' class='hidden'>
        <p>Do you accept application to access your data?</p>
        <ul style="list-style:none;float: left">            
            <li><input style="display: none" type='text' id='ODS_jid' name='<%=configuration.getxmppServerURL()%>'/></li>                    
        </ul>
          <ul style="float: left">
              <li style="display: none" id="browsingloader_ODS"><img src="images/loadinfo.gif" /></li>
          </ul>
    </div> 
    <div id='selectLD_dialog' class='hidden'>
        <div>
            <ul style="list-style:none;float: left">
              <li>
                  <p id="resultsNum">Results:</p>
                  <table id="LDResults" style="background-color:#D8D8D8">
                    <tbody>
                    </tbody>
                  </table>
              </li>
            </ul>
        </div>
        <div style="height: 30px">
            <ul >
                <li style="display: none" id="browsingloader_1"><img src="images/loadinfo.gif" /></li>
            </ul>            
        </div>
        <div class='pager' id="pagerId" style="padding:10px 10px 0px 100px">
            <a href='#' alt='Previous' class='prevPage'>Prev</a>
            <span class='currentPage'></span> of <span class='totalPages'></span>
            <a href='#' alt='Next' class='nextPage'>Next</a>
        </div>
        
    </div>     
    <div id='login_dialog' class='hidden'>
        <ul style="list-style:none;float: left">
            <li><label>Username:</label></li>
            <li><input type='text' id='jid' name='<%=configuration.getxmppServerURL()%>'/></li>
          <li><label>Password:</label></li>
          <li><input type='password' id='psw'></li>          
        </ul>
        <ul style="float: left">
              <li style="display: none" id="browsingloader"><img src="images/loadinfo.gif" /></li>
        </ul>
    </div>          
    <div id="x-desktop">
        <dl id="x-shortcuts">
            <dt id="generalInfoWindow-shortcut">
                <a href="#"><img src="images/s.gif" />
                <div>Activities</div></a>
            </dt>
            <dt id="acc-win-shortcut">
                <a href="#"><img src="images/s.gif" />
                <div>Group Users</div></a>
            </dt>
        </dl>
        <input type="hidden" id="validationPanel" value="false" />
        
    </div>
    <div id="ux-taskbar">
        <div id="ux-taskbar-start"></div>
        <div id="ux-taskbuttons-panel"></div>
	<div class="x-clear"></div>        
    </div>
   
     <div id='error_dialog' class='hidden'>
      <p></p>
    </div>
    <div>
            <form style="width:190px;float:left;margin-top:6px;height:50px" target="upload_target" method="post" name="uploadFile" id="uploadFile" action="downloadFileFromServer.jsp" >
                <input id ="tmp_filename" name="tmp_filename" type="hidden" value=""/>
                <input id ="filename" name="filename" type="hidden" value=""/>
                <input id="content" name="content" value=""/>
                <input id="submitSharingFile" type="submit"/>
            </form>
            <iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:0px solid #fff;display: none"></iframe>

    </div>
          
</body>
</html>