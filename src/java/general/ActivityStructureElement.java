/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package general;

import java.util.ArrayList;
import org.w3c.dom.Node;

/**
 *
 * @author Administrator
 */
public class ActivityStructureElement extends ActivityElement {
    
    //private Node activity_structureNode;
    //private String activity_strucutureId;
    private ArrayList<ActivityElement> activities;
    //private ArrayList<String> roles;

    public ActivityStructureElement(String role_id,Node activity_structureNode, String activity_strucutureId) {
        super(role_id, activity_structureNode,activity_strucutureId);
        this.activities = new ArrayList<ActivityElement>();
        
    }
    public ActivityStructureElement(Node activity_structureNode,String activityStructureId){
        super(activity_structureNode,activityStructureId);
        this.activities = new ArrayList<ActivityElement>();
    }
    @Override
    public String getType(){
        return "Structure";
    }
    public ArrayList getActivities() {
        return this.activities;
    }

    public void addActivities(ActivityElement activity) {
        this.activities.add(activity);
    }

    public String getinnerActivities(Node activity_structureNode,ActivityStructureElement activity_structure,String structureTreeString){
        XMLParser parser = new XMLParser();
        ActivityStructureElement innerActivityStructure = null;
        ActivityElement innerActivity = null;
        ArrayList activitiesInstructure = parser.getChildsElementNodeByName(activity_structureNode,"learning-activity");
        ArrayList structuresInstructure = parser.getChildsElementNodeByName(activity_structureNode,"activity-structure");

        for(int i=0;i<activitiesInstructure.size();i++){
            innerActivity = new ActivityElement((Node)activitiesInstructure.get(i),((Node)activitiesInstructure.get(i)).getAttributes().getNamedItem("identifier").getNodeName());
            activity_structure.addActivities(innerActivity);
            structureTreeString += "<li><span class=''>"+innerActivity.getActivityName()+"</span></li>";
        }
        
        for(int i=0;i<structuresInstructure.size();i++){
            innerActivityStructure = new ActivityStructureElement((Node)structuresInstructure.get(i),((Node)structuresInstructure.get(i)).getAttributes().getNamedItem("identifier").getNodeName());
            activity_structure.addActivities(innerActivityStructure);
            structureTreeString += "<li><span class='folder'>"+innerActivityStructure.getActivityName()+"</span><ul>";
            return getinnerActivities((Node)structuresInstructure.get(i),innerActivityStructure,structureTreeString);
            
        }
        return structureTreeString + "</li></ul>";
        
       
    }
    /*public void addRoles(String role) {
        this.roles.add(role);
    }

    public ArrayList getRoles() {
        return this.roles;
    }
    public Node getActivity_structureNode() {
        return activity_structureNode;
    }

    public void setActivity_structureNode(Node activity_structureNode) {
        this.activity_structureNode = activity_structureNode;
    }

    public String getActivity_strucutureId() {
        return activity_strucutureId;
    }

    public void setActivity_strucutureId(String activity_strucutureId) {
        this.activity_strucutureId = activity_strucutureId;
    }*/

}
