/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package general;

/**
 *
 * @author Administrator
 */
public class ActivityResources {
    private String title;
    private LDItem learning_objectives;
    private LDItem prerequisities;
    //private String environment_ref;
    private LDItem activityDescription;

    public ActivityResources(String title){
        this.title = title;
    }

    public LDItem getActivityDescription() {
        return this.activityDescription;
    }

    public void setActivityDescription(String title,boolean visibility,String contentURL,String type) {
        this.activityDescription = new LDItem(title, visibility, contentURL, type);
    }

    public LDItem getLearning_objectives() {
        return this.learning_objectives;
    }

    public void setLearning_objectives(String title,boolean visibility,String contentURL,String type){
        this.learning_objectives = new LDItem(title, visibility, contentURL, type);
    }

    public LDItem getPrerequisities() {
        return this.prerequisities;
    }

    public void setPrerequisities(String title,boolean visibility,String contentURL,String type) {
        this.prerequisities = new LDItem(title, visibility, contentURL, type);
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
