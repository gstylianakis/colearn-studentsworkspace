/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package general;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

/**
 *
 * @author Administrator
 */
public class configurations {
    private Properties configFile;
    private String xmppServerURL="@petrolma-6fa201";

    public configurations()throws Exception{
        try{
           
            configFile  = new Properties();
            try{
                configFile.load( new FileInputStream("config.properties"));
                xmppServerURL = configFile.getProperty("XMPPServer");
            }catch(FileNotFoundException ex){
                xmppServerURL = "@petrolma-6fa201";
            }
        }
        catch(Exception e){
            throw e;
        }
    }

    public String getxmppServerURL(){
        return xmppServerURL;
    }
}
