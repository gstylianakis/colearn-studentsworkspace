/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package general;

/**
 *
 * @author Administrator
 */
import java.io.*;
import java.security.Principal;
import java.util.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.*;

public class ConsumerHttpServletRequest implements HttpServletRequest{
    public Map<String, String[]> parameters = new LinkedHashMap<String, String[]>();
  public String method = "POST";
  public String url;

  public void setParameter( String param, String value )
  {
    parameters.put( param, new String[] { value } );
  }

  @Override
  public Object getAttribute( String arg0 )
  {
    return null;
  }

  @Override
  public Enumeration<String> getAttributeNames()
  {
    return new Vector<String>().elements();
  }

  @Override
  public String getCharacterEncoding()
  {
    return "UTF-8";
  }

  @Override
  public int getContentLength()
  {
    return 0;
  }

  @Override
  public String getContentType()
  {
    return "";
  }

  @Override
  public ServletInputStream getInputStream() throws IOException
  {
    return new ServletInputStream()
      {
        @Override
        public int read() throws IOException
        {
          return 0;
        }
      };
  }

  @Override
  public String getLocalAddr()
  {
    return "127.0.0.1";
  }

  @Override
  public String getLocalName()
  {
    return "localhost";
  }

  @Override
  public int getLocalPort()
  {
    return 1234;
  }

  @Override
  public Locale getLocale()
  {
    return Locale.getDefault();
  }

  @Override
  public Enumeration<Locale> getLocales()
  {
    return new Vector<Locale>( Arrays.asList( Locale.getAvailableLocales() ) ).elements();
  }

  @Override
  public String getParameter( String arg0 )
  {
    String[] params = parameters.get( arg0 );
    if ( params != null && params.length > 0 )
    {
      return params[ 0 ];
    }
    else
    {
      return null;
    }
  }

  @Override
  public Map<String, String[]> getParameterMap()
  {
    return parameters;
  }

  @Override
  public Enumeration<String> getParameterNames()
  {
    return new Vector<String>( parameters.keySet() ).elements();
  }

  @Override
  public String[] getParameterValues( String arg0 )
  {
    return parameters.get( arg0 );
  }

  @Override
  public String getProtocol()
  {
    return "http";
  }

  @Override
  public BufferedReader getReader() throws IOException
  {
    return new BufferedReader( new StringReader( "" ) );
  }

  @Override
  public String getRealPath( String arg0 )
  {
    return "";
  }

  @Override
  public String getRemoteAddr()
  {
    return "147.27.41.25";
  }

  @Override
  public String getRemoteHost()
  {
    return "147.27.41.25";
  }

  @Override
  public int getRemotePort()
  {
    return 8080;
  }

  @Override
  public RequestDispatcher getRequestDispatcher( String arg0 )
  {
    return null;
  }

  @Override
  public String getScheme()
  {
    return "http";
  }

  @Override
  public String getServerName()
  {
    return "localhost";
  }

  @Override
  public int getServerPort()
  {
    return 1234;
  }

  @Override
  public boolean isSecure()
  {
    return false;
  }

  @Override
  public void removeAttribute( String arg0 )
  {

  }

  @Override
  public void setAttribute( String arg0, Object arg1 )
  {

  }

  @Override
  public void setCharacterEncoding( String arg0 ) throws UnsupportedEncodingException
  {

  }

  @Override
  public String getAuthType()
  {
    return "";
  }

  @Override
  public String getContextPath()
  {
    return "";
  }

  @Override
  public Cookie[] getCookies()
  {
    return new Cookie[ 0 ];
  }

  @Override
  public long getDateHeader( String arg0 )
  {
    return 0;
  }

  @Override
  public String getHeader( String arg0 )
  {
    return null;
  }

  @Override
  public Enumeration<String> getHeaderNames()
  {
    return new Vector<String>().elements();
  }

  @Override
  public Enumeration<String> getHeaders( String arg0 )
  {
    return new Vector<String>().elements();
  }

  @Override
  public int getIntHeader( String arg0 )
  {
    return 0;
  }

  @Override
  public String getMethod()
  {
    return method;
  }

  @Override
  public String getPathInfo()
  {
    return "";
  }

  @Override
  public String getPathTranslated()
  {
    return "";
  }

  @Override
  public String getQueryString()
  {
    return "";
  }

  @Override
  public String getRemoteUser()
  {
    return "";
  }

  @Override
  public String getRequestURI()
  {
    return url;
  }

  @Override
  public StringBuffer getRequestURL()
  {
    return new StringBuffer( url );
  }

  @Override
  public String getRequestedSessionId()
  {
    return "123456";
  }

  @Override
  public String getServletPath()
  {
    return "/";
  }

  @Override
  public HttpSession getSession()
  {
    return null;
  }

  @Override
  public HttpSession getSession( boolean arg0 )
  {
    return null;
  }

  @Override
  public Principal getUserPrincipal()
  {
    return null;
  }

  @Override
  public boolean isRequestedSessionIdFromCookie()
  {
    return false;
  }

  @Override
  public boolean isRequestedSessionIdFromURL()
  {
    return false;
  }

  @Override
  public boolean isRequestedSessionIdFromUrl()
  {
    return false;
  }

  @Override
  public boolean isRequestedSessionIdValid()
  {
    return false;
  }

  @Override
  public boolean isUserInRole( String arg0 )
  {
    return false;
  }

    public boolean authenticate(HttpServletResponse response) throws IOException, ServletException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void login(String username, String password) throws ServletException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void logout() throws ServletException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    

    public boolean isAsyncStarted() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isAsyncSupported() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

   
  
}
