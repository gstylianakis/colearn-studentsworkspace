/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package general;

import java.util.ArrayList;
import org.w3c.dom.Node;

/**
 *
 * @author Administrator
 */
public class ActivityElement {
    private ArrayList roles;
    private Node activityNode;
    private String activityId;
    private XMLParser parser = new XMLParser();

    public ActivityElement(){
        
    }
    public String getType(){
            return "Activity";
    }
    public ActivityElement(Node activityNode,String activityId){
        this.activityId = activityId;
        this.activityNode = activityNode;
    }
    public ActivityElement(String role_id, Node activityNode,String activityId) {
        this.roles = new ArrayList();
        this.roles.add(role_id);
        this.activityNode = activityNode;
        this.activityId = activityId;
    }

    public Node getActivityNode() {
        return activityNode;
    }

    public ArrayList getRoles() {
        return roles;
    }

    public String getActivityId() {
        return activityId;
    }
    public String getActivityName(){
        return ((Node)parser.getChildsElementNodeByName(this.activityNode,"title").get(0)).getTextContent();
    }
    

}
