/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package general;




import blackboard.blti.consumer.BLTIConsumer;
import blackboard.blti.message.BLTIMessage;
import blackboard.blti.message.Role;

import blackboard.blti.util.StringUtil;

import java.util.Map;
import javax.servlet.RequestDispatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class BasicLtiConsumption {

   

  /**
   * Test a (signed) message that's missing a required BLTI parameter.
   */

  

  /**
   * Test a minimal valid Basic LTI message.
   */

  
  /**
   * Test a valid message with user and roles.
   */

  public void consumeJforum() throws Exception
   {
    //ConsumerHttpServletResponse response = new ConsumerHttpServletResponse();
    try{
        BLTIMessage msg = new BLTIMessage( "testKey" );

        msg.getResourceLink().setId( "testResourceId" );
        msg.getUser().setId( "userId" );
        msg.getUser().setFullName( "John Smith" );
        msg.getUser().addRole( new Role( "Instructor" ) );
        HttpServletRequest request = constructBltiRequest( msg, "testSecret" );
        
        //javax.servlet.RequestDispatcher.redirect();
        RequestDispatcher dispatcher = request.getRequestDispatcher(request.getRequestURI());
        System.out.println(request.getRequestURI()+" "+dispatcher);
        
    }
    catch(Exception e){
        e.printStackTrace();
    }
    //System.out.println(request.getRequestURI());
    //request.getRequestDispatcher(request.getRequestURI().toString()).forward(request, null);
    /*BLTIMessage result = BLTIProvider.getMessage( request );
    
    System.out.println( BLTIProvider.isValid( result, "testSecret" ) );
    HttpSession session = request.getSession(false);
    if(session!=null){
        session.invalidate();
    }
    session = request.getSession();
    session.setAttribute("bltiSessionMsg",msg);
*/
    //return "redirect:/blti/tool/index";
    /*assertEquals( "testKey", result.getKey() );
    assertTrue( BLTIProvider.isValid( result, "testSecret" ) );
    assertEquals( "testResourceId", result.getResourceLink().getId() );
    assertEquals( "userId", result.getUser().getId() );
    assertEquals( "John Smith", result.getUser().getFullName() );
    assertEquals( 1, result.getUser().getRoles().size() );
    assertTrue( result.getUser().isInstructor() );
    assertFalse( result.getUser().isLearner() );
    assertEquals( "urn:lti:role:ims/lis/Instructor", result.getUser().getRoles().get( 0 ).getFull() );*/
  }

  /**
   * Test a valid unsigned message (validation with no signature check)
   */

  

  /**
   * Create a mock BLTI request.
   */
  
  private HttpServletRequest constructBltiRequest( BLTIMessage msg, String secret ) throws Exception
  {
    ConsumerHttpServletRequest req = new ConsumerHttpServletRequest();
    req.url = "http://147.27.41.25:8080/jforum/redirect2Jforum.jsp";
    req.method = "POST";

    BLTIConsumer client = new BLTIConsumer( req.method, req.url, msg );
    if ( StringUtil.notEmpty( secret ) )
    {
      client.sign( secret );
      
    }
    for ( Map.Entry<String, String> param : client.getParameters() )
    {
      req.setParameter( param.getKey(), param.getValue() );
    }
    return req;
  }
}
