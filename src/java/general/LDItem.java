/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package general;

/**
 *
 * @author Administrator
 */
public class LDItem {
    private String title;
    private boolean visibility;
    private String contentURL;
    private String type;

    public LDItem(String title,boolean visibility,String contentURL,String type){
            this.title = title;
            this.visibility = visibility;
            this.contentURL = contentURL;
            this.type = type;
    }

    public String getContentURL() {
        return contentURL;
    }

    public void setContentURL(String contentURL) {
        this.contentURL = contentURL;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }


}
