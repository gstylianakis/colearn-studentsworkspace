/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package general;


import java.util.ArrayList;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
/**
 *
 * @author Administrator
 */
public class XMLParser {
    public ArrayList getChildsElementNodeByName(Node parent,String childName){
        Node node_child = null;
        ArrayList child_nodes = new ArrayList();
       
        for(int i=0;i<parent.getChildNodes().getLength();i++){
            node_child = parent.getChildNodes().item(i);        
            if(node_child.getNodeType()==1 && node_child.getNodeName().contains(childName))
                child_nodes.add(node_child);
        }
      
        return child_nodes;
    }

    public Element getsecondGradeChildren(Node parent,String firstGradeName,String secondGradeName)throws Exception{
        try{
            Element sibling = null;
            Element innerSibling = null;
            for(int i=0;i<parent.getChildNodes().getLength();i++){
                sibling = (Element) parent.getChildNodes().item(i);
                if(sibling.getNodeName().compareTo(firstGradeName)==0){
                    for(int j=0;j<sibling.getChildNodes().getLength();j++){
                        innerSibling = (Element) sibling.getChildNodes().item(j);
                        if(innerSibling.getNodeName().contains(secondGradeName))
                            return innerSibling;
                    }
                }
            }
            return null;
        }
        catch(Exception e){
            throw e;
        }

    }
}
