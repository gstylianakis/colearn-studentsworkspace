/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package general;

import java.io.StringReader;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Administrator
 */
public class LDElementParser {

    public Document stringToXml(String stringRecords)throws Exception{
        try{
           
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(stringRecords));         
            Document doc = db.parse(is);
            return doc;

        }
        catch(Exception e){
            throw e;
        }
    }
    public Element getEnabledActivity(Document ldPlay)throws Exception{
        try{
            Element act,role_part,activity=null;
            NodeList acts = ldPlay.getElementsByTagName("act");
            for (int i = 0; i < acts.getLength(); i++) {
                act = (Element) acts.item(i);
                NodeList role_parts = act.getElementsByTagName("role-part");
                for(int j=0;j<role_parts.getLength();j++){
                    role_part = (Element) role_parts.item(j);
                    activity = (Element)role_part.getFirstChild();                    
                    if(activity!=null)
                        return activity;
                }
            }
        }
        catch(Exception e){
            throw e;
        }
        return null;
    }

    public ActivityResources getItemsFromActivity(Document activity)throws Exception{
        try{
            Element item = null;
            String title,URLcontent,type,visibility = "";
            Element activityTitle = (Element)activity.getElementsByTagName("title").item(0);
            ActivityResources currentActivityResources = new ActivityResources(activityTitle.getTextContent());
            Element learningObjectives = (Element)activity.getElementsByTagName("learning-objectives").item(0);
            Element prerequisities = (Element)activity.getElementsByTagName("prerequisites").item(0);
            Element descriptions = (Element)activity.getElementsByTagName("activity-description").item(0);
            if(learningObjectives!=null)
                for(int i=0;i<learningObjectives.getChildNodes().getLength();i++){

                    item = (Element) learningObjectives.getChildNodes().item(i);
                    title = item.getElementsByTagName("title").item(0).getTextContent();
                    URLcontent = item.getAttribute("url");
                    type = item.getAttribute("type");
                    visibility = item.getAttribute("isvisible");
                    currentActivityResources.setLearning_objectives(title, Boolean.getBoolean(visibility), URLcontent, type);

                }

            if(prerequisities!=null)
                for(int i=0;i<prerequisities.getChildNodes().getLength();i++){
                    item = (Element) prerequisities.getChildNodes().item(i);
                    title = item.getElementsByTagName("title").item(0).getTextContent();
                    URLcontent = item.getAttribute("url");
                    type = item.getAttribute("type");
                    visibility = item.getAttribute("isvisible");
                    currentActivityResources.setPrerequisities(title, Boolean.getBoolean(visibility), URLcontent, type);
                }
            
            if(descriptions!=null)
                for(int i=0;i<descriptions.getChildNodes().getLength();i++){
                    item = (Element) descriptions.getChildNodes().item(i);
                    title = item.getElementsByTagName("title").item(0).getTextContent();
                    URLcontent = item.getAttribute("url");
                    type = item.getAttribute("type");
                    visibility = item.getAttribute("isvisible");
                    currentActivityResources.setActivityDescription(title, Boolean.getBoolean(visibility), URLcontent, type);
                }

            return currentActivityResources;
        }
        catch(Exception e){
            throw e;
        }

    }
}
