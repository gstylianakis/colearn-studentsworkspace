/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package smackAPI;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;

import java.io.File;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferNegotiator;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;
/**
 *
 * @author stylianos
 */
public class smackFileManagment {
    private XMPPConnection connection;
    private FileTransferManager manager ;
    final org.jivesoftware.smack.ConnectionConfiguration configuration = new ConnectionConfiguration("petrolma-6fa201", 5222);

    public smackFileManagment() throws Exception {
        try{

            connection = new XMPPConnection(configuration);
            connection.connect();
 // Most servers require you to login before performing other tasks.
            

        }
        catch(Exception e){
            throw e;
        }
    }
    public void login(String username, String psw) throws Exception{
        
        connection.login(username, psw);
    }

    public void sendFile(String destination,String file)throws Exception{
        try{
            // Create the file transfer manager
            manager = new FileTransferManager(connection);

      // Create the outgoing file transfer
            FileTransferNegotiator.setServiceEnabled(connection,true);
            OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer(destination);//"john@bpstylianos/Smack");
System.out.println("sending file");


            OutgoingFileTransfer.setResponseTimeout(30000);
      // Send the file
            transfer.sendFile("C:/test.txt",100,"");

            try {
                Thread.sleep(10000);
                
            }catch(Exception e){System.out.println("error while sending file "+e.toString());}

            System.out.println("Status :: " + transfer.getStatus() + " Error :: " + transfer.getError() + " Exception :: " + transfer.getException());
            System.out.println("Is it done? " + transfer.isDone());
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

     public void receiveFile()throws Exception{// Create the listener
        manager = new FileTransferManager(connection);
System.out.println("try to get file");
        manager.addFileTransferListener(new FileTransferListener() {
            public void fileTransferRequest(FileTransferRequest request) {
                try{
                // Check to see if the request should be accepted
                  //if(shouldAccept(request)) {
                      System.out.println("getting file "+request.getFileName());
                        IncomingFileTransfer transfer = request.accept();
                        transfer.recieveFile(new File("C:/a"+request.getFileName()));
                  //} else {
                        // Reject it
                        //request.reject();
                  //}
                }
                catch(Exception e){
                    e.printStackTrace();
                }

            }
            private boolean shouldAccept(FileTransferRequest request) {
                throw new UnsupportedOperationException("Not yet implemented");
            }
      });
     }

}


     
