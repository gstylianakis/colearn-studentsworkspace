DELIMITER $$

DROP PROCEDURE IF EXISTS openfire.updateUserRoster $$
CREATE PROCEDURE openfire.updateUserRoster(userName varchar(100),jid varchar(100), nickname varchar(100))
Begin
  insert into openfire.ofroster (username,jid,sub,ask,recv,nick) values(userName,jid,3,-1,-1,nickname);
END $$

DELIMITER ;
